Imports System.IO
Imports vTouch.Display.Settings

Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then
                
                Dim tmpProgramDirectory As String = Constants.RegData_DisplayInstallPath.GetValue().ToString() 'Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "vTouch Pro\vTouch Pro Sound")
                tmpProgramDirectory = tmpProgramDirectory.Replace("Display", "Sound")
                Constants.RegData_SoundInstallPath.SetValue(tmpProgramDirectory)
                If Not Directory.Exists(tmpProgramDirectory) Then Directory.CreateDirectory(tmpProgramDirectory)

                For Each tmpProcess As Process In Process.GetProcessesByName("vTouch Pro Sound")
                    tmpProcess.Kill()
                Next

                Threading.Thread.Sleep(5000)

                For Each tmpProcess As Process In Process.GetProcessesByName("vTouch Pro Sound Updater")
                    tmpProcess.Kill()
                Next

                Threading.Thread.Sleep(5000)

                Shell("taskkill -f -im vTouch Pro Sound Updater.exe")

                Threading.Thread.Sleep(10000)

                Shell("taskkill -f -im vTouch Pro Sound.exe")

                Threading.Thread.Sleep(10000)

                Dim tmpFileList As New ArrayList
                tmpFileList.Add("AxInterop.VSMEDIAPLAYERLib.1.0.dll")
                tmpFileList.Add("AxInterop.VTOUCHPROSOUNDLib.1.0.dll")
                tmpFileList.Add("ConfigFile.dll")
                tmpFileList.Add("ICSharpCode.SharpZipLib.dll")
                tmpFileList.Add("Interop.CONFIGFILELib.dll")
                tmpFileList.Add("Interop.VSDOGWATCHLib.1.0.dll")
                tmpFileList.Add("Interop.VSMEDIAPLAYERLib.1.0.dll")
                tmpFileList.Add("Interop.VTOUCHPROSOUNDLib.1.0.dll")
                tmpFileList.Add("mermaid.BaseObjects.dll")
                tmpFileList.Add("mermaid.BaseObjects.pdb")
                tmpFileList.Add("mermaid.Collections.dll")
                tmpFileList.Add("mermaid.Collections.pdb")
                tmpFileList.Add("mermaid.LogSystem.Shared.dll")
                tmpFileList.Add("mermaid.LogSystem.Shared.pdb")
                tmpFileList.Add("mermaid.RegistryEditor.dll")
                tmpFileList.Add("mermaid.RegistryEditor.pdb")
                tmpFileList.Add("mermaid.Updater.Client.dll")
                tmpFileList.Add("mermaid.Updater.Client.pdb")
                tmpFileList.Add("mermaid.Updater.Common.dll")
                tmpFileList.Add("mermaid.Updater.Common.pdb")
                tmpFileList.Add("vTouch Pro Display.pdb")
                tmpFileList.Add("vTouch Pro Sound Updater.exe")
                tmpFileList.Add("vTouch Pro Sound Updater.pdb")
                tmpFileList.Add("vTouch Pro Sound.exe")
                tmpFileList.Add("vTouch Pro Sound.pdb")
                tmpFileList.Add("vTouch.API.dll")
                tmpFileList.Add("vTouch.API.pdb")
                tmpFileList.Add("vTouch.Display.Instance.pdb")
                tmpFileList.Add("vTouch.Display.Settings.dll")
                tmpFileList.Add("vTouch.Display.Settings.pdb")
                tmpFileList.Add("vTouch.Shared.dll")
                tmpFileList.Add("vTouch.Shared.pdb")
                tmpFileList.Add("vTouch.Statistics.dll")
                tmpFileList.Add("vTouch.Statistics.pdb")
                tmpFileList.Add("vTouchProSound.ocx")
                tmpFileList.Add("wavdest.ax")
                tmpFileList.Add("StandardPlaylist.xml")


                For Each tmpFileName As String In tmpFileList
                    Dim tmpFilePath As String = Path.Combine(tmpProgramDirectory, tmpFileName)
                    If File.Exists(tmpFilePath & ".vup") Then
                        File.Delete(tmpFilePath & ".vup")
                    End If
                    If File.Exists(tmpFilePath) Then
                        File.Move(tmpFilePath, tmpFilePath & ".vup")
                    End If
                Next

                For Each tmpFileName As String In tmpFileList

                    If (tmpFileName = "StandardPlaylist.xml") Then
                        Dim tmpstandardplaylistDirectory As String = Path.Combine(Constants.RegData_LocalResourcePath.GetValue().ToString(), "Permanent Resources\Sound\")
                        If Not Directory.Exists(tmpstandardplaylistDirectory) Then Directory.CreateDirectory(tmpstandardplaylistDirectory)
                        Dim tmpFilePath As String = Path.Combine(tmpstandardplaylistDirectory, tmpFileName)
                        Dim tmpFileStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.Header & tmpFileName)
                        Helpers.WriteStream(tmpFileStream, tmpFilePath)

                    Else
                        Dim tmpFilePath As String = Path.Combine(tmpProgramDirectory, tmpFileName)
                        Dim tmpFileStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.Header & tmpFileName)
                        Helpers.WriteStream(tmpFileStream, tmpFilePath)
                    End If

                Next

                Constants.RegData_StartSound.SetValue(1)

                Constants.RegData_RestartRequired.SetValue(1)
                Helpers.RegisterPatchRan()
                LblMessage.Text = "Installed"
                Helpers.WriteLog("Installed")
            Else
                LblMessage.Text = "Not installed"
                Helpers.WriteLog("Not installed")
            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class