﻿Imports System.IO

Public Class Helpers

    Public Shared Sub WriteStream(ByVal FileStream As Stream, ByVal Filename As String)
        Dim buffer(FileStream.Length - 1) As Byte
        FileStream.Read(buffer, 0, buffer.Length)
        Dim tmpStream As Stream = File.Create(Filename)
        Try
            tmpStream.Write(buffer, 0, buffer.Length)
        Finally
            tmpStream.Dispose()
        End Try

    End Sub

    Public Shared Sub WriteLog(ByVal message As String)
        Try
            Dim ev As New EventLog("Application", ".", "vTouch Pro Sound")
            ev.WriteEntry("vTouch Pro Sound Installor" & vbCrLf & _
                          message)

        Catch ex As Exception
            Constants.RegData_LastError.SetValue(ex.Message & " - " & ex.StackTrace)

        End Try
    End Sub

    Public Shared Sub WriteLog(ByVal exception As Exception)
        Try
            Dim tmpMessage As String = exception.Message & vbCrLf
            tmpMessage &= exception.StackTrace & vbCrLf

            Dim tmpException As Exception = exception.InnerException

            While Not tmpException Is Nothing
                tmpMessage &= vbCrLf
                tmpMessage &= "Caused by:" & vbCrLf
                tmpMessage = tmpException.Message & vbCrLf
                tmpMessage &= tmpException.StackTrace & vbCrLf

                tmpException = tmpException.InnerException

            End While

            Dim ev As New EventLog("Application", ".", "vTouch Pro Sound")
            ev.WriteEntry("vTouch Pro Sound Installor" & vbCrLf & _
                          tmpMessage)

        Catch ex As Exception
            Constants.RegData_LastError.SetValue(ex.Message & " - " & ex.StackTrace)

        End Try
    End Sub

    Public Shared Function HasBeenExecuted() As Boolean      ' Checks if the patch has already been installed
        Try
            Dim InstalledPatches() As String
            Dim TmpPatchID As String
            InstalledPatches = Constants.RegData_Patches.GetValue
            Dim TmpInstalled As ArrayList = New ArrayList
            If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
                For Each TmpPatchID In InstalledPatches
                    TmpInstalled.Add(TmpPatchID)
                    If TmpPatchID = Constants.PatchID Then
                        WriteLog("Sound already installed")
                        Return True
                    End If
                Next
            End If

            WriteLog("Sound not executed previously")
            Return False

        Catch ex As Exception
            WriteLog(ex)

            Return False

        End Try
    End Function

    Public Shared Function VerifyCustomer() As Boolean
        Try
            If Constants.AcceptedCustomerIDs.Length = 0 Then Return True

            Dim tmpCustomerID As Integer = Constants.RegData_CustomerID.GetValue

            If Array.IndexOf(Constants.AcceptedCustomerIDs, tmpCustomerID) <> -1 Then
                WriteLog("Sound valid for CustomerID " & tmpCustomerID)

                Return True

            Else
                WriteLog("Sound not valid for CustomerID " & tmpCustomerID)

                Return False

            End If

        Catch ex As Exception
            WriteLog(ex)

            Return False

        End Try
    End Function

    Public Shared Sub RegisterPatchRan()
        Try
            Dim InstalledPatches() As String

            InstalledPatches = Constants.RegData_Patches.GetValue
            Dim TmpInstalled As ArrayList = New ArrayList
            If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
                TmpInstalled.AddRange(InstalledPatches)
            End If
            TmpInstalled.Add(Constants.PatchID.ToString)
            Constants.RegData_Patches.SetValue(TmpInstalled.ToArray(GetType(String)))

        Catch ex As Exception
            WriteLog(ex)

        End Try
    End Sub

End Class
