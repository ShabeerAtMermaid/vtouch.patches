Imports System.IO

Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then
                Dim tmpCurrentDirectory As New DirectoryInfo(My.Application.Info.DirectoryPath)
                Dim tmpProgramDirectory As String = Path.Combine(tmpCurrentDirectory.Parent.FullName, "vTouch Pro Display")

                If Not Directory.Exists(tmpProgramDirectory) Then Directory.CreateDirectory(tmpProgramDirectory)

                For Each tmpProcess As Process In Process.GetProcessesByName("vtouch pro display")
                    tmpProcess.Kill()
                Next

                For Each tmpProcess As Process In Process.GetProcessesByName("vtouch.display.instance")
                    tmpProcess.Kill()
                Next

                Threading.Thread.Sleep(5000)

                Shell("taskkill -f -im ""vtouch pro display.exe""")
                Shell("taskkill -f -im ""vtouch.display.instance.exe""")

                Threading.Thread.Sleep(10000)

                Dim tmpFileList As New ArrayList
                tmpFileList.Add("axinterop.vsmediaplayerlib.1.0.dll")
                tmpFileList.Add("client.dll")
                tmpFileList.Add("client.pdb")
                tmpFileList.Add("configfile.dll")
                tmpFileList.Add("icsharpcode.sharpziplib.dll")
                tmpFileList.Add("interop.configfilelib.1.0.dll")
                tmpFileList.Add("interop.configfilelib.dll")
                tmpFileList.Add("interop.vsdogwatchlib.1.0.dll")
                tmpFileList.Add("interop.vsextenddesktoplib.1.0.dll")
                tmpFileList.Add("interop.vsmediaplayerlib.1.0.dll")
                tmpFileList.Add("mermaid.baseobjects.dll")
                tmpFileList.Add("mermaid.baseobjects.pdb")
                tmpFileList.Add("mermaid.collections.dll")
                tmpFileList.Add("mermaid.collections.pdb")
                tmpFileList.Add("mermaid.keyboardhook.dll")
                tmpFileList.Add("mermaid.keyboardhook.pdb")
                tmpFileList.Add("mermaid.logsystem.shared.dll")
                tmpFileList.Add("mermaid.logsystem.shared.pdb")
                tmpFileList.Add("mermaid.registryeditor.dll")
                tmpFileList.Add("mermaid.registryeditor.pdb")
                tmpFileList.Add("mermaid.updater.client.dll")
                tmpFileList.Add("mermaid.updater.client.pdb")
                tmpFileList.Add("mermaid.updater.common.dll")
                tmpFileList.Add("mermaid.updater.common.pdb")
                tmpFileList.Add("vsdogwatch.dll")
                tmpFileList.Add("vsextenddesktop.dll")
                tmpFileList.Add("vsmediaplayer.ocx")
                tmpFileList.Add("vtouch pro display.exe")
                tmpFileList.Add("vtouch pro display.pdb")
                tmpFileList.Add("vtouch.api.dll")
                tmpFileList.Add("vtouch.api.pdb")
                tmpFileList.Add("vtouch.display.instance.exe")
                tmpFileList.Add("vtouch.display.instance.pdb")
                tmpFileList.Add("vtouch.display.settings.dll")
                tmpFileList.Add("vtouch.display.settings.pdb")
                tmpFileList.Add("vtouch.shared.dll")
                tmpFileList.Add("vtouch.shared.pdb")
                tmpFileList.Add("vtouch.statistics.dll")
                tmpFileList.Add("vtouch.statistics.pdb")

                For Each tmpFileName As String In tmpFileList
                    Try
                        Dim tmpFilePath As String = Path.Combine(tmpProgramDirectory, tmpFileName)

                        If File.Exists(tmpFilePath & ".vup") Then
                            File.Delete(tmpFilePath & ".vup")
                        End If

                        If File.Exists(tmpFilePath) Then
                            File.Move(tmpFilePath, tmpFilePath & ".vup")
                        End If

                    Catch ex As Exception
                        'SILENT
                    End Try
                Next

                For Each tmpFileName As String In tmpFileList
                    Try
                        Dim tmpFilePath As String = Path.Combine(tmpProgramDirectory, tmpFileName)

                        Dim tmpFileStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.Header & tmpFileName)
                        Helpers.WriteStream(tmpFileStream, tmpFilePath)

                    Catch ex As Exception
                        'SILENT
                    End Try
                Next

                Dim tmpArgData As New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\vTouch Pro DisplayServices\Processes\vTouch Pro 3G Modem", "Arguments")
                tmpArgData.SetValue("/min_dialup_interval 30 84.246.245.135 8.8.8.8 /logfile \""C:\\Program Files\\vTouch Pro\\vTouch Pro 3G Modem\\mm.log\""  /number_of_quiet_pass_pings 89")


                Helpers.RegisterPatchRan()

                Constants.RegData_RestartRequired.SetValue(1)

                LblMessage.Text = "Installed"
                Helpers.WriteLog("Installed")

            Else
                LblMessage.Text = "Not installed"
                Helpers.WriteLog("Not installed")

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class