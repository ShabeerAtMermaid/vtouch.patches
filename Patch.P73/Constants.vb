﻿Public Class Constants

    Public Shared Header = "vTouch.Patch."
    Public Shared PatchID As Integer = 73

    'Empty means all
    Public Shared AcceptedCustomerIDs() As Integer = New Integer() {1005, 1026, 1027, 1028, 1029, 1030, 1032, 1087, 1088, 1089, 1310, 1311, 1312, 1313, 1314, 2018, 2021, 2022, 2023, 2025, 2029, 2032, 2037, 2039, 2042, 2045, 2047, 2050, 2051, 2052, 2053, 2054, 2057, 2058, 2059, 2060, 2062, 2063, 2064, 2065, 2066, 2067, 2069, 2071, 2072, 2073, 2074, 2076, 2082, 2088} 'TODO: Change accepted customers

    Public Shared RegData_Patches As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\mermaid Updater\mermaid Update Client", "InstalledPatches")
    Public Shared RegData_LastError As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro", "LastError")

    Public Shared RegData_CustomerID As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\Common Settings", "CustomerID")
    Public Shared RegData_RestartRequired As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\vTouch Pro DisplayServices", "RestartRequired")
    Public Shared RegData_LocalResourcePath As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\Common Settings", "LocalResourcePath")

End Class