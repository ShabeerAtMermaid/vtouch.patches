Imports System.IO

Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then

                Constants.RegData_CleanExt.DeleteValue()
                Constants.RegData_Cleanupfolder.DeleteKey()

                Dim tmpProgramDirectory As String = Constants.RegData_CleanupInstallPath.GetValue().ToString()
                tmpProgramDirectory = tmpProgramDirectory.Replace(Constants.Displayfolder, Constants.Cleanfolder)
                If Directory.Exists(tmpProgramDirectory) Then

                    '    Dim tmpFileList As New ArrayList
                    '    tmpFileList.Add("ConfigFile.dll")
                    '    tmpFileList.Add("Interop.CONFIGFILELib.dll")
                    '    tmpFileList.Add("mermaid.BaseObjects.dll")
                    '    tmpFileList.Add("mermaid.LogSystem.Shared.dll")
                    '    tmpFileList.Add("mermaid.LogSystem.Shared.pdb")
                    '    tmpFileList.Add("mermaid.RegistryEditor.dll")
                    '    tmpFileList.Add("mermaid.RegistryEditor.pdb")
                    '    tmpFileList.Add("vTouch.Display.Settings.dll")
                    '    tmpFileList.Add("vTouch.Display.Settings.pdb")
                    '    tmpFileList.Add("vTouch.Shared.dll")
                    '    tmpFileList.Add("vTouchCleaupExtension.dll")
                    '    tmpFileList.Add("vTouchCleaupExtension.pdb")
                    '    tmpFileList.Add("vTouch.CleaupExtension.dll")
                    '    tmpFileList.Add("vTouch.CleaupExtension.pdb")

                    '    For Each tmpFileName As String In tmpFileList
                    '        Dim tmpFilePath As String = Path.Combine(tmpProgramDirectory, tmpFileName)
                    '        If File.Exists(tmpFilePath & ".vup") Then
                    '            File.Delete(tmpFilePath & ".vup")
                    '        End If
                    '        If File.Exists(tmpFilePath) Then
                    '            File.Move(tmpFilePath, tmpFilePath & ".vup")
                    '        End If
                    '    Next
                    'End If

                    Dim tmpfolder As New DirectoryInfo(tmpProgramDirectory)
                    For Each tmpFileInfo As FileInfo In tmpfolder.GetFiles("*.vup", SearchOption.AllDirectories)
                        tmpFileInfo.Delete()
                    Next
                    For Each tmpFileInfo As FileInfo In tmpfolder.GetFiles("*", SearchOption.AllDirectories)
                        tmpFileInfo.MoveTo(tmpFileInfo.FullName & ".vup")
                    Next
                    Constants.RegData_RestartRequired.SetValue(1)
                End If
                Helpers.RegisterPatchRan()
                LblMessage.Text = "Installed"
                Helpers.WriteLog("Installed")
            Else
                LblMessage.Text = "Not installed"
                Helpers.WriteLog("Not installed")

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class