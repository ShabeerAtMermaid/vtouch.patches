Imports System.IO
Imports Microsoft.Win32

Public Class Main
    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Function GetDiskSize(ByVal drive As String) As Double
        Try
            Dim allDrives() As DriveInfo = DriveInfo.GetDrives()
            Dim DiskSize As Double = 0
            Dim d As DriveInfo
            For Each d In allDrives
                If d.IsReady = True And d.Name.ToUpper() = drive.ToUpper() Then
                    'DiskSize = d.TotalSize
                    DiskSize = d.AvailableFreeSpace
                    Exit For
                End If
            Next
            Return DiskSize
        Catch ex As Exception
            Return 0
        End Try

    End Function
    Private Sub UnInstalledDiskpart()
        'Try
        ' Open the Uninstall registry subkey.
        Dim Key As RegistryKey = Registry.LocalMachine.OpenSubKey _
           ("SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", False)
        ' Retrieve a list of installed software products.
        ' This list also includes some software products that are not valid.
        Dim SubKeyNames() As String = Key.GetSubKeyNames()
        ' Declare a variable to iterate through the retrieved list of 
        ' installed software products.
        Dim Index As Integer
        ' Declare a variable to hold the registry subkey that correspond
        ' to each retrieved software product.
        Dim SubKey As RegistryKey
        ' Iterate through the retrieved software products.
        For Index = 0 To Key.SubKeyCount - 1
            ' Open the registry subkey that corresponds to the current software product.
            ' SubKeyNames(Index) contains the name of the node that corresponds to the 
            ' current software product.
            SubKey = Registry.LocalMachine.OpenSubKey _
               ("SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall" + "\" _
                  + SubKeyNames(Index), False)
            ' Verify that the DisplayName exists. If the DisplayName does not exist, 
            ' return a null string. If the returned value is a null string, the 
            ' DisplayName does not exist, and the software product is not valid.
            If Not SubKey.GetValue("DisplayName", "") Is "" Then
                ' The current software product is valid.
                ' Display the DisplayName of this valid software product.
                Dim strrequiredApp As String = CType(SubKey.GetValue("DisplayName", ""), String)
                Dim strrequiredAppUninstalled As String = CType(SubKey.GetValue("UninstallString", ""), String)
                If (strrequiredApp.ToLower.ToString().IndexOf("diskpart") > 0) Then
                    ''MsiExec.exe /X{9782762F-639B-499B-A23D-5EBEAFC160E6} /qn /norestart REBOOT=ReallySuppress
                    Dim strtemp As String = String.Concat(strrequiredAppUninstalled, " /qn /norestart REBOOT=ReallySuppress")
                    Try
                        Shell(strtemp, AppWinStyle.NormalFocus, True, -1)
                    Catch ex As Exception
                        Helpers.WriteLog(ex.Message.ToString() & "\n" & ex.StackTrace)
                    End Try
                    Exit For
                End If
            End If
        Next
        'Catch ex As Exception
        '   Helpers.WriteLog(ex.Message.ToString() & "\n" & ex.StackTrace)
        ' End Try

    End Sub
    Private Sub Startcleanup()
        'Try
        Shell("C:\windows\system32\cleanmgr.exe /dC /sagerun: 1", AppWinStyle.NormalFocus, True, -1)
        'Catch ex As Exception
        'Helpers.WriteLog(ex.Message.ToString() & "\n" & ex.StackTrace)
        'End Try
    End Sub
    Private Sub vTouchcleanup()
        'Try
        Dim strLocalResourcePath As String = CType(Constants.RegData_LocalResourcePath.GetValue(), String)  'Registry.GetValue("HKEY_LOCAL_MACHINE\Software\mermaid technology\vTouch Pro\Common Settings", "LocalResourcePath", "").ToString()
        If Not String.IsNullOrEmpty(strLocalResourcePath) Then
            Dim parts As String() = strLocalResourcePath.Split(New Char() {"\"c})
            ' Loop through result strings with For Each
            Dim part As String = String.Empty
            Dim Requiredpart As String = String.Empty
            Dim Previouspart As String = String.Empty
            For Each part In parts
                If (part.ToLower().ToString().StartsWith("vtouch")) Then
                    Requiredpart = part
                    Exit For
                Else
                    Previouspart = System.IO.Path.Combine(Previouspart, part)
                End If
                'Helpers.WriteLog(part)
            Next
            If (Not String.IsNullOrEmpty(Requiredpart)) Then
                For Each myDirectory As IO.DirectoryInfo In New IO.DirectoryInfo(Previouspart).GetDirectories()
                    If (myDirectory.Name.ToLower().ToString() = Requiredpart.ToLower().ToString()) Then
                        Continue For
                    ElseIf (myDirectory.Name.ToLower().ToString().StartsWith("vtouch")) Then
                        Try
                            If (System.IO.Directory.Exists(System.IO.Path.Combine(Previouspart, myDirectory.Name.ToString()))) Then
                                System.IO.Directory.Delete(System.IO.Path.Combine(Previouspart, myDirectory.Name.ToString()), True)
                            End If
                        Catch ex As Exception
                            Helpers.WriteLog(ex.Message.ToString() & "\n" & ex.StackTrace)
                        End Try
                    Else
                        Continue For
                    End If
                Next
            End If
        End If
        'Catch ex As Exception
        'Helpers.WriteLog(ex.Message.ToString() & "\n" & ex.StackTrace)
        'End Try
    End Sub
    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then

                vTouchcleanup()
                UnInstalledDiskpart()
                Startcleanup()

                LblMessage.Text = "Installed"
                Helpers.WriteLog("Installed")
                Constants.RegData_RestartRequired.SetValue(1)
                Helpers.WriteLog("Display services is going to reboot machine")
                Helpers.RegisterPatchRan()

            Else
                LblMessage.Text = "Not Installed"

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class
