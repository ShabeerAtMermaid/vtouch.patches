Imports System.IO

Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then
                Dim tmpRegData_FlashVersion As RegData = New RegData(Enums.Hive.LocalMachine, Constants.RegPathFlashplayer, "CurrentVersion")

                Dim tmpCurrentVersion As String = tmpRegData_FlashVersion.GetValue

                If String.IsNullOrEmpty(tmpCurrentVersion) OrElse String.Compare(tmpCurrentVersion, "11,4,402,278", True) <> 0 Then

                    'ActiveX installer
                    'Dim tmpActiveXFlashInstallerPath As String = Path.Combine(Path.GetTempPath, "install_flash_player_ax.exe")
                    'Dim tmpActiveXFlashInstallerFileData As New FileData("install_flash_player_ax.exe", tmpActiveXFlashInstallerPath, False)
                    'tmpActiveXFlashInstallerFileData.DeployFile()

                    'Dim tmpActiveXFlashProcess As New Process
                    'tmpActiveXFlashProcess.StartInfo.FileName = tmpActiveXFlashInstallerPath
                    'tmpActiveXFlashProcess.StartInfo.Arguments = "/s"
                    'tmpActiveXFlashProcess.Start()

                    'tmpActiveXFlashProcess.WaitForExit()


                    Dim tmpActiveXFlashInstallerPath As String = Path.Combine(Path.GetTempPath, "install_flash_player_ax.exe")
                    Dim tmpActiveXFlashProcess As New Process
                    tmpActiveXFlashProcess.StartInfo.FileName = tmpActiveXFlashInstallerPath
                    tmpActiveXFlashProcess.StartInfo.Arguments = "/passive /quiet"
                    tmpActiveXFlashProcess.Start()
                    tmpActiveXFlashProcess.WaitForExit()


                    'Plugin installer
                    'Dim tmpPluginFlashInstallerPath As String = Path.Combine(Path.GetTempPath, "install_flash_player.exe")
                    'Dim tmpPluginFlashInstallerFileData As New FileData("install_flash_player.exe", tmpPluginFlashInstallerPath, False)
                    'tmpPluginFlashInstallerFileData.DeployFile()

                    'Dim tmpPluginFlashProcess As New Process
                    'tmpPluginFlashProcess.StartInfo.FileName = tmpPluginFlashInstallerPath
                    'tmpPluginFlashProcess.StartInfo.Arguments = "/s"
                    'tmpPluginFlashProcess.Start()

                    'tmpPluginFlashProcess.WaitForExit()

                    Dim tmpPluginFlashInstallerPath As String = Path.Combine(Path.GetTempPath, "install_flash_player.exe")
                    Dim tmpPluginFlashProcess As New Process
                    tmpPluginFlashProcess.StartInfo.FileName = tmpPluginFlashInstallerPath
                    tmpPluginFlashProcess.StartInfo.Arguments = "/passive /quiet"
                    tmpPluginFlashProcess.Start()
                    tmpPluginFlashProcess.WaitForExit()

                    Constants.RegData_RestartRequired.SetValue(1)

                Else
                    Helpers.WriteLog("Flash 11.4.402.278 already installed")

                End If

                LblMessage.Text = "Installed"
                Helpers.WriteLog("Installed")

                Helpers.RegisterPatchRan()

            Else
                LblMessage.Text = "Patch Not Installed - skipped"

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class