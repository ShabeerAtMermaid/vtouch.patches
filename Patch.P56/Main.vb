Imports System.IO
Imports System.ComponentModel

Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then
                'ActiveX installer

                Dim tmpvTouchProPath As String = System.IO.Path.Combine(My.Application.Info.DirectoryPath, "vTouchPro.jpg")

                Dim tmpPermanentPath As String = Path.Combine(Constants.RegData_LocalResourcePath.GetValue().ToString(), "Permanent Resources") ''System.Environment.ExpandEnvironmentVariables("%windir%\system32")
                tmpPermanentPath = Path.Combine(tmpPermanentPath, "vTouchPro.jpg")

                File.Copy(tmpvTouchProPath, tmpPermanentPath, True)
                File.Delete(tmpvTouchProPath)

                'System.IO.File.Copy(tmpSystem32Path, tmpSystem32Path, True)

                'Dim tmpGplMpgDecInstallerFileData As New FileData("GplMpgDec.ax", tmpSystem32Path, True)
                'tmpGplMpgDecInstallerFileData.DeployFile()

                LblMessage.Text = "Installed"
                Helpers.WriteLog("Installed")
                Helpers.RegisterPatchRan()

                Dim localDisplay As Process() = Process.GetProcessesByName("vTouch Pro Display.exe")
                For Each currentprocess As Process In localDisplay
                    currentprocess.Kill()
                Next

                Dim localDisplayInstance As Process() = Process.GetProcessesByName("vTouch.Display.Instance")
                For Each currentprocess As Process In localDisplayInstance
                    currentprocess.Kill()
                Next
                If (localDisplay.Length > 0 Or localDisplayInstance.Length > 0) Then
                    Dim newprocess As Process = New Process
                    newprocess.StartInfo.FileName = System.IO.Path.Combine(My.Application.Info.DirectoryPath, "vTouch Pro Display.exe")
                    newprocess.StartInfo.Arguments = "skipupdate"
                    newprocess.Start()
                End If
            Else
                LblMessage.Text = "Patch Not Installed - skipped"
            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class