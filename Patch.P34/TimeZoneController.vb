﻿Imports System.Runtime.InteropServices

Public Class TimeZoneController

#Region "API Declarations"

    ''' <summary>        
    ''' [Win32 API call]        
    ''' The GetTimeZoneInformation function retrieves the current time-zone parameters.        
    ''' These parameters control the translations between Coordinated Universal Time (UTC)        
    ''' and local time.        
    ''' </summary>        
    ''' <param name="lpTimeZoneInformation">[out] Pointer to a TIME_ZONE_INFORMATION structure to receive the current time-zone parameters.</param>        
    ''' <returns>        ''' If the function succeeds, the return value is one of the following values.        
    ''' <list type="table">        
    ''' <listheader>        
    ''' <term>Return code/value</term>        
    ''' <description>Description</description>        
    ''' </listheader>        
    ''' <item>        
    ''' <term>TIME_ZONE_ID_UNKNOWN == 0</term>        
    ''' <description>        
    ''' The system cannot determine the current time zone. This error is also returned if you call the SetTimeZoneInformation function and supply the bias values but no transition dates.        ''' This value is returned if daylight saving time is not used in the current time zone, because there are no transition dates.        ''' </description>        ''' </item>        ''' <item>        ''' <term>TIME_ZONE_ID_STANDARD == 1</term>        ''' <description>        ''' The system is operating in the range covered by the StandardDate member of the TIME_ZONE_INFORMATION structure.        
    ''' </description>        
    ''' </item>        
    ''' <item>        
    ''' <term>TIME_ZONE_ID_DAYLIGHT == 2</term>        
    ''' <description>        
    ''' The system is operating in the range covered by the DaylightDate member of the TIME_ZONE_INFORMATION structure.        
    ''' </description>        
    ''' </item>        
    ''' </list>        
    ''' If the function fails, the return value is TIME_ZONE_ID_INVALID. To get extended error information, call GetLastError.        
    ''' </returns>        
    Private Declare Function GetTimeZoneInformation Lib "Kernel32.dll" _
        (ByRef lpTimeZoneInformation As TimeZoneInformation) As UInt32

    ''' <summary>        
    ''' [Win32 API call]        
    ''' The SetTimeZoneInformation function sets the current time-zone parameters.        
    ''' These parameters control translations from Coordinated Universal Time (UTC)        
    ''' to local time.        
    ''' </summary>        
    ''' <param name="lpTimeZoneInformation">[in] Pointer to a TIME_ZONE_INFORMATION structure that contains the time-zone parameters to set.</param>        
    ''' <returns>       
    ''' If the function succeeds, the return value is nonzero.    
    ''' If the function fails, the return value is zero. To get extended error information, call GetLastError.   
    ''' </returns> 
    Private Declare Function SetTimeZoneInformation Lib "Kernel32.dll" _
        (ByRef lpTimeZoneInformation As TimeZoneInformation) As Boolean

#End Region

#Region "Structures"

    ''' <summary>   
    ''' The SystemTime structure represents a date and time using individual members      
    ''' for the month, day, year, weekday, hour, minute, second, and millisecond.    
    ''' </summary>   
    <StructLayoutAttribute(LayoutKind.Sequential)> _
    Public Structure SystemTime
        Public year As Int16
        Public month As Int16
        Public dayOfWeek As Int16
        Public day As Int16
        Public hour As Int16
        Public minute As Int16
        Public second As Int16
        Public milliseconds As Int16

    End Structure

    ''' <summary>       
    ''' The TimeZoneInformation structure specifies information specific to the time zone.      
    ''' </summary>      
    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Unicode)> _
    Public Structure TimeZoneInformation
        ''' <summary>      
        ''' Current bias for local time translation on this computer, in minutes. 
        ''' The bias is the difference, in minutes, between Coordinated Universal Time (UTC) and local time. 
        ''' All translations between UTC and local time are based on the following formula:            
        ''' <para>UTC = local time + bias</para>        
        ''' <para>This member is required.</para>          
        ''' </summary>         
        Public bias As Int32

        ''' <summary>     
        ''' Pointer to a null-terminated string associated with standard time. 
        ''' For example, "EST" could indicate Eastern Standard Time. 
        ''' The string will be returned unchanged by the GetTimeZoneInformation function. 
        ''' This string can be empty.  
        ''' </summary>     
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=32)> _
        Public standardName As String

        ''' <summary>           
        ''' A SystemTime structure that contains a date and local time when the transition from daylight saving time to 
        ''' standard time occurs on this operating system. 
        ''' If the time zone does not support daylight saving time or if the caller needs to disable daylight saving time, 
        ''' the wMonth member in the SystemTime structure must be zero. 
        ''' If this date is specified, the DaylightDate value in the TimeZoneInformation structure must also be specified. 
        ''' Otherwise, the system assumes the time zone data is invalid and no changes will be applied.          
        ''' <para>To select the correct day in the month, set the wYear member to zero, the wHour and wMinute members to 
        ''' the transition time, the wDayOfWeek member to the appropriate weekday, and the wDay member to indicate the 
        ''' occurence of the day of the week within the month (first through fifth).</para>         
        ''' <para>Using this notation, specify the 2:00a.m. on the first Sunday in April as follows: 
        ''' wHour = 2, wMonth = 4, wDayOfWeek = 0, wDay = 1. Specify 2:00a.m. on the last Thursday in October as follows: 
        ''' wHour = 2, wMonth = 10, wDayOfWeek = 4, wDay = 5.</para>       
        ''' </summary>         
        Public standardDate As SystemTime

        ''' <summary>      
        ''' Bias value to be used during local time translations that occur during standard time. 
        ''' This member is ignored if a value for the StandardDate member is not supplied.           
        ''' <para>This value is added to the value of the Bias member to form the bias used during standard time. 
        ''' In most time zones, the value of this member is zero.</para>       
        ''' </summary>          
        Public standardBias As Int32

        ''' <summary>      
        ''' Pointer to a null-terminated string associated with daylight saving time. 
        ''' For example, "PDT" could indicate Pacific Daylight Time. 
        ''' The string will be returned unchanged by the GetTimeZoneInformation function. 
        ''' This string can be empty.     
        ''' </summary>         
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=32)> _
        Public daylightName As String

        ''' <summary>          
        ''' A SystemTime structure that contains a date and local time when the transition from standard time 
        ''' to daylight saving time occurs on this operating system. 
        ''' If the time zone does not support daylight saving time or if the caller needs to disable daylight saving time, 
        ''' the wMonth member in the SystemTime structure must be zero. 
        ''' If this date is specified, the StandardDate value in the TimeZoneInformation structure must also be specified. 
        ''' Otherwise, the system assumes the time zone data is invalid and no changes will be applied.         
        ''' <para>To select the correct day in the month, set the wYear member to zero, the wHour and wMinute members to 
        ''' the transition time, the wDayOfWeek member to the appropriate weekday, and the wDay member to indicate the 
        ''' occurence of the day of the week within the month (first through fifth).</para>     
        ''' </summary>         
        Public daylightDate As SystemTime

        ''' <summary>     
        ''' Bias value to be used during local time translations that occur during daylight saving time. 
        ''' This member is ignored if a value for the DaylightDate member is not supplied.          
        ''' <para>This value is added to the value of the Bias member to form the bias used during daylight saving time. 
        ''' In most time zones, the value of this member is –60.</para>          
        ''' </summary>        
        Public daylightBias As Int32

    End Structure

#End Region

    ''' <summary>      
    ''' Sets new time-zone information for the local system.       
    ''' </summary>       
    ''' <param name="tzi">Struct containing the time-zone parameters to set.</param>   
    Public Shared Sub SetTimeZone(ByVal tzi As TimeZoneInformation)
        ' set local system timezone            
        SetTimeZoneInformation(tzi)

    End Sub

    ''' <summary>       
    ''' Gets current timezone information for the local system.     
    ''' </summary>       
    ''' <returns>Struct containing the current time-zone parameters.</returns>    
    Public Shared Function GetTimeZone() As TimeZoneInformation
        ' create struct instance           
        Dim tzi As TimeZoneInformation

        ' retrieve timezone info       
        Dim currentTimeZone As Int32 = GetTimeZoneInformation(tzi)

        Return tzi

    End Function

End Class