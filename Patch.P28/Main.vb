Imports System.IO

Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then
                Dim tmpRegData_DisplayInstallPath As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\vTouch Pro Display", "InstallPath")

                Dim tmpDisplayInstallPath As String = tmpRegData_DisplayInstallPath.GetValue

                If String.IsNullOrEmpty(tmpDisplayInstallPath) Then
                    tmpDisplayInstallPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "vTouch Pro\vTouch Pro Display")
                End If

                Dim tmpDisplayFolderInfo As New DirectoryInfo(tmpDisplayInstallPath)

                If Not tmpDisplayFolderInfo.Exists Then
                    tmpDisplayFolderInfo.Create()
                End If

                Dim tmpDisplayFiles As New ArrayList

                tmpDisplayFiles.Add(New FileData("AxInterop.VSMEDIAPLAYERLib.1.0.dll", Path.Combine(tmpDisplayFolderInfo.FullName, "AxInterop.VSMEDIAPLAYERLib.1.0.dll"), False))
                tmpDisplayFiles.Add(New FileData("ICSharpCode.SharpZipLib.dll", Path.Combine(tmpDisplayFolderInfo.FullName, "ICSharpCode.SharpZipLib.dll"), False))
                tmpDisplayFiles.Add(New FileData("Interop.VSDOGWATCHLib.1.0.dll", Path.Combine(tmpDisplayFolderInfo.FullName, "Interop.VSDOGWATCHLib.1.0.dll"), False))
                tmpDisplayFiles.Add(New FileData("Interop.VSMEDIAPLAYERLib.1.0.dll", Path.Combine(tmpDisplayFolderInfo.FullName, "Interop.VSMEDIAPLAYERLib.1.0.dll"), False))
                tmpDisplayFiles.Add(New FileData("mermaid.BaseObjects.dll", Path.Combine(tmpDisplayFolderInfo.FullName, "mermaid.BaseObjects.dll"), False))
                tmpDisplayFiles.Add(New FileData("mermaid.BaseObjects.pdb", Path.Combine(tmpDisplayFolderInfo.FullName, "mermaid.BaseObjects.pdb"), False))
                tmpDisplayFiles.Add(New FileData("mermaid.Collections.dll", Path.Combine(tmpDisplayFolderInfo.FullName, "mermaid.Collections.dll"), False))
                tmpDisplayFiles.Add(New FileData("mermaid.Collections.pdb", Path.Combine(tmpDisplayFolderInfo.FullName, "mermaid.Collections.pdb"), False))
                tmpDisplayFiles.Add(New FileData("mermaid.LogSystem.Shared.dll", Path.Combine(tmpDisplayFolderInfo.FullName, "mermaid.LogSystem.Shared.dll"), False))
                tmpDisplayFiles.Add(New FileData("mermaid.LogSystem.Shared.pdb", Path.Combine(tmpDisplayFolderInfo.FullName, "mermaid.LogSystem.Shared.pdb"), False))
                tmpDisplayFiles.Add(New FileData("mermaid.RegistryEditor.dll", Path.Combine(tmpDisplayFolderInfo.FullName, "mermaid.RegistryEditor.dll"), False))
                tmpDisplayFiles.Add(New FileData("mermaid.RegistryEditor.pdb", Path.Combine(tmpDisplayFolderInfo.FullName, "mermaid.RegistryEditor.pdb"), False))
                tmpDisplayFiles.Add(New FileData("mermaid.Updater.Client.dll", Path.Combine(tmpDisplayFolderInfo.FullName, "mermaid.Updater.Client.dll"), False))
                tmpDisplayFiles.Add(New FileData("mermaid.Updater.Client.pdb", Path.Combine(tmpDisplayFolderInfo.FullName, "mermaid.Updater.Client.pdb"), False))
                tmpDisplayFiles.Add(New FileData("mermaid.Updater.Common.dll", Path.Combine(tmpDisplayFolderInfo.FullName, "mermaid.Updater.Common.dll"), False))
                tmpDisplayFiles.Add(New FileData("mermaid.Updater.Common.pdb", Path.Combine(tmpDisplayFolderInfo.FullName, "mermaid.Updater.Common.pdb"), False))
                tmpDisplayFiles.Add(New FileData("VSDogWatch.dll", Path.Combine(tmpDisplayFolderInfo.FullName, "VSDogWatch.dll"), True))
                tmpDisplayFiles.Add(New FileData("VSMediaPlayer.ocx", Path.Combine(tmpDisplayFolderInfo.FullName, "VSMediaPlayer.ocx"), True))
                tmpDisplayFiles.Add(New FileData("vTouch Pro Display.exe", Path.Combine(tmpDisplayFolderInfo.FullName, "vTouch Pro Display.exe"), False))
                tmpDisplayFiles.Add(New FileData("vTouch Pro Display.pdb", Path.Combine(tmpDisplayFolderInfo.FullName, "vTouch Pro Display.pdb"), False))
                tmpDisplayFiles.Add(New FileData("vTouch.API.dll", Path.Combine(tmpDisplayFolderInfo.FullName, "vTouch.API.dll"), False))
                tmpDisplayFiles.Add(New FileData("vTouch.API.pdb", Path.Combine(tmpDisplayFolderInfo.FullName, "vTouch.API.pdb"), False))
                tmpDisplayFiles.Add(New FileData("vTouch.Display.Instance.exe", Path.Combine(tmpDisplayFolderInfo.FullName, "vTouch.Display.Instance.exe"), False))
                tmpDisplayFiles.Add(New FileData("vTouch.Display.Instance.pdb", Path.Combine(tmpDisplayFolderInfo.FullName, "vTouch.Display.Instance.pdb"), False))
                tmpDisplayFiles.Add(New FileData("vTouch.Display.Settings.dll", Path.Combine(tmpDisplayFolderInfo.FullName, "vTouch.Display.Settings.dll"), False))
                tmpDisplayFiles.Add(New FileData("vTouch.Display.Settings.pdb", Path.Combine(tmpDisplayFolderInfo.FullName, "vTouch.Display.Settings.pdb"), False))
                tmpDisplayFiles.Add(New FileData("vTouch.Shared.dll", Path.Combine(tmpDisplayFolderInfo.FullName, "vTouch.Shared.dll"), False))
                tmpDisplayFiles.Add(New FileData("vTouch.Shared.pdb", Path.Combine(tmpDisplayFolderInfo.FullName, "vTouch.Shared.pdb"), False))
                tmpDisplayFiles.Add(New FileData("vTouch.Statistics.dll", Path.Combine(tmpDisplayFolderInfo.FullName, "vTouch.Statistics.dll"), False))
                tmpDisplayFiles.Add(New FileData("vTouch.Statistics.pdb", Path.Combine(tmpDisplayFolderInfo.FullName, "vTouch.Statistics.pdb"), False))

                For Each tmpFileData As FileData In tmpDisplayFiles
                    tmpFileData.DeployFile()

                Next

                LblMessage.Text = "Installed"
                Helpers.WriteLog("Installed")

                Helpers.RegisterPatchRan()

            Else
                LblMessage.Text = "Not Installed"

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class