Imports System.IO
Imports Microsoft.Win32
Imports vTouch.PowerManagement
Imports System.Diagnostics


Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then
                Try
                    Helpers.WriteLog("Rebooting throught PMS Way")

                    Dim p As Process = New Process()
                    p.StartInfo.UseShellExecute = False
                    p.StartInfo.RedirectStandardOutput = True
                    p.StartInfo.FileName = "shutdown.exe"
                    p.StartInfo.Arguments = "-r -f -t 0"
                    p.StartInfo.CreateNoWindow = True
                    p.Start()

                Catch ex As Exception
                    Try
                        Helpers.WriteLog(ex)
                        Helpers.WriteLog("Rebooting throught shutdown.exe Way")
                        System.Diagnostics.Process.Start("shutdown.exe", "-r -t 60")
                    Catch ex1 As Exception
                        Helpers.WriteLog("Rebooting throught Shell Way")
                        Shell("shutdown -r -t 60")
                    End Try

                End Try

                LblMessage.Text = "Installed"
                Helpers.WriteLog("Installed")

                Helpers.WriteLog("Machine is going to restart")

                Helpers.RegisterPatchRan()

            Else
                LblMessage.Text = "Not Installed"

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class
