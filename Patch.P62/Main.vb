Imports System.IO
Imports vTouch.Display.Settings

Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then
                
                Dim RegData_UpdateBackupExpireTime As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\vTouch Pro DisplayServices", "BackupExpireTime")

                RegData_UpdateBackupExpireTime.SetValue(1000)
                DisplayServicesSettings.BackupExpireTime = 1000


                'Dim localDisplay As Process() = Process.GetProcessesByName("vTouch Pro Display services")
                'For Each currentprocess As Process In localDisplay
                '    currentprocess.Kill()
                'Next

                'Dim localDisplay1 As Process() = Process.GetProcessesByName("DFSClient")
                'For Each currentprocess As Process In localDisplay1
                '    currentprocess.Kill()
                'Next
                'Try

                '    Dim strSource As String = String.Empty
                '    Dim strDest As String = String.Empty

                '    '"D:\Program file\vTouch Pro\vTouch Pro Display Services"

                '    strSource = System.IO.Path.Combine(My.Application.Info.DirectoryPath, "ConfigFile.dll")
                '    strDest = System.IO.Path.Combine(My.Application.Info.DirectoryPath.ToString() & " services", "ConfigFile.dll")
                '    Helpers.WriteLog("strSource : " & strSource)
                '    Helpers.WriteLog("strDest : " & strDest)
                '    If File.Exists(strSource) Then
                '        File.Copy(strSource, strDest, True)
                '        Helpers.WriteLog("Copied : ")
                '    End If

                '    strSource = System.IO.Path.Combine(My.Application.Info.DirectoryPath, "DFSClient.exe")
                '    strDest = System.IO.Path.Combine(My.Application.Info.DirectoryPath.ToString() & " services", "DFSClient.exe")

                '    If File.Exists(strSource) Then
                '        File.Copy(strSource, strDest, True)
                '        Try
                '            File.Delete(strSource)
                '        Catch ex As Exception
                '            Helpers.WriteLog(ex)
                '        End Try
                '    End If


                '    strSource = System.IO.Path.Combine(My.Application.Info.DirectoryPath, "vTouch.Display.Settings.dll")
                '    strDest = System.IO.Path.Combine(My.Application.Info.DirectoryPath.ToString() & " services", "vTouch.Display.Settings.dll")

                '    If File.Exists(strSource) Then
                '        File.Copy(strSource, strDest, True)
                '    End If


                '    strSource = System.IO.Path.Combine(My.Application.Info.DirectoryPath, "vTouch.Display.Settings.pdb")
                '    strDest = System.IO.Path.Combine(My.Application.Info.DirectoryPath.ToString() & " services", "vTouch.Display.Settings.pdb")

                '    If File.Exists(strSource) Then
                '        File.Copy(strSource, strDest, True)
                '    End If


                '    'Use existing functionality
                '    Dim tmpScriptBuilder As New System.Text.StringBuilder
                '    tmpScriptBuilder.AppendLine("net start ""vTouch Pro Display Services"" /skipupdate")
                '    Dim tmpRestartScriptFile As New FileInfo(Path.Combine(Path.GetTempPath, "restartapp.bat"))

                '    If tmpRestartScriptFile.Exists Then tmpRestartScriptFile.Delete()
                '    File.WriteAllText(tmpRestartScriptFile.FullName, tmpScriptBuilder.ToString)
                '    Shell(tmpRestartScriptFile.FullName, AppWinStyle.MinimizedNoFocus, False)

                'Catch ex As Exception
                '    Helpers.WriteLog(ex)
                'End Try

                Constants.RegData_RestartRequired.SetValue(1)

                Helpers.RegisterPatchRan()

                LblMessage.Text = "Installed"
                Helpers.WriteLog("Installed")

            Else
                LblMessage.Text = "Not installed"
                Helpers.WriteLog("Not installed")

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class