﻿Public Class Constants

    Public Shared Header = "vTouch.Patch."
    Public Shared PatchID As Integer = 90 'TODO: Change PatchID

    'Empty means all
    Public Shared AcceptedCustomerIDs() As Integer = New Integer() {} 'TODO: Change accepted customers

    Public Shared RegData_Patches As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\mermaid Updater\mermaid Update Client", "InstalledPatches")
    Public Shared RegData_LastError As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro", "LastError")

    Public Shared RegData_CustomerID As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\Common Settings", "CustomerID")
    Public Shared RegData_RestartRequired As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\vTouch Pro DisplayServices", "RestartRequired")
    Public Shared RegData_LocalResourcePath As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\Common Settings", "LocalResourcePath")

    Public Shared RegPathFlashplayer As String = "SOFTWARE\Macromedia\FlashPlayer"

End Class
