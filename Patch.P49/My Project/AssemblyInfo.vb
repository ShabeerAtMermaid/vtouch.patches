﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("vTouch Pro Patch 49")> 
<Assembly: AssemblyDescription("Updates updater port to 8029 and removes any port no in update server address")> 
<Assembly: AssemblyCompany("Mermaid Technology A/S")> 
<Assembly: AssemblyProduct("vTouch Pro")> 
<Assembly: AssemblyCopyright("Copyright © Mermaid Technology A/S 2010")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("4327fce6-9bc3-47a7-9af0-2d7d0a5c6c98")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("4.0.0.*")> 
<Assembly: AssemblyFileVersion("4.0.0.0")> 
