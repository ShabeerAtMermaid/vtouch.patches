﻿Public Class Constants

    Public Shared Header = "vTouch.Patch."
    Public Shared PatchID As Integer = 94

    'Empty means all
    Public Shared AcceptedCustomerIDs() As Integer = New Integer() {} 'TODO: Change accepted customers

    Public Shared RegData_Patches As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\mermaid Updater\mermaid Update Client", "InstalledPatches")
    Public Shared RegData_LastError As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro", "LastError")
    Public Shared RegData_CustomerID As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\Common Settings", "CustomerID")
    Public Shared RegData_ModelName As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\Common Settings", "ModelName")
    Public Shared RegData_RestartRequired As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\vTouch Pro DisplayServices", "RestartRequired")

    Public Shared powercfgAcvalueindex100 As String = "cmd /c Powercfg -setacvalueindex 381b4222-F694-41f0-9685-ff5bb260df2e 54533251-82be-4824-96c1-47b60b740d00 893dee8e-2bef-41e0-89c6-b55d0929964c 100"
    Public Shared powercfgAcvalueindex0 As String = "cmd /c Powercfg -setacvalueindex 381b4222-F694-41f0-9685-ff5bb260df2e 238c9fa8-0aad-41ed-83f4-97be242c8f20 9d7815a6-7ee4-497e-8888-515a05f02364 0"

    Public Shared powercfgAcvalueindex100_command As String = " /c Powercfg -setacvalueindex 381b4222-F694-41f0-9685-ff5bb260df2e 54533251-82be-4824-96c1-47b60b740d00 893dee8e-2bef-41e0-89c6-b55d0929964c 100"
    Public Shared powercfgAcvalueindex0_command As String = " /c Powercfg -setacvalueindex 381b4222-F694-41f0-9685-ff5bb260df2e 238c9fa8-0aad-41ed-83f4-97be242c8f20 9d7815a6-7ee4-497e-8888-515a05f02364 0"


    Public Shared expectedModel As String = "NDIS 127"

End Class
