Imports System.IO


Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try

            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then

                If Constants.RegData_ModelName.ValueExists Then
                    If String.Compare(Constants.RegData_ModelName.GetValue, Constants.expectedModel, True) = 0 Then
                        Try
                            Dim processorMgmControl As String = Constants.powercfgAcvalueindex100
                            Dim sleepHibernateAfter As String = Constants.powercfgAcvalueindex0

                            Shell(processorMgmControl, AppWinStyle.NormalFocus, True)
                            Threading.Thread.Sleep(5000)
                            Shell(sleepHibernateAfter, AppWinStyle.NormalFocus, True)

                        Catch ex As Exception

                            Dim p As Process = New Process()
                            Dim pi As ProcessStartInfo = New ProcessStartInfo()
                            pi.Arguments = Constants.powercfgAcvalueindex100_command
                            pi.FileName = "cmd.exe"
                            p.StartInfo = pi
                            p.WaitForExit()
                            p.Start()

                            Threading.Thread.Sleep(5000)

                            Dim p1 As Process = New Process()
                            Dim pi1 As ProcessStartInfo = New ProcessStartInfo()
                            pi1.Arguments = Constants.powercfgAcvalueindex0_command
                            pi1.FileName = "cmd.exe"
                            p1.StartInfo = pi1
                            p1.WaitForExit()
                            p1.Start()

                        End Try
                        Constants.RegData_RestartRequired.SetValue(1)
                    End If
                End If

                Helpers.RegisterPatchRan()
                LblMessage.Text = "Installed"
                Helpers.WriteLog("Installed")
            Else
                LblMessage.Text = "Patch Not Installed - skipped"
                Helpers.WriteLog("Not installed")
            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class