Imports System.IO

Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then
                If Not Directory.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "vTouch Pro\vTouch Pro DID Monitor")) Then
                    Dim tmpMonitorInstallerPath As String = Path.Combine(Path.GetTempPath, "vTouch Pro DID Monitor Extension Setup 1.0.1.msi")
                    Dim tmpMonitorStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.Header & "vTouch Pro DID Monitor Extension Setup 1.0.1.msi")
                    Helpers.WriteStream(tmpMonitorStream, tmpMonitorInstallerPath)

                    Dim tmpMonitorProcess As New Process
                    tmpMonitorProcess.StartInfo.FileName = tmpMonitorInstallerPath
                    'tmpMonitorProcess.StartInfo.Arguments = "/s"
                    tmpMonitorProcess.Start()

                    tmpMonitorProcess.WaitForExit()

                    LblMessage.Text = "Installed"
                    Helpers.WriteLog("Installed")

                Else
                    LblMessage.Text = "Already installed"
                    Helpers.WriteLog("Already installed")
                End If

                Helpers.RegisterPatchRan()

            Else
                LblMessage.Text = "Not Installed"

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class