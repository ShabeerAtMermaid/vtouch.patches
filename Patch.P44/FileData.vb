﻿Imports System.IO

Public Class FileData

    Public FileName As String
    Public TargetPath As String
    Public Register As Boolean

    Public Sub New(ByVal filename As String, ByVal target As String, ByVal register As Boolean)
        MyBase.New()

        Me.FileName = filename
        Me.TargetPath = target
        Me.Register = register

    End Sub

    Public Function DeployFile() As Boolean
        Try
            Dim tmpFileStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.Header & Me.FileName)
            Helpers.WriteStream(tmpFileStream, Me.TargetPath)

            If Me.Register Then
                Dim tmpRegisterProcess As New Process
                tmpRegisterProcess.StartInfo.FileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.System), "regsvr32.exe")
                tmpRegisterProcess.StartInfo.Arguments = "/s """ & Me.TargetPath & """"
                tmpRegisterProcess.StartInfo.CreateNoWindow = True
                tmpRegisterProcess.Start()

            End If

            Return True

        Catch ex As Exception
            Helpers.WriteLog(ex)

            Return False

        End Try
    End Function

End Class