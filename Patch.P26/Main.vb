Imports System.IO
Imports Microsoft.Win32

Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then
                Dim tmpRegData_ComputerID_Old As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\Victor Soft\vTouch Pro\Common Settings", "ComputerId")
                Dim tmpRegData_CustomerID_Old As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\Victor Soft\vTouch Pro\Common Settings", "CustomerId")
                Dim tmpRegData_LocalBackupPath_Old As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\Victor Soft\vTouch Pro\Common Settings", "DownloadManagerBackupFolder")
                Dim tmpRegData_LocalResumePath_Old As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\Victor Soft\vTouch Pro\Common Settings", "DownloadManagerResumeFolder")
                Dim tmpRegData_LocalResourcePath_Old As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\Victor Soft\vTouch Pro\Common Settings", "LocalResourcePath")
                Dim tmpRegData_ServerAddress_Old As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\Victor Soft\vTouch Pro\Common Settings", "ServerAddress")
                Dim tmpRegData_UpdateServer_Old As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\Victor Soft\vTouch Pro\Common Settings", "UpdateServer")

                Dim tmpRegData_DisplayInstallPath_Old As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\Victor Soft\vTouch Pro\vTouch Pro Display", "InstallPath")
                Dim tmpRegData_DisplayID0_Old As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\Victor Soft\vTouch Pro\vTouch Pro Display\DISPLAY0", "DisplayId")
                Dim tmpRegData_DisplayID1_Old As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\Victor Soft\vTouch Pro\vTouch Pro Display\DISPLAY1", "DisplayId")
                Dim tmpRegData_DisplayID2_Old As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\Victor Soft\vTouch Pro\vTouch Pro Display\DISPLAY2", "DisplayId")

                Dim tmpRegData_ComputerID_New As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\Common Settings", "ComputerID")
                Dim tmpRegData_CustomerID_New As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\Common Settings", "CustomerID")
                Dim tmpRegData_LocalBackupPath_New As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\Common Settings", "DownloadManagerBackupFolder")
                Dim tmpRegData_LocalResumePath_New As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\Common Settings", "DownloadManagerResumeFolder")
                Dim tmpRegData_LocalResourcePath_New As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\Common Settings", "LocalResourcePath")
                Dim tmpRegData_LocalCachePath_New As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\Common Settings", "LocalCachePath")
                Dim tmpRegData_ServerAddress_New As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\Common Settings", "ServerAddress")
                Dim tmpRegData_UpdateServer_New As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\Common Settings", "UpdateServer")

                Dim tmpRegData_DisplayInstallPath_New As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\vTouch Pro Display", "InstallPath")
                Dim tmpRegData_DisplayID0_New As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\vTouch Pro Display\DISPLAY0", "DisplayID")
                Dim tmpRegData_DisplayID1_New As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\vTouch Pro Display\DISPLAY1", "DisplayID")
                Dim tmpRegData_DisplayID2_New As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\vTouch Pro Display\DISPLAY2", "DisplayID")

                'Registry
                tmpRegData_ComputerID_New.SetValue(tmpRegData_ComputerID_Old.GetValue)
                tmpRegData_CustomerID_New.SetValue(tmpRegData_CustomerID_Old.GetValue)
                tmpRegData_LocalBackupPath_New.SetValue(tmpRegData_LocalBackupPath_Old.GetValue)
                tmpRegData_LocalResumePath_New.SetValue(tmpRegData_LocalResumePath_Old.GetValue)
                tmpRegData_LocalResourcePath_New.SetValue(tmpRegData_LocalResourcePath_Old.GetValue)
                tmpRegData_ServerAddress_New.SetValue(tmpRegData_ServerAddress_Old.GetValue)
                tmpRegData_UpdateServer_New.SetValue(tmpRegData_UpdateServer_Old.GetValue & ":8019")
                tmpRegData_DisplayInstallPath_New.SetValue(tmpRegData_DisplayInstallPath_Old.GetValue)
                tmpRegData_DisplayID0_New.SetValue(tmpRegData_DisplayID0_Old.GetValue)
                tmpRegData_DisplayID1_New.SetValue(tmpRegData_DisplayID1_Old.GetValue)
                tmpRegData_DisplayID2_New.SetValue(tmpRegData_DisplayID2_Old.GetValue)

                Registry.LocalMachine.DeleteSubKeyTree("SOFTWARE\Victor Soft")

                Constants.RegData_RestartRequired.SetValue(1)

                'File System

                'Resources
                Dim tmpOldResourceFolderInfo As New DirectoryInfo(Path.Combine(tmpRegData_LocalResourcePath_New.GetValue, tmpRegData_CustomerID_New.GetValue & "\Media\1030"))

                If tmpOldResourceFolderInfo.Exists Then
                    Dim tmpNewResourceFolderInfo As New DirectoryInfo(Path.Combine(tmpRegData_LocalResourcePath_New.GetValue, tmpRegData_CustomerID_New.GetValue & "\Media"))

                    For Each tmpFile As FileInfo In tmpOldResourceFolderInfo.GetFiles
                        Dim tmpNewPath As String = Path.Combine(tmpNewResourceFolderInfo.FullName, tmpFile.Name)

                        If File.Exists(tmpNewPath) Then File.Delete(tmpNewPath)

                        tmpFile.MoveTo(tmpNewPath)

                    Next

                    tmpOldResourceFolderInfo.Delete(True)

                End If

                'Fallback
                Dim tmpOldFallbackFileInfo As New FileInfo(Path.Combine(tmpRegData_LocalResourcePath_New.GetValue, "PermanentResources\vTouchPro.jpg"))

                If tmpOldFallbackFileInfo.Exists Then
                    Dim tmpNewFallbackFileInfo As New FileInfo(Path.Combine(tmpRegData_LocalResourcePath_New.GetValue, "Permanent Resources\vTouchPro.jpg"))

                    If tmpNewFallbackFileInfo.Exists Then tmpNewFallbackFileInfo.Delete()

                    If Not tmpNewFallbackFileInfo.Directory.Exists Then
                        tmpNewFallbackFileInfo.Directory.Create()
                    End If

                    tmpOldFallbackFileInfo.MoveTo(tmpNewFallbackFileInfo.FullName)

                End If

                'Delete unused folder and files
                Dim tmpOldPermanentFolderInfo As New DirectoryInfo(Path.Combine(tmpRegData_LocalResourcePath_New.GetValue, "PermanentResources"))

                If tmpOldPermanentFolderInfo.Exists Then
                    tmpOldPermanentFolderInfo.Delete(True)

                End If

                Dim tmpTemplateFolderInfo As New DirectoryInfo(Path.Combine(tmpRegData_LocalResourcePath_New.GetValue, tmpRegData_CustomerID_New.GetValue & "\Templates"))

                If tmpTemplateFolderInfo.Exists Then
                    tmpTemplateFolderInfo.Delete(True)

                End If

                Dim tmpDedicatedFolderInfo As New DirectoryInfo(Path.Combine(tmpRegData_LocalResourcePath_New.GetValue, tmpRegData_CustomerID_New.GetValue & "\Dedicated Media"))

                If tmpDedicatedFolderInfo.Exists Then
                    tmpDedicatedFolderInfo.Delete(True)

                End If

                Dim tmpResourceFolderInfo As New DirectoryInfo(Path.Combine(tmpRegData_LocalResourcePath_New.GetValue, tmpRegData_CustomerID_New.GetValue))

                If tmpResourceFolderInfo.Exists Then
                    For Each tmpFile As FileInfo In tmpResourceFolderInfo.GetFiles
                        tmpFile.Delete()

                    Next
                End If

                LblMessage.Text = "Installed"
                Helpers.WriteLog("Installed")

                Helpers.RegisterPatchRan()

            Else
                LblMessage.Text = "Not Installed"

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class