﻿Imports mermaid.LogSystem.Shared
Imports System
Imports System.Management
Imports System.ComponentModel
Imports System.Runtime.InteropServices

Public NotInheritable Class NetworkInformation

#Region "API Calls"

    <DllImport("netapi32.dll", CharSet:=CharSet.Unicode, SetLastError:=True)> _
    Private Shared Function NetGetJoinInformation( _
        ByVal computerName As String, _
        ByRef buffer As IntPtr, _
        ByRef status As JoinStatus) As Integer
    End Function

    <DllImport("netapi32.dll", SetLastError:=True)> _
   Private Shared Function NetApiBufferFree(ByVal buffer As IntPtr) As Integer
    End Function

    <DllImport("netapi32.dll", CharSet:=CharSet.Unicode, SetLastError:=True)> _
  Private Shared Function NetRenameMachineInDomain( _
        ByVal lpServer As String, _
        ByVal lpNewMachineName As String, _
        ByVal lpAccount As String, _
        ByVal lpPassword As String, _
        ByVal fRenameOptions As UInt32) As Integer
    End Function

#End Region

#Region "Variables"

    Private Shared _Local As New NetworkInformation()
    Private _ComputerName As String
    Private _DomainName As String
    Private _Status As JoinStatus = JoinStatus.Unknown

#End Region

#Region "Properties"

    Public Shared ReadOnly Property LocalComputer() As NetworkInformation
        Get
            Return _Local

        End Get
    End Property

    Public ReadOnly Property ComputerName() As String
        Get
            If _ComputerName Is Nothing Then Return "(local)"

            Return _ComputerName

        End Get
    End Property

    Public ReadOnly Property DomainName() As String
        Get
            Return _DomainName

        End Get
    End Property

    Public ReadOnly Property Status() As JoinStatus
        Get
            Return _Status

        End Get
    End Property

#End Region

#Region "Enums"

    Public Enum JoinStatus
        Unknown = 0
        UnJoined = 1
        Workgroup = 2
        Domain = 3
    End Enum

#End Region

#Region "Constants"

    Const NETSETUP_JOIN_DOMAIN As UInteger = 1
    Const NETSETUP_ACCT_CREATE As UInteger = 2
    Const NETSETUP_ACCT_DELETE As UInteger = 4
    Const NETSETUP_WIN9X_UPGRADE As UInteger = 16
    Const NETSETUP_DOMAIN_JOIN_IF_JOINED As UInteger = 32
    Const NETSETUP_JOIN_UNSECURE As UInteger = 64
    Const NETSETUP_MACHINE_PASSWORD_PASSED As UInteger = 128
    Const NETSETUP_DEFERRED_SPN_SET As UInteger = 256
    Const NETSETUP_INSTALL_INVOCATION As UInteger = 262144

#End Region

    Public Sub New(ByVal computerName As String)
        If String.IsNullOrEmpty(computerName) Then
            Throw New ArgumentNullException("computerName")

        End If

        _ComputerName = computerName

        LoadInformation()

    End Sub

    Private Sub New()
        LoadInformation()

    End Sub

    Private Sub LoadInformation()
        Dim pBuffer As IntPtr = IntPtr.Zero
        Dim status As JoinStatus

        Try
            Dim result As Integer = NetGetJoinInformation(_computerName, pBuffer, status)
            If 0 <> result Then Throw New Win32Exception()

            _status = status
            _domainName = Marshal.PtrToStringUni(pBuffer)

        Finally
            If Not IntPtr.Zero.Equals(pBuffer) Then
                NetApiBufferFree(pBuffer)
            End If

        End Try
    End Sub

    'Public Function RenameAndJoinComputer(ByVal newName As String, ByVal domain As String, ByVal domainOU As String, ByVal domainAccount As String, ByVal domainPassword As String) As Boolean
    '    Try
    '        Dim oldName As String = My.Computer.Name

    '        Dim mo As ManagementObject = New ManagementObject("Win32_ComputerSystem.Name='" & My.Computer.Name & "'")
    '        Dim tmpMethodArgs() As Object = New Object() {domain, domainPassword, domainAccount, domainOU, 3}

    '        Dim tmpResult As Object = mo.InvokeMethod("JoinDomainOrWorkgroup", tmpMethodArgs)

    '        If tmpResult <> 0 Then
    '            LogWriter.WriteError("Error joining domain - Error code:" & tmpResult)

    '        End If

    '        If String.Compare(oldName, newName, True) <> 0 Then
    '            Dim tmpError As Integer = NetRenameMachineInDomain(Nothing, newName, domainAccount, domainPassword, NETSETUP_JOIN_DOMAIN Or NETSETUP_ACCT_CREATE)

    '            If tmpError = 0 Then
    '                Return True

    '            Else
    '                LogWriter.WriteError("Computer rename (in domain) failed with code: " & tmpError)

    '                Return False

    '            End If
    '        End If

    '    Catch ex As Exception
    '        LogWriter.WriteError(ex)

    '    End Try

    'End Function

    Public Function JoinDomain(ByVal domain As String, ByVal domainOU As String, ByVal domainAccount As String, ByVal domainPassword As String) As Boolean
        Try
            Dim oldName As String = My.Computer.Name

            Dim mo As ManagementObject = New ManagementObject("Win32_ComputerSystem.Name='" & My.Computer.Name & "'")
            Dim tmpMethodArgs() As Object = New Object() {domain, domainPassword, domainAccount, domainOU, 3}

            Dim tmpResult As Object = mo.InvokeMethod("JoinDomainOrWorkgroup", tmpMethodArgs)

            If tmpResult <> 0 Then
                Helpers.WriteLog("Error joining domain - Error code:" & tmpResult)

            End If

            Return True

        Catch ex As Exception
            Helpers.WriteLog(ex)

        End Try

    End Function

    Public Overrides Function ToString() As String
        Select Case _Status
            Case JoinStatus.Domain
                Return ComputerName & " is a member of the domain " & DomainName
            Case JoinStatus.Workgroup
                Return ComputerName & " is a member of the workgroup " & DomainName
            Case JoinStatus.UnJoined
                Return ComputerName & " is a standalone computer"
            Case Else
                Return "Unable to determine the network status of " & ComputerName
        End Select
    End Function

End Class