Imports System.IO

Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then
                Dim info As NetworkInformation = NetworkInformation.LocalComputer

                Dim tmpRegData_Network_DNSSuffix As RegData = New RegData(Enums.Hive.LocalMachine, "System\CurrentControlSet\Services\TCPIP\Parameters", "SearchList")
                Dim tmpRegData_Network_WorkGroup_Firewall As RegData = New RegData(Enums.Hive.LocalMachine, "SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\StandardProfile", "EnableFirewall")
                Dim tmpRegData_Network_WorkGroup_FirewallPolicy As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\Policies\Microsoft\WindowsFirewall\StandardProfile", "EnableFirewall")
                Dim tmpRegData_Network_WorkGroup_FirewallDoNotAllowExceptions As RegData = New RegData(Enums.Hive.LocalMachine, "SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\StandardProfile", "DoNotAllowExceptions")
                Dim tmpRegData_Network_WorkGroup_FirewallExceptionList As RegData = New RegData(Enums.Hive.LocalMachine, "SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\StandardProfile\GloballyOpenPorts\List", "")
                Dim tmpRegData_Network_Domain_Firewall As RegData = New RegData(Enums.Hive.LocalMachine, "SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\DomainProfile", "EnableFirewall")
                Dim tmpRegData_Network_Domain_FirewallPolicy As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\Policies\Microsoft\WindowsFirewall\DomainProfile", "EnableFirewall")
                Dim tmpRegData_Network_Domain_FirewallDoNotAllowExceptions As RegData = New RegData(Enums.Hive.LocalMachine, "SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\DomainProfile", "DoNotAllowExceptions")
                Dim tmpRegData_Network_Domain_FirewallExceptionList As RegData = New RegData(Enums.Hive.LocalMachine, "SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\DomainProfile\GloballyOpenPorts\List", "")

                Dim tmpRegData_Network_Firewall As RegData
                Dim tmpRegData_Network_FirewallPolicy As RegData
                Dim tmpRegData_Network_FirewallDoNotAllowExceptions As RegData
                Dim tmpRegData_Network_FirewallExceptionList As RegData

                If info.Status = NetworkInformation.JoinStatus.Workgroup Then
                    tmpRegData_Network_FirewallPolicy = tmpRegData_Network_WorkGroup_FirewallPolicy
                    tmpRegData_Network_Firewall = tmpRegData_Network_WorkGroup_Firewall
                    tmpRegData_Network_FirewallDoNotAllowExceptions = tmpRegData_Network_WorkGroup_FirewallDoNotAllowExceptions
                    tmpRegData_Network_FirewallExceptionList = tmpRegData_Network_WorkGroup_FirewallExceptionList
                Else
                    tmpRegData_Network_FirewallPolicy = tmpRegData_Network_Domain_FirewallPolicy
                    tmpRegData_Network_Firewall = tmpRegData_Network_Domain_Firewall
                    tmpRegData_Network_FirewallDoNotAllowExceptions = tmpRegData_Network_Domain_FirewallDoNotAllowExceptions
                    tmpRegData_Network_FirewallExceptionList = tmpRegData_Network_Domain_FirewallExceptionList

                End If

                Dim tmpEntryName As String = "9090:TCP"

                Dim tmpExceptionData As New RegData(tmpRegData_Network_FirewallExceptionList.Hive, tmpRegData_Network_FirewallExceptionList.Path, tmpEntryName)
                tmpExceptionData.SetValue("9090:TCP:*:Enabled:vTouch Pro DFS")

            End If

            LblMessage.Text = "Installed"
            Helpers.WriteLog("Installed")

            Helpers.RegisterPatchRan()

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class