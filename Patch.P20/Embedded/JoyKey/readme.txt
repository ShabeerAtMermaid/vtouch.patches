﻿Terms And Conditions:
=====================
This application is developed by Markus Raab,
derRaab(); - Programming | Development,
for SuperClass.de.

This programme is provided "AS  IS"!

The developer of this application is not responsible for any damage (or damages) attributed to this application. You are warned that you use this application at your own risk. No warranties are implied or given by the developer of this application or any representative.

The developer of this application reserves all rights to this programme and all original archives and contents.


Help:
=====
Joykey converts joystick events into keyboard events.

Joykey supports the directions left, up, right, down and up o eight joystick buttons.

Keyboard configurations are defined by external XML files stored in a directory called "joykey_sets". Within these files keycodes will be assigned to a specific joystick event. It's possible to edit these files or create files for new configurations. Joykey will load all these configurations on applicationstart and offer them in the keyboard configuration combobox.


Version:
========
Beta 2 (2006.06.16)


Contact And Credits:
====================
Developer:
Markus Raab"
"derRaab(); - Programming | Development
http://www.derRaab.com

Contact:
http://www.SuperClass.de
info@SuperClass.de

Credits:
Thanks to André Michelle for the idea and to Wheez for the inspiration to this simple method of resolution.
