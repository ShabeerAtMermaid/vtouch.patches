Imports System.IO

Public Class Main
    Enum MoveFileFlags As UInteger
        MOVEFILE_REPLACE_EXISTING = &H1
        MOVEFILE_COPY_ALLOWED = &H2
        MOVEFILE_DELAY_UNTIL_REBOOT = &H4
        MOVEFILE_WRITE_THROUGH = &H8
        MOVEFILE_CREATE_HARDLINK = &H10
        MOVEFILE_FAIL_IF_NOT_TRACKABLE = &H20
    End Enum
    Declare Unicode Function MoveFileEx Lib "kernel32.dll" Alias "MoveFileExW" (ByVal lpExistingFileName As String, ByVal lpNewFileName As String, ByVal dwFlags As MoveFileFlags) As Integer
    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Function GetDiskSize(ByVal drive As String) As Double
        Try
            Dim allDrives() As DriveInfo = DriveInfo.GetDrives()
            Dim DiskSize As Double = 0
            Dim d As DriveInfo
            For Each d In allDrives
                If d.IsReady = True And d.Name.ToUpper() = drive.ToUpper() Then
                    'DiskSize = d.TotalSize
                    DiskSize = d.AvailableFreeSpace
                    Exit For
                End If
            Next
            Return DiskSize
        Catch ex As Exception
            Return 0
        End Try

    End Function

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then
                Dim tmpRegData_PagingFiles As RegData = New RegData(Enums.Hive.LocalMachine, "SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management", "PagingFiles")
                'Dim tmpRegData_ClearPageFileAtShutdown As RegData = New RegData(Enums.Hive.LocalMachine, "SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management", "ClearPageFileAtShutdown")
                'Dim Cdisksize As Double = GetDiskSize("c:\")
                Dim Ddisksize As Double = GetDiskSize("d:\")
                Dim pagingSizes As New ArrayList
                If (Ddisksize > 3221225472) Then
                    Helpers.WriteLog("D drive Free space > 3GB")
                    pagingSizes.Add("d:\pagefile.sys 0 0")
                    Try
                        Dim bdelet As Boolean = MoveFileEx("c:\\pagefile.sys", vbNullString, MoveFileFlags.MOVEFILE_DELAY_UNTIL_REBOOT)
                        Helpers.WriteLog("c:\pagefile.sys will be removed at Reboot")
                    Catch ex As Exception
                        Helpers.WriteLog("Exception in removing c:\pagefile.sys")
                    End Try

                End If

                If pagingSizes.Count > 0 Then
                    Dim datalist As String() = CType(pagingSizes.ToArray(GetType(String)), String())
                    tmpRegData_PagingFiles.SetValue(datalist)
                    Helpers.WriteLog("Set VM for D, that system will manage")
                    Constants.RegData_RestartRequired.SetValue(1)
                    Helpers.WriteLog("DS will reboot the System")
                    Dim f As Integer = 0
                End If

                LblMessage.Text = "Installed"
                Helpers.WriteLog("Installed")

                Helpers.RegisterPatchRan()

                'System.Diagnostics.Process.Start("shutdown", "-r -t 00")
                'Constants.RegData_RestartRequired.SetValue(1)


            Else
                LblMessage.Text = "Not Installed"

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class
