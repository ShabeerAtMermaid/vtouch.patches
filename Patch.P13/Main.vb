Imports Microsoft.Win32
Imports System.IO

Public Class Main

    Private Const PatchID As Integer = 13
    Private RegData_Patches As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro", "InstalledPatches")
    Private RegData_CustomerID As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro\Common Settings", "CustomerId")
    Private RegData_RestartRequired As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro\vTouch Pro DisplayServices", "RestartRequired")
    Private Const Header = "VictorSoft.vTouch.Patch.P13."

    Private Sub WriteStream(ByVal FileStream As Stream, ByVal Filename As String)
        Dim buffer(FileStream.Length - 1) As Byte
        FileStream.Read(buffer, 0, buffer.Length)
        Dim tmpStream As Stream = File.Create(Filename)
        Try
            tmpStream.Write(buffer, 0, buffer.Length)
        Finally
            tmpStream.Dispose()
        End Try

    End Sub

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not VerifyAlreadyRun() And VerifyCustomer() Then
                LblStatus.Text = "Loading files.."

                Dim TmpFileStream As Stream
                Dim FilePath As String

                Dim FileList As New List(Of DllData)
                'Update files
                FileList.Add(New DllData("VictorSoft.vTouch.Management.Update.Shared.dll", False))
                FileList.Add(New DllData("VictorSoft.vTouch.Update.dll", False))
                FileList.Add(New DllData("vTouch Pro Display.exe", False))

                For Each tmpDLLData As DllData In FileList
                    FilePath = "c:\programmer\vtouch pro\vtouch pro display\" & tmpDLLData.FileName
                    Try
                        If File.Exists(FilePath) Then File.Delete(FilePath)
                    Catch ex As Exception
                        LblStatus.Text = "Cannot delete " & FilePath
                    End Try

                Next

                LblStatus.Text = "Writing files.."

                For Each tmpDLLData As DllData In FileList
                    FilePath = "c:\programmer\vtouch pro\vtouch pro display\" & tmpDLLData.FileName
                    TmpFileStream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Header & tmpDLLData.FileName)
                    Try
                        WriteStream(TmpFileStream, FilePath)

                    Catch ex As Exception
                        ' SILENT
                    End Try

                Next

                'Lets mark it for restart
                '                SetRegValue(Enums.Hive.localmachine, RegData_RestartRequired.Path, RegData_RestartRequired.Value, 1)

                RegisterPatchRan()


            End If

            LblStatus.Text = "All done."

        Catch ex As Exception
            LblStatus.Text = "Exception: " & ex.Message

        End Try

        Me.Close()

    End Sub

    Private Function VerifyAlreadyRun() As Boolean      ' Checks if the patch has already been installed
        Dim InstalledPatches() As String
        Dim TmpPatchID As String
        InstalledPatches = GetRegValue(Enums.Hive.localmachine, RegData_Patches.Path, RegData_Patches.Value)
        Dim TmpInstalled As ArrayList = New ArrayList
        If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
            For Each TmpPatchID In InstalledPatches
                TmpInstalled.Add(TmpPatchID)
                If TmpPatchID = PatchID Then
                    Return True
                End If
            Next
        End If
        ' TmpInstalled.Add(PatchID.ToString)
        ' SetRegValue(Enums.Hive.localmachine, RegData_Patches.Path, RegData_Patches.Value, TmpInstalled.ToArray(GetType(String)))

        Return False

    End Function

    Private Function VerifyCustomer() As Boolean
        Dim tmpCustomerId As Integer = GetRegValue(RegData_CustomerID.Hive, RegData_CustomerID.Path, RegData_CustomerID.Value)

        If tmpCustomerId = 2019 Then
            Return True

        End If

        Return False

    End Function

    Private Sub RegisterPatchRan()
        Dim InstalledPatches() As String

        InstalledPatches = GetRegValue(Enums.Hive.localmachine, RegData_Patches.Path, RegData_Patches.Value)
        Dim TmpInstalled As ArrayList = New ArrayList
        If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
            TmpInstalled.AddRange(InstalledPatches)
        End If
        TmpInstalled.Add(PatchID.ToString)
        SetRegValue(Enums.Hive.localmachine, RegData_Patches.Path, RegData_Patches.Value, TmpInstalled.ToArray(GetType(String)))
    End Sub

#Region "Registry"
    Public Shared Function GetRegValue(ByVal whereToRead As Enums.Hive, ByVal path As String, ByVal value As String) As Object
        Dim regKey As RegistryKey
        Dim regKeyValue As Object

        'Determine the hive and open/create the designated path
        Select Case whereToRead
            Case Enums.Hive.classesroot
                regKey = Registry.ClassesRoot.CreateSubKey(path)
            Case Enums.Hive.currentconfig
                regKey = Registry.CurrentConfig.CreateSubKey(path)
            Case Enums.Hive.currentuser
                regKey = Registry.CurrentUser.CreateSubKey(path)
            Case Enums.Hive.dyndata
                regKey = Registry.DynData.CreateSubKey(path)
            Case Enums.Hive.localmachine
                regKey = Registry.LocalMachine.CreateSubKey(path)
            Case Enums.Hive.performancedata
                regKey = Registry.PerformanceData.CreateSubKey(path)
            Case Enums.Hive.users
                regKey = Registry.Users.CreateSubKey(path)
            Case Else
                regKey = Registry.LocalMachine.CreateSubKey(path)
        End Select

        regKeyValue = regKey.GetValue(value, Nothing)
        regKey.Close()
        Return regKeyValue
    End Function

    Public Shared Sub SetRegValue(ByVal whereToWrite As Enums.Hive, ByVal path As String, ByVal value As String, ByVal data As Object)
        Dim regKey As RegistryKey

        'Determine the hive and open/create the designated path
        Select Case whereToWrite
            Case Enums.Hive.classesroot
                regKey = Registry.ClassesRoot.CreateSubKey(path)
            Case Enums.Hive.currentconfig
                regKey = Registry.CurrentConfig.CreateSubKey(path)
            Case Enums.Hive.currentuser
                regKey = Registry.CurrentUser.CreateSubKey(path)
            Case Enums.Hive.dyndata
                regKey = Registry.DynData.CreateSubKey(path)
            Case Enums.Hive.localmachine
                regKey = Registry.LocalMachine.CreateSubKey(path)
            Case Enums.Hive.performancedata
                regKey = Registry.PerformanceData.CreateSubKey(path)
            Case Enums.Hive.users
                regKey = Registry.Users.CreateSubKey(path)
            Case Else
                regKey = Registry.LocalMachine.CreateSubKey(path)
        End Select

        'Determine which datatype to write to regedit
        If TypeOf data Is Integer Then
            regKey.SetValue(value, data, RegistryValueKind.DWord)

        ElseIf TypeOf data Is String() Then
            regKey.SetValue(value, data, RegistryValueKind.MultiString)

        ElseIf TypeOf data Is Byte() Then
            regKey.SetValue(value, data, RegistryValueKind.Binary)

        Else
            regKey.SetValue(value, data)

        End If

        regKey.Close()

    End Sub

#End Region

End Class

Friend Class DllData

    Public FileName As String
    Public Register As Boolean

    Public Sub New(ByVal filename As String, ByVal register As Boolean)
        MyBase.New()

        Me.FileName = filename
        Me.Register = register

    End Sub

End Class

Friend Class RegData

#Region "Attributes"

    Private _Hive As Enums.Hive
    Private _Path As String
    Private _Value As String

#End Region

#Region "Properties"

    Public ReadOnly Property Hive() As Enums.Hive
        Get
            Return Me._Hive

        End Get
    End Property

    Public ReadOnly Property Path() As String
        Get
            Return Me._Path

        End Get
    End Property

    Public ReadOnly Property Value() As String
        Get
            Return Me._Value

        End Get
    End Property

#End Region

    Public Sub New(ByVal hive As Enums.Hive, ByVal path As String, ByVal value As String)
        Me._Hive = hive
        Me._Path = path
        Me._Value = value

    End Sub

End Class
