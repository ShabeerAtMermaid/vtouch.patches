Imports Microsoft.Win32
Imports System.IO

Public Class Main

    Private Const Header = "vTouch.Patch."
    Private Const PatchID As Integer = 25

    Private RegData_Patches As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\Victor Soft Updater", "InstalledPatches")
    Private RegData_CustomerID As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro\Common Settings", "CustomerId")
    Private RegData_RestartRequired As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro\vTouch Pro DisplayServices", "RestartRequired")
    Private RegData_LocalResourcePath As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro\Common Settings", "LocalResourcePath")
    Private RegData_LastError As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro", "LastError")

    Private RegData_Tweak_DesktopBGImage As RegData = New RegData(Enums.Hive.currentuser, "Control Panel\Desktop", "Wallpaper")
    Private RegData_Tweak_DesktopConvertedBGImage As RegData = New RegData(Enums.Hive.currentuser, "Control Panel\Desktop", "ConvertedWallpaper")
    Private RegData_Tweak_DesktopBGImageStyle As RegData = New RegData(Enums.Hive.currentuser, "Control Panel\Desktop", "WallpaperStyle")
    Private RegData_Tweak_DesktopBGColor As RegData = New RegData(Enums.Hive.currentuser, "Control Panel\Colors", "Background")

    Private Sub WriteStream(ByVal FileStream As Stream, ByVal Filename As String)
        Dim buffer(FileStream.Length - 1) As Byte
        FileStream.Read(buffer, 0, buffer.Length)
        Dim tmpStream As Stream = File.Create(Filename)
        Try
            tmpStream.Write(buffer, 0, buffer.Length)
        Finally
            tmpStream.Dispose()
        End Try

    End Sub

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not HasBeenExecuted() And VerifyCustomer() Then
                'Set BGImage
                Dim tmpImage As Image = My.Resources.vTouchPro

                Dim filepath As String = System.Environment.GetEnvironmentVariable("SystemRoot") & "\desktopbg.bmp"

                If File.Exists(filepath) Then File.Delete(filepath)

                tmpImage.Save(filepath, System.Drawing.Imaging.ImageFormat.Bmp)

                Me.RegData_Tweak_DesktopBGImage.SetValue(filepath)
                Me.RegData_Tweak_DesktopConvertedBGImage.SetValue(filepath)
                Me.RegData_Tweak_DesktopBGImageStyle.SetValue("2")

                Shell(System.Environment.GetFolderPath(Environment.SpecialFolder.System) & "\RUNDLL32.EXE user32.dll,UpdatePerUserSystemParameters", AppWinStyle.Hide)

                'Set fallback image
                Dim tmpFallbackPath As String = Path.Combine(RegData_LocalResourcePath.GetValue, "PermanentResources\vTouchPro.jpg")

                If File.Exists(tmpFallbackPath) Then File.Delete(tmpFallbackPath)
                If File.Exists(tmpFallbackPath & ".md5") Then File.Delete(tmpFallbackPath & ".md5")

                tmpImage.Save(tmpFallbackPath, System.Drawing.Imaging.ImageFormat.Jpeg)

                tmpImage.Dispose()
                'File.Copy(filepath, tmpFallbackPath)

                RegisterPatchRan()

            End If

            LblStatus.Text = "All done."

        Catch ex As Exception
            LblStatus.Text = "Exception: " & ex.Message

            RegData_LastError.SetValue(ex.Message & " - " & ex.StackTrace)

        End Try

        Me.Close()

    End Sub

    Private Function HasBeenExecuted() As Boolean      ' Checks if the patch has already been installed
        Dim InstalledPatches() As String
        Dim TmpPatchID As String
        InstalledPatches = RegData_Patches.GetValue
        Dim TmpInstalled As ArrayList = New ArrayList
        If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
            For Each TmpPatchID In InstalledPatches
                TmpInstalled.Add(TmpPatchID)
                If TmpPatchID = PatchID Then
                    Return True
                End If
            Next
        End If

        Return False

    End Function

    Private Function VerifyCustomer() As Boolean
        Try
            Dim tmpCustomerID As Integer = Me.RegData_CustomerID.GetValue

            If tmpCustomerID = 2041 Then
                Return True

            Else
                Return False

            End If

        Catch ex As Exception
            Return False

        End Try
    End Function

    Private Sub RegisterPatchRan()
        Dim InstalledPatches() As String

        InstalledPatches = RegData_Patches.GetValue
        Dim TmpInstalled As ArrayList = New ArrayList
        If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
            TmpInstalled.AddRange(InstalledPatches)
        End If
        TmpInstalled.Add(PatchID.ToString)
        RegData_Patches.SetValue(TmpInstalled.ToArray(GetType(String)))

    End Sub

End Class

Friend Class DllData

    Public FileName As String
    Public Register As Boolean

    Public Sub New(ByVal filename As String, ByVal register As Boolean)
        MyBase.New()

        Me.FileName = filename
        Me.Register = register

    End Sub

End Class

Friend Class RegData

#Region "Attributes"

    Private _Hive As Enums.Hive
    Private _Path As String
    Private _Value As String

#End Region

#Region "Properties"

    Public ReadOnly Property Hive() As Enums.Hive
        Get
            Return Me._Hive

        End Get
    End Property

    Public ReadOnly Property Path() As String
        Get
            Return Me._Path

        End Get
    End Property

    Public ReadOnly Property Value() As String
        Get
            Return Me._Value

        End Get
    End Property

#End Region

    Public Sub New(ByVal hive As Enums.Hive, ByVal path As String, ByVal value As String)
        Me._Hive = hive
        Me._Path = path
        Me._Value = value

    End Sub

    Public Function GetValue() As Object
        Dim regKey As RegistryKey
        Dim regKeyValue As Object

        'Determine the hive and open/create the designated path
        Select Case Me.Hive
            Case Enums.Hive.classesroot
                regKey = Registry.ClassesRoot.CreateSubKey(Path)
            Case Enums.Hive.currentconfig
                regKey = Registry.CurrentConfig.CreateSubKey(Path)
            Case Enums.Hive.currentuser
                regKey = Registry.CurrentUser.CreateSubKey(Path)
            Case Enums.Hive.dyndata
                regKey = Registry.DynData.CreateSubKey(Path)
            Case Enums.Hive.localmachine
                regKey = Registry.LocalMachine.CreateSubKey(Path)
            Case Enums.Hive.performancedata
                regKey = Registry.PerformanceData.CreateSubKey(Path)
            Case Enums.Hive.users
                regKey = Registry.Users.CreateSubKey(Path)
            Case Else
                regKey = Registry.LocalMachine.CreateSubKey(Path)
        End Select

        regKeyValue = regKey.GetValue(Value, Nothing)
        regKey.Close()

        Return regKeyValue

    End Function

    Public Sub SetValue(ByVal data As Object)
        Dim regKey As RegistryKey

        'Determine the hive and open/create the designated path
        Select Case Me.Hive
            Case Enums.Hive.classesroot
                regKey = Registry.ClassesRoot.CreateSubKey(Path)
            Case Enums.Hive.currentconfig
                regKey = Registry.CurrentConfig.CreateSubKey(Path)
            Case Enums.Hive.currentuser
                regKey = Registry.CurrentUser.CreateSubKey(Path)
            Case Enums.Hive.dyndata
                regKey = Registry.DynData.CreateSubKey(Path)
            Case Enums.Hive.localmachine
                regKey = Registry.LocalMachine.CreateSubKey(Path)
            Case Enums.Hive.performancedata
                regKey = Registry.PerformanceData.CreateSubKey(Path)
            Case Enums.Hive.users
                regKey = Registry.Users.CreateSubKey(Path)
            Case Else
                regKey = Registry.LocalMachine.CreateSubKey(Path)
        End Select

        'Determine which datatype to write to regedit
        If TypeOf data Is Integer Then
            regKey.SetValue(Value, data, RegistryValueKind.DWord)

        ElseIf TypeOf data Is String() Then
            regKey.SetValue(Value, data, RegistryValueKind.MultiString)

        ElseIf TypeOf data Is Byte() Then
            regKey.SetValue(Value, data, RegistryValueKind.Binary)

        Else
            regKey.SetValue(Value, data)

        End If

        regKey.Close()

    End Sub

End Class
