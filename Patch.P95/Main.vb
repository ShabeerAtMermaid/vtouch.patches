Imports System.IO

Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Hide()

            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try

            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then

                Dim displayMessage As String = "Registered and Installed"

                Dim tmpRegData_ProductName As RegData = New RegData(Enums.Hive.LocalMachine, Constants.RegPathWindow, "ProductName")
                Dim tmpProductName As String = tmpRegData_ProductName.GetValue

                If Not String.IsNullOrEmpty(tmpProductName) And tmpProductName.ToLower().Contains("windows embedded standard") Then

                    Dim tmpRegData_CentralProcessor As RegData = New RegData(Enums.Hive.LocalMachine, Constants.RegPathCentralProcessor, "Identifier")
                    Dim tmpCentralProcessor As String = tmpRegData_CentralProcessor.GetValue

                    If Not String.IsNullOrEmpty(tmpCentralProcessor) And tmpCentralProcessor.Contains("x86") Then

                        Dim tmpRegData_svcVersion As RegData = New RegData(Enums.Hive.LocalMachine, Constants.RegPathExplorerVesrion, "svcVersion")
                        Dim tmpsvcVersion As String = tmpRegData_svcVersion.GetValue

                        If Not String.IsNullOrEmpty(tmpsvcVersion) And Not tmpsvcVersion.Trim.StartsWith("11") Then

                            Dim tmpFileList As New ArrayList
                            tmpFileList.Add("IE11-Windows6.1-x86-en-us.exe")
                            For Each tmpFileName As String In tmpFileList
                                Dim tmpFilePath As String = Path.Combine(Path.GetTempPath, tmpFileName)
                                Dim tmpFileStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.Header & tmpFileName)
                                Helpers.WriteStream(tmpFileStream, tmpFilePath)
                            Next

                            Dim tmpIEInstallerPath As String = Path.Combine(Path.GetTempPath, "IE11-Windows6.1-x86-en-us.exe")
                            Dim tmpIEProcess As New Process
                            tmpIEProcess.StartInfo.FileName = tmpIEInstallerPath
                            tmpIEProcess.StartInfo.Arguments = "/passive /quiet"
                            tmpIEProcess.Start()
                            tmpIEProcess.WaitForExit()

                            Constants.RegData_RestartRequired.SetValue(1)
                        Else
                            Helpers.WriteLog("IE version 11 is already installed")
                            displayMessage = "Registered but not Installed"
                        End If
                    Else
                        Helpers.WriteLog("Window architecture is not 32 bits")
                        displayMessage = "Registered but not Installed"
                    End If
                Else
                    Helpers.WriteLog("Window ProductName is not Window 7")
                    displayMessage = "Registered but not Installed"
                End If

                LblMessage.Text = displayMessage
                Helpers.WriteLog(displayMessage)

                Helpers.RegisterPatchRan()
            Else
                LblMessage.Text = "Patch Not Installed - skipped"

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class