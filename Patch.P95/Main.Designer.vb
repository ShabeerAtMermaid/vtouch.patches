<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LblMessage = New System.Windows.Forms.Label()
        Me.TmrShutdown = New System.Windows.Forms.Timer(Me.components)
        Me.TmrInstall = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'LblMessage
        '
        Me.LblMessage.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMessage.Location = New System.Drawing.Point(12, 92)
        Me.LblMessage.Name = "LblMessage"
        Me.LblMessage.Size = New System.Drawing.Size(324, 23)
        Me.LblMessage.TabIndex = 1
        Me.LblMessage.Text = "Installing vTouch Pro Patch"
        Me.LblMessage.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'TmrShutdown
        '
        Me.TmrShutdown.Interval = 10000
        '
        'TmrInstall
        '
        Me.TmrInstall.Enabled = True
        Me.TmrInstall.Interval = 1000
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImage = Global.vTouch.Patch.My.Resources.Resources.logo
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ClientSize = New System.Drawing.Size(348, 114)
        Me.Controls.Add(Me.LblMessage)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "Main"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "vTouch Pro Patch"
        Me.WindowState = System.Windows.Forms.FormWindowState.Minimized
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LblMessage As System.Windows.Forms.Label
    Friend WithEvents TmrShutdown As System.Windows.Forms.Timer
    Friend WithEvents TmrInstall As System.Windows.Forms.Timer

End Class
