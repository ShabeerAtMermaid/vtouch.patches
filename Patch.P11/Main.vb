Imports Microsoft.Win32
Imports System.IO

Public Module Main

#Region "Constants"

    Private Const PatchID As Integer = 11

#End Region

    Private RegData_Patches As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro", "InstalledPatches")
    Private RegData_Patches_NEW As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\Victor Soft Updater", "InstalledPatches")
    Private RegData_CustomerID As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro\Common Settings", "CustomerId")
    Private RegData_LastError As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro", "LastError")

    Public Sub Main(ByVal args() As String)
        Try
            If Not VerifyAlreadyRun() And VerifyCustomer() And ValidateExecution() Then
                ''Sleep 2 mins in case the patch is run at the same time as the IP Change patch.
                'System.Threading.Thread.Sleep(TimeSpan.FromMinutes(2))

                Dim tmpFileInfo As New FileInfo(System.AppDomain.CurrentDomain.BaseDirectory & "\Matas AntiVirus Installer.exe")

                Dim tmpFileStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("VictorSoft.vTouch.Patch.P11.Matas AntiVirus Installer.exe")
                WriteStream(tmpFileStream, tmpFileInfo.FullName)

                Dim tmpProcess As New Process
                tmpProcess.StartInfo.FileName = tmpFileInfo.FullName
                tmpProcess.StartInfo.CreateNoWindow = True
                tmpProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden
                tmpProcess.Start()

                'The "Matas AntiVirus Installer" will register if the Patch has completed successfully
                'RegisterPatchRan()

            End If

        Catch ex As Exception
            Dim sb As New System.Text.StringBuilder
            Dim tmpEx As Exception = ex

            sb.Append("EXCEPTION**********************" & vbCrLf)
            sb.Append(ex.Message & " - " & ex.StackTrace & vbCrLf)

            tmpEx = ex.InnerException

            While Not tmpEx Is Nothing
                sb.Append("INNER EXCEPTION****************" & vbCrLf)
                sb.Append(ex.InnerException.Message & " - " & ex.InnerException.StackTrace & vbCrLf)

                tmpEx = tmpEx.InnerException

            End While

            Console.WriteLine(sb.ToString)
            Console.WriteLine()
            Console.WriteLine("Press Enter to close")

            Console.ReadLine()

        End Try
    End Sub

    Private Sub WriteStream(ByVal fileStream As Stream, ByVal filepath As String)
        If File.Exists(filepath) Then
            File.Delete(filepath)

        End If

        Dim buffer(fileStream.Length - 1) As Byte
        fileStream.Read(buffer, 0, buffer.Length)

        Dim tmpLocation = filepath
        Dim tmpStream As Stream = File.Create(tmpLocation)

        Try
            tmpStream.Write(buffer, 0, buffer.Length)

        Finally
            tmpStream.Dispose()

        End Try
    End Sub

    Private Function VerifyAlreadyRun() As Boolean      ' Checks if the patch has already been installed
        Dim InstalledPatches() As String = GetRegValue(RegData_Patches_NEW.Hive, RegData_Patches_NEW.Path, RegData_Patches_NEW.Value)

        If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
            For Each tmpPatchID As String In InstalledPatches
                If tmpPatchID = PatchID Then
                    Return True

                End If
            Next
        End If

        InstalledPatches = GetRegValue(RegData_Patches.Hive, RegData_Patches.Path, RegData_Patches.Value)

        If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
            For Each tmpPatchID As String In InstalledPatches
                If tmpPatchID = PatchID Then
                    Return True

                End If
            Next
        End If

        Return False

    End Function

    Private Function VerifyCustomer() As Boolean
        Dim tmpCustomerId As Integer = GetRegValue(RegData_CustomerID.Hive, RegData_CustomerID.Path, RegData_CustomerID.Value)

        If tmpCustomerId = 2019 Then
            Return True

        End If

        Return False

    End Function

    Private Function ValidateExecution() As Boolean
        'Dim tmpComputerList As New ArrayList
        'tmpComputerList.Add("b17493p1")
        'tmpComputerList.Add("b17493p2")
        'tmpComputerList.Add("b17507p1")
        'tmpComputerList.Add("b17507p2")
        'tmpComputerList.Add("b17515p1")
        'tmpComputerList.Add("b17515p2")
        'tmpComputerList.Add("b17523p1")
        'tmpComputerList.Add("b17523p2")
        'tmpComputerList.Add("b10243p1")
        'tmpComputerList.Add("b10243p2")
        'tmpComputerList.Add("b10529p1")
        'tmpComputerList.Add("b10529p2")
        'tmpComputerList.Add("b12522p1")
        'tmpComputerList.Add("b12522p2")
        'tmpComputerList.Add("b12526p1")
        'tmpComputerList.Add("b12526p2")

        'Return tmpComputerList.Contains(My.Computer.Name.ToLower)

        Return True

    End Function

    Private Sub RegisterPatchRan()
        Dim InstalledPatches() As String = GetRegValue(RegData_Patches_NEW.Hive, RegData_Patches_NEW.Path, RegData_Patches_NEW.Value)
        Dim tmpInstalled As ArrayList = New ArrayList

        If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
            tmpInstalled.AddRange(InstalledPatches)

        End If

        tmpInstalled.Add(PatchID.ToString)

        SetRegValue(RegData_Patches_NEW.Hive, RegData_Patches_NEW.Path, RegData_Patches_NEW.Value, tmpInstalled.ToArray(GetType(String)))

    End Sub

#Region "Registry"

    Public Function GetRegValue(ByVal whereToRead As Enums.Hive, ByVal path As String, ByVal value As String) As Object
        Dim regKey As RegistryKey
        Dim regKeyValue As Object

        'Determine the hive and open/create the designated path
        Select Case whereToRead
            Case Enums.Hive.classesroot
                regKey = Registry.ClassesRoot.CreateSubKey(path)
            Case Enums.Hive.currentconfig
                regKey = Registry.CurrentConfig.CreateSubKey(path)
            Case Enums.Hive.currentuser
                regKey = Registry.CurrentUser.CreateSubKey(path)
            Case Enums.Hive.dyndata
                regKey = Registry.DynData.CreateSubKey(path)
            Case Enums.Hive.localmachine
                regKey = Registry.LocalMachine.CreateSubKey(path)
            Case Enums.Hive.performancedata
                regKey = Registry.PerformanceData.CreateSubKey(path)
            Case Enums.Hive.users
                regKey = Registry.Users.CreateSubKey(path)
            Case Else
                regKey = Registry.LocalMachine.CreateSubKey(path)
        End Select

        regKeyValue = regKey.GetValue(value, Nothing)
        regKey.Close()

        Return regKeyValue

    End Function

    Public Sub SetRegValue(ByVal whereToWrite As Enums.Hive, ByVal path As String, ByVal value As String, ByVal data As Object)
        Dim regKey As RegistryKey

        'Determine the hive and open/create the designated path
        Select Case whereToWrite
            Case Enums.Hive.classesroot
                regKey = Registry.ClassesRoot.CreateSubKey(path)
            Case Enums.Hive.currentconfig
                regKey = Registry.CurrentConfig.CreateSubKey(path)
            Case Enums.Hive.currentuser
                regKey = Registry.CurrentUser.CreateSubKey(path)
            Case Enums.Hive.dyndata
                regKey = Registry.DynData.CreateSubKey(path)
            Case Enums.Hive.localmachine
                regKey = Registry.LocalMachine.CreateSubKey(path)
            Case Enums.Hive.performancedata
                regKey = Registry.PerformanceData.CreateSubKey(path)
            Case Enums.Hive.users
                regKey = Registry.Users.CreateSubKey(path)
            Case Else
                regKey = Registry.LocalMachine.CreateSubKey(path)
        End Select

        'Determine which datatype to write to regedit
        If TypeOf data Is Integer Then
            regKey.SetValue(value, data, RegistryValueKind.DWord)

        ElseIf TypeOf data Is String() Then
            regKey.SetValue(value, data, RegistryValueKind.MultiString)

        ElseIf TypeOf data Is Byte() Then
            regKey.SetValue(value, data, RegistryValueKind.Binary)

        Else
            regKey.SetValue(value, data)

        End If

        regKey.Close()

    End Sub

#End Region

End Module

Friend Class RegData

#Region "Attributes"

    Private _Hive As Enums.Hive
    Private _Path As String
    Private _Value As String

#End Region

#Region "Properties"

    Public ReadOnly Property Hive() As Enums.Hive
        Get
            Return Me._Hive

        End Get
    End Property

    Public ReadOnly Property Path() As String
        Get
            Return Me._Path

        End Get
    End Property

    Public ReadOnly Property Value() As String
        Get
            Return Me._Value

        End Get
    End Property

#End Region

    Public Sub New(ByVal hive As Enums.Hive, ByVal path As String, ByVal value As String)
        Me._Hive = hive
        Me._Path = path
        Me._Value = value

    End Sub


End Class