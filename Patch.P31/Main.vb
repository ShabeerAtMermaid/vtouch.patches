Imports System.IO

Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then
                Dim install As Boolean = False

                If Not Directory.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "vTouch Pro\vTouch Pro Display Services Monitor")) Then
                    install = True
                End If

                If Not install Then
                    If Not Directory.Exists("D" & Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "vTouch Pro\vTouch Pro Display Services Monitor").Substring(1)) Then
                        install = True
                    End If
                End If

                If install Then
                    'Display Services Monitor installer
                    Dim tmpMonitorInstallerPath As String = Path.Combine(Path.GetTempPath, "dsmonitor.msi")
                    Dim tmpMonitorStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.Header & "dsmonitor.msi")
                    Helpers.WriteStream(tmpMonitorStream, tmpMonitorInstallerPath)

                    Dim tmpMonitorProcess As New Process
                    tmpMonitorProcess.StartInfo.FileName = tmpMonitorInstallerPath
                    'tmpMonitorProcess.StartInfo.Arguments = "/s"
                    tmpMonitorProcess.Start()

                    tmpMonitorProcess.WaitForExit()

                    LblMessage.Text = "Installed"
                    Helpers.WriteLog("Installed")

                Else
                    LblMessage.Text = "Already installed"
                    Helpers.WriteLog("Already installed")

                    Helpers.RegisterPatchRan()
                End If
            Else
                LblMessage.Text = "Not Installed"

            End If

                Me.TmrShutdown.Interval = 2000
                Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class