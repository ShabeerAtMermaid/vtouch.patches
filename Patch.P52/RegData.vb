﻿Imports Microsoft.Win32

Public Class RegData

#Region "Attributes"

    Private _Hive As Enums.Hive
    Private _Path As String
    Private _Value As String

#End Region

#Region "Properties"

    Public ReadOnly Property Hive() As Enums.Hive
        Get
            Return Me._Hive

        End Get
    End Property

    Public ReadOnly Property Path() As String
        Get
            Return Me._Path

        End Get
    End Property

    Public ReadOnly Property Value() As String
        Get
            Return Me._Value

        End Get
    End Property

#End Region

    Public Sub New(ByVal hive As Enums.Hive, ByVal path As String, ByVal value As String)
        Me._Hive = hive
        Me._Path = path
        Me._Value = value

    End Sub

    Public Function GetValue() As Object
        Dim regKey As RegistryKey
        Dim regKeyValue As Object

        'Determine the hive and open/create the designated path
        Select Case Me.Hive
            Case Enums.Hive.ClassesRoot
                regKey = Registry.ClassesRoot.CreateSubKey(Path)
            Case Enums.Hive.CurrentConfig
                regKey = Registry.CurrentConfig.CreateSubKey(Path)
            Case Enums.Hive.CurrentUser
                regKey = Registry.CurrentUser.CreateSubKey(Path)
            Case Enums.Hive.DynData
                regKey = Registry.DynData.CreateSubKey(Path)
            Case Enums.Hive.LocalMachine
                regKey = Registry.LocalMachine.CreateSubKey(Path)
            Case Enums.Hive.PerformanceData
                regKey = Registry.PerformanceData.CreateSubKey(Path)
            Case Enums.Hive.Users
                regKey = Registry.Users.CreateSubKey(Path)
            Case Else
                regKey = Registry.LocalMachine.CreateSubKey(Path)
        End Select

        regKeyValue = regKey.GetValue(Value, Nothing)
        regKey.Close()

        Return regKeyValue

    End Function

    Public Sub SetValue(ByVal data As Object)
        Dim regKey As RegistryKey

        'Determine the hive and open/create the designated path
        Select Case Me.Hive
            Case Enums.Hive.ClassesRoot
                regKey = Registry.ClassesRoot.CreateSubKey(Path)
            Case Enums.Hive.CurrentConfig
                regKey = Registry.CurrentConfig.CreateSubKey(Path)
            Case Enums.Hive.CurrentUser
                regKey = Registry.CurrentUser.CreateSubKey(Path)
            Case Enums.Hive.DynData
                regKey = Registry.DynData.CreateSubKey(Path)
            Case Enums.Hive.LocalMachine
                regKey = Registry.LocalMachine.CreateSubKey(Path)
            Case Enums.Hive.PerformanceData
                regKey = Registry.PerformanceData.CreateSubKey(Path)
            Case Enums.Hive.Users
                regKey = Registry.Users.CreateSubKey(Path)
            Case Else
                regKey = Registry.LocalMachine.CreateSubKey(Path)
        End Select

        'Determine which datatype to write to regedit
        If TypeOf data Is Integer Then
            regKey.SetValue(Value, data, RegistryValueKind.DWord)

        ElseIf TypeOf data Is String() Then
            regKey.SetValue(Value, data, RegistryValueKind.MultiString)

        ElseIf TypeOf data Is Byte() Then
            regKey.SetValue(Value, data, RegistryValueKind.Binary)

        Else
            regKey.SetValue(Value, data)

        End If

        regKey.Close()

    End Sub

    Public Function ValueExists() As Boolean
        Dim regKey As RegistryKey
        Dim regKeyValue As Object
        Dim returnValue As Boolean

        Select Case Me.Hive
            Case Enums.Hive.ClassesRoot
                regKey = Registry.ClassesRoot.OpenSubKey(Path)
            Case Enums.Hive.CurrentConfig
                regKey = Registry.CurrentConfig.OpenSubKey(Path)
            Case Enums.Hive.CurrentUser
                regKey = Registry.CurrentUser.OpenSubKey(Path)
            Case Enums.Hive.DynData
                regKey = Registry.DynData.OpenSubKey(Path)
            Case Enums.Hive.LocalMachine
                regKey = Registry.LocalMachine.OpenSubKey(Path)
            Case Enums.Hive.PerformanceData
                regKey = Registry.PerformanceData.OpenSubKey(Path)
            Case Enums.Hive.Users
                regKey = Registry.Users.OpenSubKey(Path)
            Case Else
                regKey = Registry.LocalMachine.OpenSubKey(Path)
        End Select

        If Not regKey Is Nothing Then
            regKeyValue = regKey.GetValue(Value, Nothing)

            If regKeyValue Is Nothing Then
                returnValue = False
            Else
                returnValue = True
            End If
            regKey.Close()
        End If

        Return returnValue
    End Function

End Class