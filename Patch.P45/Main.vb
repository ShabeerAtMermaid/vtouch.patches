Imports System.IO

Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Declare Function waveOutSetVolume Lib "Winmm" (ByVal wDeviceID As Integer, ByVal dwVolume As Long) As Integer
    Private Declare Function waveOutGetVolume Lib "Winmm" (ByVal wDeviceID As Integer, ByVal dwVolume As Long) As Integer

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then
                Dim tmpMonitorInstallerPath As String = Path.Combine(Path.GetTempPath, "SetVolume.exe")
                Dim tmpMonitorStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.Header & "SetVolume.exe")
                Helpers.WriteStream(tmpMonitorStream, tmpMonitorInstallerPath)

                Dim tmpMonitorInstallerPath2 As String = Path.Combine(Path.GetTempPath, "vTouch Pro Serial Controller.dll")
                Dim tmpMonitorStream2 As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.Header & "vTouch Pro Serial Controller.dll")
                Helpers.WriteStream(tmpMonitorStream2, tmpMonitorInstallerPath2)

                Dim tmpMonitorInstallerPath3 As String = Path.Combine(Path.GetTempPath, "Serial Controller Protocol.xml")
                Dim tmpMonitorStream3 As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.Header & "Serial Controller Protocol.xml")
                Helpers.WriteStream(tmpMonitorStream3, tmpMonitorInstallerPath3)

                Dim tmpMonitorProcess As New Process
                tmpMonitorProcess.StartInfo.FileName = tmpMonitorInstallerPath
                tmpMonitorProcess.StartInfo.Arguments = "19" ' Set screen volume to 25 (19 hex)
                tmpMonitorProcess.Start()

                tmpMonitorProcess.WaitForExit()

                ' Set client wave volume to 100%
                waveOutSetVolume(0, "&Hfffffffff") ' No clue really, its big, its large, and it should beat it into 100% /SFP.

                ' Set vTouch to this also so its in sync
                My.Computer.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Mermaid technology\vTouch Pro\vTouch Pro Display\Display0", "AudioLevel", "100")


                LblMessage.Text = "Installed"
                Helpers.WriteLog("Installed")
                Helpers.RegisterPatchRan()

            Else
                LblMessage.Text = "Not Installed"

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class