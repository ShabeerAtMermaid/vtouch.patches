Imports System.IO

Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then
                'Fallback image
                Dim tmpFallbackPath As String = Path.Combine(Constants.RegData_LocalResourcePath.GetValue, "Permanent Resources\vTouchPro.jpg")
                Dim tmpFallbackStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.Header & "vTouchPro.jpg")
                Helpers.WriteStream(tmpFallbackStream, tmpFallbackPath)

                'Desktop Image
                Dim tmpBackgroundPath As String = System.Environment.GetEnvironmentVariable("SystemRoot") & "\vTouchProBG.bmp"
                Dim tmpBackgroundStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.Header & "vTouchProBG.bmp")
                Helpers.WriteStream(tmpBackgroundStream, tmpBackgroundPath)

                Dim tmpRegData_Tweak_DesktopBGImage As RegData = New RegData(Enums.Hive.CurrentUser, "Control Panel\Desktop", "Wallpaper")
                Dim tmpRegData_Tweak_DesktopConvertedBGImage As RegData = New RegData(Enums.Hive.CurrentUser, "Control Panel\Desktop", "ConvertedWallpaper")
                Dim tmpRegData_Tweak_DesktopBGImageStyle As RegData = New RegData(Enums.Hive.CurrentUser, "Control Panel\Desktop", "WallpaperStyle")

                tmpRegData_Tweak_DesktopBGImage.SetValue(tmpBackgroundPath)
                tmpRegData_Tweak_DesktopConvertedBGImage.SetValue(tmpBackgroundPath)
                tmpRegData_Tweak_DesktopBGImageStyle.SetValue("2")

                Shell(System.Environment.GetFolderPath(Environment.SpecialFolder.System) & "\RUNDLL32.EXE user32.dll,UpdatePerUserSystemParameters", AppWinStyle.Hide)

                LblMessage.Text = "Installed"
                Helpers.WriteLog("Installed")

                Helpers.RegisterPatchRan()

            Else
                LblMessage.Text = "Not Installed"

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class