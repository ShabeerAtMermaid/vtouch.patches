Imports System.IO
Imports Microsoft.Win32
Imports System.ServiceProcess
Imports System.Management


Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() Then
                If Helpers.VerifyCustomer() Then
                    Dim tmpFileList As New ArrayList
                    tmpFileList.Add("pxe_wol_ac.exe")
                    For Each tmpFileName As String In tmpFileList
                        Dim tmpFilePath As String = Path.Combine(Path.GetTempPath, tmpFileName)
                        Dim tmpFileStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.Header & tmpFileName)
                        Helpers.WriteStream(tmpFileStream, tmpFilePath)
                    Next
                    Try
                        Dim tmpBIOSUpateInstallerPath As String = Path.Combine(Path.GetTempPath, "pxe_wol_ac.exe")
                        Dim tmpProcess As New Process
                        tmpProcess.StartInfo.FileName = tmpBIOSUpateInstallerPath
                        tmpProcess.StartInfo.Arguments = "/passive /quiet"
                        tmpProcess.Start()
                        tmpProcess.WaitForExit()
                        Helpers.WriteLog("BIOS has been Updated")
                        Try
                            Dim pathcommit As String = "cmd.exe /c ""ewfmgr c: -commit"""
                            Shell(pathcommit, AppWinStyle.NormalFocus, True)
                        Catch ex As Exception
                            Helpers.WriteLog(ex)
                        End Try
                    Catch ex1 As Exception
                        Helpers.WriteLog(ex1)
                    End Try

                    LblMessage.Text = "Installed"
                    Helpers.WriteLog("Installed")
                    Constants.RegData_RestartRequired.SetValue(1)
                End If
                Helpers.RegisterPatchRan()
            Else
                LblMessage.Text = "Patch Not Installed - skipped"
            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class
