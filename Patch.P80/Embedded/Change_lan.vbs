Const ssfCONTROLS = 3 
 
 sConnectionName = "Local Area Connection" 
 
 sEnableVerb = "En&able" 
 sDisableVerb = "Disa&ble" 
 
 set shellApp = createobject("shell.application") 
 set oControlPanel = shellApp.Namespace(ssfCONTROLS) 
 
 set oNetConnections = nothing 
 for each folderitem in oControlPanel.items 
 	if folderitem.name = "Network Connections" then 
 		set oNetConnections = folderitem.getfolder: exit for 
 	end if 
 next 
 if oNetConnections is nothing then 
 	msgbox "Couldn't find 'Network Connections' folder" 
 	wscript.quit 
 end if 
 set oLanConnection = nothing 
 for each folderitem in oNetConnections.items 
 	if lcase(folderitem.name) = lcase(sConnectionName) then 
 		set oLanConnection = folderitem: exit for 
 	end if 
 next 
 if oLanConnection is nothing then 
 	msgbox "Couldn't find '" & sConnectionName & "' item" 
 	wscript.quit 
 end if 
 
 bEnabled = true 
 set oEnableVerb = nothing 
 set oDisableVerb = nothing 
 for each verb in oLanConnection.verbs 
 	if verb.name = sEnableVerb then 
 		set oEnableVerb = verb 
 		bEnabled = false 
 	end if 
 	if verb.name = sDisableVerb then 
 		set oDisableVerb = verb 
 	end if 
 next 
 if bEnabled then 
 	oDisableVerb.DoIt()
 else 
 	oEnableVerb.DoIt 
 end if 
 
 wscript.sleep 500