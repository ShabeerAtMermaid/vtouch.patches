﻿Imports System.IO
Imports System.ServiceProcess

Public Class Helpers

    Public Shared Sub WriteStream(ByVal FileStream As Stream, ByVal Filename As String)
        Dim buffer(FileStream.Length - 1) As Byte
        FileStream.Read(buffer, 0, buffer.Length)
        Dim tmpStream As Stream = File.Create(Filename)
        Try
            tmpStream.Write(buffer, 0, buffer.Length)
        Finally
            tmpStream.Dispose()
        End Try

    End Sub

    Public Shared Sub WriteLog(ByVal message As String)
        Try
            Dim ev As New EventLog("Application", ".", "vTouch Pro Patch")
            ev.WriteEntry("vTouch Pro Patch " & Constants.PatchID & vbCrLf & _
                          message)

        Catch ex As Exception
            Constants.RegData_LastError.SetValue(ex.Message & " - " & ex.StackTrace)

        End Try
    End Sub

    Public Shared Sub WriteLog(ByVal exception As Exception)
        Try
            Dim tmpMessage As String = exception.Message & vbCrLf
            tmpMessage &= exception.StackTrace & vbCrLf

            Dim tmpException As Exception = exception.InnerException

            While Not tmpException Is Nothing
                tmpMessage &= vbCrLf
                tmpMessage &= "Caused by:" & vbCrLf
                tmpMessage = tmpException.Message & vbCrLf
                tmpMessage &= tmpException.StackTrace & vbCrLf

                tmpException = tmpException.InnerException

            End While

            Dim ev As New EventLog("Application", ".", "vTouch Pro Patch")
            ev.WriteEntry("vTouch Pro Patch " & Constants.PatchID & vbCrLf & _
                          tmpMessage)

        Catch ex As Exception
            Constants.RegData_LastError.SetValue(ex.Message & " - " & ex.StackTrace)

        End Try
    End Sub

    Public Shared Sub WriteViruslog(ByVal contents As String)
        File.AppendAllText("c:\temp\Virus_log.txt", System.DateTime.Now.ToString() & ": " & contents & Environment.NewLine)
    End Sub

    Public Shared Sub EnableFirewall()
        Try
            Dim Proc As Process = New Process
            Dim top As String = "netsh.exe"
            Proc.StartInfo.Arguments = ("firewall set opmode enable")
            Proc.StartInfo.FileName = top
            Proc.StartInfo.UseShellExecute = False
            Proc.StartInfo.RedirectStandardOutput = True
            Proc.StartInfo.CreateNoWindow = True
            Proc.Start()
            Proc.WaitForExit()
            WriteViruslog("FireWall has been Enabled successfully")
        Catch ex As Exception
            WriteLog(ex)
        End Try
    End Sub
    Public Shared Sub DisableFirewall()
        Try
            Dim Proc As Process = New Process
            Dim top As String = "netsh.exe"
            Proc.StartInfo.Arguments = ("firewall set opmode disable")
            Proc.StartInfo.FileName = top
            Proc.StartInfo.UseShellExecute = False
            Proc.StartInfo.RedirectStandardOutput = True
            Proc.StartInfo.CreateNoWindow = True
            Proc.Start()
            Proc.WaitForExit()
            WriteViruslog("FireWall has been disabled successfully")
        Catch ex As Exception
            WriteLog(ex)
        End Try
    End Sub

    Public Shared Sub StopInternet()
        Try
            Shell("net stop dhcp")   'Run this to stop internet connection
            Dim sc As New ServiceController("DHCP Client")
            Try
                If (sc.Status = ServiceControllerStatus.Running) Then
                    sc.Stop()
                    sc.Refresh()
                    ' WriteViruslog("Lan has been disabled successfully")
                End If
            Catch ex1 As Exception
                WriteLog(ex1)
            End Try
            WriteViruslog("Lane has been disabled successfully")
        Catch ex As Exception
            WriteLog(ex)
            Dim sc As New ServiceController("DHCP Client")
            Try
                If (sc.Status = ServiceControllerStatus.Running) Then
                    sc.Stop()
                    sc.Refresh()
                    WriteViruslog("Lan has been disabled successfully")
                End If
            Catch ex1 As Exception
                WriteLog(ex1)
            End Try
        End Try
        Threading.Thread.Sleep(5 * 1000)
    End Sub
    Public Shared Sub StartInternet()
        Try
            Shell("net start dhcp")  'Run this to start internet connection
            Dim sc As New ServiceController("DHCP Client")
            Try
                If (sc.Status = ServiceControllerStatus.Stopped) Then
                    sc.Start()
                    sc.Refresh()
                    'WriteViruslog("Lan has been Enabled successfully")
                End If
            Catch ex1 As Exception
                WriteLog(ex1)
            End Try
            WriteViruslog("Lan has been Enabled successfully")
        Catch ex As Exception
            WriteLog(ex)
            Dim sc As New ServiceController("DHCP Client")
            Try
                If (sc.Status = ServiceControllerStatus.Stopped) Then
                    sc.Start()
                    sc.Refresh()
                    WriteViruslog("Lan has been Enabled successfully")
                End If
            Catch ex1 As Exception
                WriteLog(ex1)
            End Try
        End Try
        Threading.Thread.Sleep(5 * 1000)
    End Sub

    Public Shared Sub RemoveDownandup()
        Try

            Try
                WriteViruslog("Down and up removal starting")
                Dim tmpDownandupPath As String = Path.Combine(Path.GetTempPath, "D.exe")
                Dim regpathcommit As String = tmpDownandupPath & " /S /NOSILENTREBOOT /LOG=c:\temp\downadup_removal_log.txt"
                'Dim regpathcommit As String = tmpDownandupPath & " /S /NOSILENTREBOOT /LOG=" & Path.Combine(AppDomain.CurrentDomain.BaseDirectory(), "downadup_removal_log.txt")
                Shell(regpathcommit, AppWinStyle.NormalFocus, True)
                File.AppendAllText("c:\temp\Virus_log.txt", "Down and up has been removed successfully" & Environment.NewLine)
                WriteViruslog("Down and up exe has been run successfully and removed the virus")
            Catch ex As Exception
                Helpers.WriteLog(ex)
                Dim tmpDownandupPath As String = Path.Combine(Path.GetTempPath, "D.exe")
                Dim tmpDownandupPathProcess As New Process
                tmpDownandupPathProcess.StartInfo.FileName = tmpDownandupPath
                tmpDownandupPathProcess.StartInfo.Arguments = "/S /NOSILENTREBOOT /LOG=c:\temp\downadup_removal_log.txt"
                tmpDownandupPathProcess.Start()
                tmpDownandupPathProcess.WaitForExit()
                WriteViruslog("Down and up exe has been run successfully and removed the virus")
            End Try

            Threading.Thread.Sleep(10 * 1000)
        Catch ex As Exception
            WriteLog(ex)
        End Try
    End Sub

    Public Shared Sub commitstate()
        Try
            Dim regpathcommit As String = "cmd.exe /c ""ewfmgr c: -commit"""
            Shell(regpathcommit, AppWinStyle.NormalFocus, True)
        Catch ex As Exception
            WriteLog(ex)
        End Try
    End Sub

    Public Shared Function HasBeenExecuted() As Boolean      ' Checks if the patch has already been installed
        Try
            Dim InstalledPatches() As String
            Dim TmpPatchID As String
            InstalledPatches = Constants.RegData_Patches.GetValue
            Dim TmpInstalled As ArrayList = New ArrayList
            If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
                For Each TmpPatchID In InstalledPatches
                    TmpInstalled.Add(TmpPatchID)
                    If TmpPatchID = Constants.PatchID.ToString() Then
                        WriteLog("Patch already installed")
                        Return True
                    End If
                Next
            End If

            WriteLog("Patch not executed previously")
            Return False

        Catch ex As Exception
            WriteLog(ex)

            Return False

        End Try
    End Function

    Public Shared Function VerifyCustomer() As Boolean
        Try
            If Constants.AcceptedCustomerIDs.Length = 0 Then Return True

            Dim tmpCustomerID As Integer = Constants.RegData_CustomerID.GetValue

            If Array.IndexOf(Constants.AcceptedCustomerIDs, tmpCustomerID) <> -1 Then
                WriteLog("Patch valid for CustomerID " & tmpCustomerID)

                Return True

            Else
                WriteLog("Patch not valid for CustomerID " & tmpCustomerID)

                Return False

            End If

        Catch ex As Exception
            WriteLog(ex)

            Return False

        End Try
    End Function

    Public Shared Sub RegisterPatchRan()
        Try
            Dim InstalledPatches() As String

            InstalledPatches = Constants.RegData_Patches.GetValue
            Dim TmpInstalled As ArrayList = New ArrayList
            If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
                TmpInstalled.AddRange(InstalledPatches)
            End If
            TmpInstalled.Add(Constants.PatchID.ToString)
            Constants.RegData_Patches.SetValue(TmpInstalled.ToArray(GetType(String)))

        Catch ex As Exception
            WriteLog(ex)

        End Try
    End Sub

End Class
