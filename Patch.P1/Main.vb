Imports Microsoft.Win32
Imports System.IO

Public Class Main

    Private Const PatchID As Integer = 1
    Private RegData_Patches As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro", "InstalledPatches")
    Private RegData_CustomerID As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro\Common Settings", "CustomerId")


    Private Sub WriteStream(ByVal FileStream As Stream, ByVal Filename As String)
        Dim buffer(FileStream.Length - 1) As Byte
        FileStream.Read(buffer, 0, buffer.Length)
        Dim TmpLocation = System.Environment.GetFolderPath(Environment.SpecialFolder.System) & "\" & Filename
        Dim tmpStream As Stream = File.Create(TmpLocation)
        Try
            tmpStream.Write(buffer, 0, buffer.Length)
        Finally
            tmpStream.Dispose()
        End Try

    End Sub

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not VerifyAlreadyRun() Then
                LblStatus.Text = "Loading files.."

                Dim TmpFileStream As Stream
                Dim FilePath As String

                Dim FileList(15) As String
                FileList(0) = "DSKernel2.dll"
                FileList(1) = "lfbmp14N.dll"
                FileList(2) = "LFCMP14N.DLL"
                FileList(3) = "lffax14N.dll"
                FileList(4) = "lfgif14N.dll"
                FileList(5) = "lfpcd14N.dll"
                FileList(6) = "Lfpct14N.dll"
                FileList(7) = "Lfpng14N.dll"
                FileList(8) = "lftif14n.dll"
                FileList(9) = "LMVRGBxf.dll"
                FileList(10) = "LMVRsz2.dll"
                FileList(11) = "LMVTOvLy2.dll"
                FileList(12) = "LTDIS14N.dll"
                FileList(13) = "ltfil14N.DLL"
                FileList(14) = "ltkrn14N.dll"
                FileList(15) = "LTStlImgRd.dll"

                For Each FileName As String In FileList
                    FilePath = System.Environment.GetFolderPath(Environment.SpecialFolder.System) & "\" & FileName
                    If File.Exists(FilePath) Then File.Delete(FilePath)
                Next

                LblStatus.Text = "Writing files.."

                For Each FileName As String In FileList
                    TmpFileStream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("VictorSoft.vTouch.Patch.P1." & FileName)
                    WriteStream(TmpFileStream, FileName)
                Next

                LblStatus.Text = "Registering files.."

                Dim tmpProcess As New Process()
                For Each FileName As String In FileList
                    tmpProcess.StartInfo.FileName = System.Environment.GetFolderPath(Environment.SpecialFolder.System) & "\regsvr32.exe"
                    tmpProcess.StartInfo.Arguments = "/s " & FileName
                    tmpProcess.StartInfo.CreateNoWindow = True
                    tmpProcess.Start()
                Next
                RegisterPatchRan()
            End If
            LblStatus.Text = "All done."
        Catch ex As Exception
            LblStatus.Text = "Exception: " & ex.Message
        End Try
        Me.Close()
    End Sub

    Private Function VerifyAlreadyRun() As Boolean      ' Checks if the patch has already been installed
        Dim InstalledPatches() As String
        Dim TmpPatchID As String
        InstalledPatches = GetRegValue(Enums.Hive.localmachine, RegData_Patches.Path, RegData_Patches.Value)
        Dim TmpInstalled As ArrayList = New ArrayList
        If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
            For Each TmpPatchID In InstalledPatches
                TmpInstalled.Add(TmpPatchID)
                If TmpPatchID = PatchID Then
                    Return True
                End If
            Next
        End If
        ' TmpInstalled.Add(PatchID.ToString)
        ' SetRegValue(Enums.Hive.localmachine, RegData_Patches.Path, RegData_Patches.Value, TmpInstalled.ToArray(GetType(String)))
        Return False
    End Function
    Private Sub RegisterPatchRan()
        Dim InstalledPatches() As String
        Dim TmpPatchID As String
        InstalledPatches = GetRegValue(Enums.Hive.localmachine, RegData_Patches.Path, RegData_Patches.Value)
        Dim TmpInstalled As ArrayList = New ArrayList
        If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
            TmpInstalled.AddRange(InstalledPatches)
        End If
        TmpInstalled.Add(PatchID.ToString)
        SetRegValue(Enums.Hive.localmachine, RegData_Patches.Path, RegData_Patches.Value, TmpInstalled.ToArray(GetType(String)))
    End Sub

#Region "Registry"
    Public Shared Function GetRegValue(ByVal whereToRead As Enums.Hive, ByVal path As String, ByVal value As String) As Object
        Dim regKey As RegistryKey
        Dim regKeyValue As Object

        'Determine the hive and open/create the designated path
        Select Case whereToRead
            Case Enums.Hive.classesroot
                regKey = Registry.ClassesRoot.CreateSubKey(path)
            Case Enums.Hive.currentconfig
                regKey = Registry.CurrentConfig.CreateSubKey(path)
            Case Enums.Hive.currentuser
                regKey = Registry.CurrentUser.CreateSubKey(path)
            Case Enums.Hive.dyndata
                regKey = Registry.DynData.CreateSubKey(path)
            Case Enums.Hive.localmachine
                regKey = Registry.LocalMachine.CreateSubKey(path)
            Case Enums.Hive.performancedata
                regKey = Registry.PerformanceData.CreateSubKey(path)
            Case Enums.Hive.users
                regKey = Registry.Users.CreateSubKey(path)
            Case Else
                regKey = Registry.LocalMachine.CreateSubKey(path)
        End Select

        regKeyValue = regKey.GetValue(value, Nothing)
        regKey.Close()
        Return regKeyValue
    End Function

    Public Shared Sub SetRegValue(ByVal whereToWrite As Enums.Hive, ByVal path As String, ByVal value As String, ByVal data As Object)
        Dim regKey As RegistryKey

        'Determine the hive and open/create the designated path
        Select Case whereToWrite
            Case Enums.Hive.classesroot
                regKey = Registry.ClassesRoot.CreateSubKey(path)
            Case Enums.Hive.currentconfig
                regKey = Registry.CurrentConfig.CreateSubKey(path)
            Case Enums.Hive.currentuser
                regKey = Registry.CurrentUser.CreateSubKey(path)
            Case Enums.Hive.dyndata
                regKey = Registry.DynData.CreateSubKey(path)
            Case Enums.Hive.localmachine
                regKey = Registry.LocalMachine.CreateSubKey(path)
            Case Enums.Hive.performancedata
                regKey = Registry.PerformanceData.CreateSubKey(path)
            Case Enums.Hive.users
                regKey = Registry.Users.CreateSubKey(path)
            Case Else
                regKey = Registry.LocalMachine.CreateSubKey(path)
        End Select

        'Determine which datatype to write to regedit
        If TypeOf data Is Integer Then
            regKey.SetValue(value, data, RegistryValueKind.DWord)

        ElseIf TypeOf data Is String() Then
            regKey.SetValue(value, data, RegistryValueKind.MultiString)

        ElseIf TypeOf data Is Byte() Then
            regKey.SetValue(value, data, RegistryValueKind.Binary)

        Else
            regKey.SetValue(value, data)

        End If

        regKey.Close()

    End Sub

#End Region
End Class

Friend Class RegData

#Region "Attributes"

    Private _Hive As Enums.Hive
    Private _Path As String
    Private _Value As String

#End Region

#Region "Properties"

    Public ReadOnly Property Hive() As Enums.Hive
        Get
            Return Me._Hive

        End Get
    End Property

    Public ReadOnly Property Path() As String
        Get
            Return Me._Path

        End Get
    End Property

    Public ReadOnly Property Value() As String
        Get
            Return Me._Value

        End Get
    End Property

#End Region

    Public Sub New(ByVal hive As Enums.Hive, ByVal path As String, ByVal value As String)
        Me._Hive = hive
        Me._Path = path
        Me._Value = value

    End Sub

End Class
