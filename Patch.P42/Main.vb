Imports System.IO

Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then
                Dim tmpProgramDirectory As String = Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "vTouch Pro\vTouch Pro Ticker Client Module")

                If Not Directory.Exists(tmpProgramDirectory) Then Directory.CreateDirectory(tmpProgramDirectory)

                Dim tmpFileList As New ArrayList
                tmpFileList.Add("GenuineChannels.dll")
                tmpFileList.Add("ICSharpCode.SharpZipLib.dll")
                tmpFileList.Add("Interop.MERMAIDSOUNDLib.dll")
                tmpFileList.Add("Interop.VSLIVELib.dll")
                tmpFileList.Add("mermaid.BaseObjects.dll")
                tmpFileList.Add("mermaid.BaseObjects.pdb")
                tmpFileList.Add("mermaid.LogSystem.Shared.dll")
                tmpFileList.Add("mermaid.LogSystem.Shared.pdb")
                tmpFileList.Add("mermaid.RegistryEditor.dll")
                tmpFileList.Add("mermaid.RegistryEditor.pdb")
                tmpFileList.Add("vTouch.API.dll")
                tmpFileList.Add("vTouch.API.pdb")
                tmpFileList.Add("vTouch.Display.Settings.dll")
                tmpFileList.Add("vTouch.Display.Settings.pdb")
                tmpFileList.Add("vTouch.DisplayServices.Model.dll")
                tmpFileList.Add("vTouch.DisplayServices.Model.pdb")
                tmpFileList.Add("vTouch.DisplayServices.PowerScheduler.dll")
                tmpFileList.Add("vTouch.DisplayServices.PowerScheduler.pdb")
                tmpFileList.Add("vTouch.PowerManagement.dll")
                tmpFileList.Add("vTouch.PowerManagement.pdb")
                tmpFileList.Add("vTouch.Shared.dll")
                tmpFileList.Add("vTouch.Shared.pdb")
                tmpFileList.Add("vTouch.TickerModule.Client.Console.exe")
                tmpFileList.Add("vTouch.TickerModule.Client.Console.pdb")
                tmpFileList.Add("vTouch.TickerModule.Client.dll")
                tmpFileList.Add("vTouch.TickerModule.Shared.dll")
                tmpFileList.Add("vTouch.TickerModule.Shared.pdb")

                For Each tmpFileName As String In tmpFileList
                    Dim tmpFilePath As String = Path.Combine(tmpProgramDirectory, tmpFileName)
                    Dim tmpFileStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.Header & tmpFileName)
                    Helpers.WriteStream(tmpFileStream, tmpFilePath)
                Next

                Helpers.RegisterPatchRan()

                LblMessage.Text = "Installed"
                Helpers.WriteLog("Installed")

            Else
                LblMessage.Text = "Not installed"
                Helpers.WriteLog("Not installed")

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class