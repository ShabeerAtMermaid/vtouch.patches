Imports System.IO
Imports Microsoft.Win32
Imports System.ServiceProcess
Imports System.Management


Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub
    Public Function HextoByte(ByVal hexstringtoByte As String) As Byte()
        Dim hex As String = hexstringtoByte '"8ad0fa189fd976bf801f410d3e1bd38fe23840068981ba3fa32c39797ad05a59"
        Dim raw As Byte() = New Byte((hex.Length / 2) - 1) {}
        Dim i As Integer
        For i = 0 To raw.Length - 1
            raw(i) = Convert.ToByte(hex.Substring((i * 2), 2), &H10)
        Next i
        Return raw
    End Function
    'Sub Main()
    '    Dim StringVariable As String = "vTouch-12"
    '    Dim ByteArray() As Byte
    '    ByteArray = System.Text.Encoding.ASCII.GetBytes(StringVariable)
    '    'For Each b As Byte In ByteArray
    '    '    Console.WriteLine(b)
    '    'Next

    '    Dim KeyPath As String = "HKEY_LOCAL_MACHINE\Software\TeamViewer\Version6\"
    '    'Dim BinaryData As Byte() = {8a,d0,fa,18,9f,d9,76,bf,80,1f,41,0d,3e,1b,d3,8f,e2,38,40,06,89,81,ba,3f,a3,2c,39,79,7a,d0,5a,59}

    '    Dim raw As Byte() = HextoByte("8ad0fa189fd976bf801f410d3e1bd38fe23840068981ba3fa32c39797ad05a59")
    '    'Dim byteArry() As Byte =
    '    My.Computer.Registry.SetValue(KeyPath, "SecurityPasswordAES1", raw, Microsoft.Win32.RegistryValueKind.Binary)
    '    Console.Read()
    'End Sub
    Public Sub StopService(ByVal PC As String)
        Dim obj As ManagementObject
        Dim inParams, outParams As ManagementBaseObject
        Dim Result As Integer

        ' we can only disable the service and wait
        ' until the next reboot to stop it

        obj = New ManagementObject("\\" & PC & "\root\cimv2:Win32_Service.Name='TeamViewer7'")

        ' change the Start Mode to Disabled
        If obj("StartMode").ToString <> "Disabled" Then
            ' Get an input parameters object for this method
            inParams = obj.GetMethodParameters("ChangeStartMode")
            inParams("StartMode") = "Disabled"

            ' do it!
            outParams = obj.InvokeMethod("ChangeStartMode", inParams, Nothing)
            Result = Convert.ToInt32(outParams("returnValue"))
            If Result <> 0 Then
                Throw New Exception("ChangeStartMode method error code " & Result)
            End If
        End If
    End Sub
    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then
                Dim tempOSPatchName As String = String.Empty
                If (Environment.OSVersion.Platform = PlatformID.Win32NT Or Environment.OSVersion.Platform = PlatformID.Win32S) And Environment.OSVersion.Version.Major = 5 And Environment.OSVersion.Version.Minor = 1 Then
                    'for XP
                    Select Case Globalization.CultureInfo.CurrentCulture.ToString
                        Case "da-DK"
                            tempOSPatchName = "WindowsXP-KB4012598-x86-Embedded-Custom-DAN.exe"
                        Case "en-GB"
                            tempOSPatchName = "windowsxp-kb4012598-x86-embedded-custom-enu_8f2c266f83a7e1b100ddb9acd4a6a3ab5ecd4059.exe"
                    End Select

                ElseIf (Environment.OSVersion.Platform = PlatformID.Win32NT Or Environment.OSVersion.Platform = PlatformID.Win32S) And Environment.OSVersion.Version.Major = 6 And Environment.OSVersion.Version.Minor = 1 Then
                    'for Window7

                    Dim tmpRegData_CentralProcessor As RegData = New RegData(Enums.Hive.LocalMachine, Constants.RegPathCentralProcessor, "Identifier")
                    Dim tmpCentralProcessor As String = tmpRegData_CentralProcessor.GetValue

                    If Not String.IsNullOrEmpty(tmpCentralProcessor) And tmpCentralProcessor.Contains("x86") Then
                        tempOSPatchName = "windows6.1-kb3212646-x86_1852348a302cd587278400e936e2daf0321dc05d.msu"
                    Else
                        tempOSPatchName = "windows6.1-kb3212646-x64_a94cf69326099fb121cdd7daf9dfc558f740afb8.msu"
                    End If
                Else
                    Helpers.WriteLog("Current OS is not supported, only XP and Window 7")
                End If

                If Not String.IsNullOrEmpty(tempOSPatchName) Then
                    Dim tmpFilePath As String = Path.Combine(Path.GetTempPath, tempOSPatchName)

                    Dim tmpFileStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.Header & tempOSPatchName)
                    Helpers.WriteStream(tmpFileStream, tmpFilePath)

                    Dim tmpWannaCryInstallerPath As String = Path.Combine(Path.GetTempPath, tempOSPatchName)
                    Dim tmpWannaCryProcess As New Process
                    tmpWannaCryProcess.StartInfo.FileName = tmpWannaCryInstallerPath
                    tmpWannaCryProcess.StartInfo.Arguments = "/passive /quiet"
                    tmpWannaCryProcess.Start()
                    tmpWannaCryProcess.WaitForExit()
                    'End If

                    Try
                        Dim regpathcommit As String = "cmd.exe /c ""ewfmgr c: -commit"""
                        Shell(regpathcommit, AppWinStyle.NormalFocus, True)
                    Catch ex As Exception
                        Helpers.WriteLog(ex)
                    End Try
                End If

                LblMessage.Text = "Installed"
                Helpers.WriteLog("Installed")
                Constants.RegData_RestartRequired.SetValue(1)
                Helpers.RegisterPatchRan()

            Else
                LblMessage.Text = "Patch Not Installed - skipped"

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class