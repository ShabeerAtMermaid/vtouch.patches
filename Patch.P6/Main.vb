Imports Microsoft.Win32
Imports System.IO
Public Class Main
#Region "Constants"

    Private Const PatchID As Integer = 6
    Private RegData_PermanentResourcePath As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro\Common Settings", "LocalResourcePath")
    Private Hive As String = Enums.Hive.localmachine
    Private Path As String = "SYSTEM\CurrentControlSet\Services\"
    Private RegData_Patches As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro", "InstalledPatches")
    Private RegData_CustomerID As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro\Common Settings", "CustomerId")
    Private RegData_TotalDisplays As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro\vTouch Pro Display", "ActiveScreens")

#End Region

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not VerifyAlreadyRun() Then
            ' Perform functions here
            Dim CustomerID As Integer = GetRegValue(Enums.Hive.localmachine, RegData_CustomerID.Path, RegData_CustomerID.Value)
            If Not CustomerID = Nothing Then
                If CustomerID = 2018 Then ' Only run this patch for click
                    Dim TmpTotal As Integer = GetRegValue(Enums.Hive.localmachine, RegData_TotalDisplays.Path, RegData_TotalDisplays.Value)
                    Dim TmpCount As Integer = 0
                    While TmpCount <= TmpTotal
                        Dim TmpPermanentResourcesPath = GetRegValue(Enums.Hive.localmachine, RegData_PermanentResourcePath.Path, RegData_PermanentResourcePath.Value) & "PermanentResources\"
                        TmpPermanentResourcesPath = TmpPermanentResourcesPath & TmpCount & "\"
                        If System.IO.Directory.Exists(TmpPermanentResourcesPath) Then
                            Dim TmpFile As String
                            For Each TmpFile In System.IO.Directory.GetFiles(TmpPermanentResourcesPath)
                                System.IO.File.Delete(TmpFile)
                            Next TmpFile
                        End If
                        TmpCount += 1
                    End While
                    RegisterPatchRan()
                End If
            End If
            End If
        Me.Close()

    End Sub

    Private Function VerifyAlreadyRun() As Boolean      ' Checks if the patch has already been installed
        Dim InstalledPatches() As String
        Dim TmpPatchID As String
        InstalledPatches = GetRegValue(Enums.Hive.localmachine, RegData_Patches.Path, RegData_Patches.Value)
        Dim TmpInstalled As ArrayList = New ArrayList
        If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
            For Each TmpPatchID In InstalledPatches
                TmpInstalled.Add(TmpPatchID)
                If TmpPatchID = PatchID Then
                    Return True
                End If
            Next
        End If
        '  TmpInstalled.Add(PatchID.ToString)
        '  SetRegValue(Enums.Hive.localmachine, RegData_Patches.Path, RegData_Patches.Value, TmpInstalled.ToArray(GetType(String)))
        Return False
    End Function
    Private Sub RegisterPatchRan()
        Dim InstalledPatches() As String
        Dim TmpPatchID As String
        InstalledPatches = GetRegValue(Enums.Hive.localmachine, RegData_Patches.Path, RegData_Patches.Value)
        Dim TmpInstalled As ArrayList = New ArrayList
        If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
            TmpInstalled.AddRange(InstalledPatches)
        End If
        TmpInstalled.Add(PatchID.ToString)
        SetRegValue(Enums.Hive.localmachine, RegData_Patches.Path, RegData_Patches.Value, TmpInstalled.ToArray(GetType(String)))
    End Sub
#Region "Services"
    Private Sub DisableService(ByVal name As String)
        SetRegValue(Hive, Path & name, "Start", 4) ' Disabled
        Dim TmpControl As Control() = Me.Controls.Find("Lbl" & name, False)
        If TmpControl.Length = 1 Then
            TmpControl(0).ForeColor = Color.DarkGreen
            TmpControl = Me.Controls.Find("Chk" & name, False)
            If TmpControl.Length = 1 Then TmpControl(0).Visible = False
        End If

    End Sub
    Private Sub EnableService(ByVal name As String)
        SetRegValue(Hive, Path & name, "Start", 2) ' Automatic
    End Sub

#End Region
#Region "Registry"
    Public Shared Function GetRegValue(ByVal whereToRead As Enums.Hive, ByVal path As String, ByVal value As String) As Object
        Dim regKey As RegistryKey
        Dim regKeyValue As Object

        'Determine the hive and open/create the designated path
        Select Case whereToRead
            Case Enums.Hive.classesroot
                regKey = Registry.ClassesRoot.CreateSubKey(path)
            Case Enums.Hive.currentconfig
                regKey = Registry.CurrentConfig.CreateSubKey(path)
            Case Enums.Hive.currentuser
                regKey = Registry.CurrentUser.CreateSubKey(path)
            Case Enums.Hive.dyndata
                regKey = Registry.DynData.CreateSubKey(path)
            Case Enums.Hive.localmachine
                regKey = Registry.LocalMachine.CreateSubKey(path)
            Case Enums.Hive.performancedata
                regKey = Registry.PerformanceData.CreateSubKey(path)
            Case Enums.Hive.users
                regKey = Registry.Users.CreateSubKey(path)
            Case Else
                regKey = Registry.LocalMachine.CreateSubKey(path)
        End Select

        regKeyValue = regKey.GetValue(value, Nothing)
        regKey.Close()
        Return regKeyValue
    End Function

    Public Shared Sub SetRegValue(ByVal whereToWrite As Enums.Hive, ByVal path As String, ByVal value As String, ByVal data As Object)
        Dim regKey As RegistryKey

        'Determine the hive and open/create the designated path
        Select Case whereToWrite
            Case Enums.Hive.classesroot
                regKey = Registry.ClassesRoot.CreateSubKey(path)
            Case Enums.Hive.currentconfig
                regKey = Registry.CurrentConfig.CreateSubKey(path)
            Case Enums.Hive.currentuser
                regKey = Registry.CurrentUser.CreateSubKey(path)
            Case Enums.Hive.dyndata
                regKey = Registry.DynData.CreateSubKey(path)
            Case Enums.Hive.localmachine
                regKey = Registry.LocalMachine.CreateSubKey(path)
            Case Enums.Hive.performancedata
                regKey = Registry.PerformanceData.CreateSubKey(path)
            Case Enums.Hive.users
                regKey = Registry.Users.CreateSubKey(path)
            Case Else
                regKey = Registry.LocalMachine.CreateSubKey(path)
        End Select

        'Determine which datatype to write to regedit
        If TypeOf data Is Integer Then
            regKey.SetValue(value, data, RegistryValueKind.DWord)

        ElseIf TypeOf data Is String() Then
            regKey.SetValue(value, data, RegistryValueKind.MultiString)

        ElseIf TypeOf data Is Byte() Then
            regKey.SetValue(value, data, RegistryValueKind.Binary)

        Else
            regKey.SetValue(value, data)

        End If

        regKey.Close()

    End Sub
#End Region
End Class
Friend Class RegData

#Region "Attributes"

    Private _Hive As Enums.Hive
    Private _Path As String
    Private _Value As String

#End Region

#Region "Properties"

    Public ReadOnly Property Hive() As Enums.Hive
        Get
            Return Me._Hive

        End Get
    End Property

    Public ReadOnly Property Path() As String
        Get
            Return Me._Path

        End Get
    End Property

    Public ReadOnly Property Value() As String
        Get
            Return Me._Value

        End Get
    End Property

#End Region

    Public Sub New(ByVal hive As Enums.Hive, ByVal path As String, ByVal value As String)
        Me._Hive = hive
        Me._Path = path
        Me._Value = value

    End Sub

End Class

