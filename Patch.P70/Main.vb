Imports System.IO

Public Class Main
    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then
                'HKEY_CURRENT_USER\AppEvents\Schemes\Apps\.Default\SystemExit\.Default
                Try
                    Dim tmpRegData_SystemExit As RegData = New RegData(Enums.Hive.CurrentUser, "AppEvents\Schemes\Apps\.Default\SystemExit\.Current", "")
                    If (String.IsNullOrEmpty(tmpRegData_SystemExit.GetValue().ToString())) Then
                        Helpers.WriteLog("SystemExit default Sound value is" & tmpRegData_SystemExit.GetValue().ToString())
                    End If

                    Dim datalist As String = "%SystemRoot%\media\Windows XP Shutdown1.wav"
                    tmpRegData_SystemExit.SetValue(datalist)
                    Helpers.WriteLog("SystemExit default Sound value " & datalist & " has been set")

                Catch ex As Exception
                    Helpers.WriteLog(ex)
                End Try

                Try
                    Dim tmpRegData_SystemStart As RegData = New RegData(Enums.Hive.CurrentUser, "AppEvents\Schemes\Apps\.Default\SystemStart\.Current", "")
                    If (String.IsNullOrEmpty(tmpRegData_SystemStart.GetValue().ToString())) Then
                        Helpers.WriteLog("SystemStart default Sound value is" & tmpRegData_SystemStart.GetValue().ToString())
                    End If

                    Dim datalist As String = "%SystemRoot%\media\Windows XP Startup1.wav"
                    tmpRegData_SystemStart.SetValue(datalist)
                    Helpers.WriteLog("SystemStart default Sound value " & datalist & " has been set")

                Catch ex As Exception
                    Helpers.WriteLog(ex)
                End Try

                Try
                    Dim tmpRegData_Logoff As RegData = New RegData(Enums.Hive.CurrentUser, "AppEvents\Schemes\Apps\.Default\WindowsLogoff\.Current", "")
                    If (String.IsNullOrEmpty(tmpRegData_Logoff.GetValue().ToString())) Then
                        Helpers.WriteLog("Logoff default Sound value is" & tmpRegData_Logoff.GetValue().ToString())
                    End If

                    Dim datalist As String = "%SystemRoot%\media\Windows XP Logoff Sound1.wav"
                    tmpRegData_Logoff.SetValue(datalist)
                    Helpers.WriteLog("Logoff default Sound value " & datalist & " has been set")

                Catch ex As Exception
                    Helpers.WriteLog(ex)
                End Try

                Try
                    Dim tmpRegData_Logon As RegData = New RegData(Enums.Hive.CurrentUser, "AppEvents\Schemes\Apps\.Default\WindowsLogon\.Current", "")
                    If (String.IsNullOrEmpty(tmpRegData_Logon.GetValue().ToString())) Then
                        Helpers.WriteLog("Logon default Sound value is" & tmpRegData_Logon.GetValue().ToString())
                    End If

                    Dim datalist As String = "%SystemRoot%\media\Windows XP Logon Sound1.wav"
                    tmpRegData_Logon.SetValue(datalist)
                    Helpers.WriteLog("Logon default sound value " & datalist & " has been set")

                Catch ex As Exception
                    Helpers.WriteLog(ex)
                End Try

                LblMessage.Text = "Installed"
                Helpers.WriteLog("Installed")
                Constants.RegData_RestartRequired.SetValue(1)
                Helpers.WriteLog("Display services is going to reboot machine")
                Helpers.RegisterPatchRan()

            Else
                LblMessage.Text = "Not Installed"

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class
