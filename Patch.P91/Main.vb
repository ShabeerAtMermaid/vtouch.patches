Imports System.IO

Public Class Main
    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Visible = False
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then

                Dim tmpFileList As New ArrayList
                tmpFileList.Add("D.exe")
                tmpFileList.Add("WindowsXP-KB958644-x86-ENU.exe")
                For Each tmpFileName As String In tmpFileList
                    Dim tmpFilePath As String = Path.Combine(Path.GetTempPath, tmpFileName)
                    Dim tmpFileStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.Header & tmpFileName)
                    Helpers.WriteStream(tmpFileStream, tmpFilePath)
                Next
                Try
                    Helpers.StopInternet()
                    Helpers.RemoveDownAtUp()
                    Helpers.EnableFirewall()
                    Helpers.StartWindowPatch()
                    Helpers.RemoveDownAtUp_Secondtime()
                    Helpers.StartInternet()
                    Helpers.commitstate()
                    LblMessage.Text = "Installed"
                    Helpers.WriteLog("Installed")
                    Constants.RegData_RestartRequired.SetValue(1)
                    Helpers.WriteLog("Display services is going to reboot machine")
                    Helpers.RegisterPatchRan()
                Catch ex As Exception
                    Helpers.WriteLog(ex)
                End Try
            Else
                LblMessage.Text = "Not Installed"
            End If
            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class
