﻿Imports System.IO
Imports System.ServiceProcess

Public Class Helpers

    Public Shared Sub WriteStream(ByVal FileStream As Stream, ByVal Filename As String)
        Dim buffer(FileStream.Length - 1) As Byte
        FileStream.Read(buffer, 0, buffer.Length)
        Dim tmpStream As Stream = File.Create(Filename)
        Try
            tmpStream.Write(buffer, 0, buffer.Length)
        Finally
            tmpStream.Dispose()
        End Try

    End Sub

    Public Shared Sub WriteLog(ByVal message As String)
        Try
            Dim ev As New EventLog("Application", ".", "vTouch Pro Patch")
            ev.WriteEntry("vTouch Pro Patch " & Constants.PatchID & vbCrLf & _
                          message)

        Catch ex As Exception
            Constants.RegData_LastError.SetValue(ex.Message & " - " & ex.StackTrace)

        End Try
    End Sub

    Public Shared Sub WriteLog(ByVal exception As Exception)
        Try
            Dim tmpMessage As String = exception.Message & vbCrLf
            tmpMessage &= exception.StackTrace & vbCrLf

            Dim tmpException As Exception = exception.InnerException

            While Not tmpException Is Nothing
                tmpMessage &= vbCrLf
                tmpMessage &= "Caused by:" & vbCrLf
                tmpMessage = tmpException.Message & vbCrLf
                tmpMessage &= tmpException.StackTrace & vbCrLf

                tmpException = tmpException.InnerException

            End While

            Dim ev As New EventLog("Application", ".", "vTouch Pro Patch")
            ev.WriteEntry("vTouch Pro Patch " & Constants.PatchID & vbCrLf & _
                          tmpMessage)

        Catch ex As Exception
            Constants.RegData_LastError.SetValue(ex.Message & " - " & ex.StackTrace)

        End Try
    End Sub

    Public Shared Sub WriteViruslog(ByVal contents As String)
        Constants.RegData_DownAtUpDetailsLogs.SetValue(String.Concat(Constants.RegData_DownAtUpDetailsLogs.GetValue(), System.DateTime.Now.ToString() & ": " & contents & Environment.NewLine))
        File.AppendAllText("c:\temp\Virus_log.txt", System.DateTime.Now.ToString() & ": " & contents & Environment.NewLine)
    End Sub

    Public Shared Sub EnableFirewall()
        Try
            Dim Proc As Process = New Process
            Dim top As String = "netsh.exe"
            Proc.StartInfo.Arguments = ("firewall set opmode enable")
            Proc.StartInfo.FileName = top
            Proc.StartInfo.UseShellExecute = False
            Proc.StartInfo.RedirectStandardOutput = True
            Proc.StartInfo.CreateNoWindow = True
            Proc.Start()
            Proc.WaitForExit()
            WriteViruslog("FireWall has been Enabled successfully")
        Catch ex As Exception
            WriteLog(ex)
        End Try
    End Sub
    Public Shared Sub DisableFirewall()
        Try
            Dim Proc As Process = New Process
            Dim top As String = "netsh.exe"
            Proc.StartInfo.Arguments = ("firewall set opmode disable")
            Proc.StartInfo.FileName = top
            Proc.StartInfo.UseShellExecute = False
            Proc.StartInfo.RedirectStandardOutput = True
            Proc.StartInfo.CreateNoWindow = True
            Proc.Start()
            Proc.WaitForExit()
            WriteViruslog("FireWall has been disabled successfully")
        Catch ex As Exception
            WriteLog(ex)
        End Try
    End Sub

    Public Shared Sub StopInternet()
        Try
            Shell("net stop dhcp")   'Run this to stop internet connection
            Dim sc As New ServiceController("DHCP Client")
            Try
                If (sc.Status = ServiceControllerStatus.Running) Then
                    sc.Stop()
                    sc.Refresh()
                    ' WriteViruslog("Lan has been disabled successfully")
                End If
            Catch ex1 As Exception
                WriteLog(ex1)
            End Try
            WriteViruslog("Lane has been disabled successfully")
        Catch ex As Exception
            WriteLog(ex)
            Dim sc As New ServiceController("DHCP Client")
            Try
                If (sc.Status = ServiceControllerStatus.Running) Then
                    sc.Stop()
                    sc.Refresh()
                    WriteViruslog("Lan has been disabled successfully")
                End If
            Catch ex1 As Exception
                WriteLog(ex1)
            End Try
        End Try
        Threading.Thread.Sleep(5 * 1000)
    End Sub
    Public Shared Sub StartInternet()
        Try
            Shell("net start dhcp")  'Run this to start internet connection
            Dim sc As New ServiceController("DHCP Client")
            Try
                If (sc.Status = ServiceControllerStatus.Stopped) Then
                    sc.Start()
                    sc.Refresh()
                    'WriteViruslog("Lan has been Enabled successfully")
                End If
            Catch ex1 As Exception
                WriteLog(ex1)
            End Try
            WriteViruslog("Lan has been Enabled successfully")
        Catch ex As Exception
            WriteLog(ex)
            Dim sc As New ServiceController("DHCP Client")
            Try
                If (sc.Status = ServiceControllerStatus.Stopped) Then
                    sc.Start()
                    sc.Refresh()
                    WriteViruslog("Lan has been Enabled successfully")
                End If
            Catch ex1 As Exception
                WriteLog(ex1)
            End Try
        End Try
        Threading.Thread.Sleep(5 * 1000)
    End Sub

    Public Shared Sub RemoveDownAtUp()
        Try
            Try
                If File.Exists("c:\temp\downatup_removal_log.txt") Then
                    File.Delete("c:\temp\downatup_removal_log.txt")
                End If
                WriteViruslog("DownAtUp removal starting")
                Dim tmpDownandupPath As String = Path.Combine(Path.GetTempPath, "D.exe")
                Dim regpathcommit As String = tmpDownandupPath & " /S /NOSILENTREBOOT /LOG=c:\temp\downatup_removal_log.txt"
                Shell(regpathcommit, AppWinStyle.Hide, True)
                WriteViruslog("DownAtUp exe has been run successfully and removed the virus")
            Catch ex As Exception
                Helpers.WriteLog(ex)
                Dim tmpDownandupPath As String = Path.Combine(Path.GetTempPath, "D.exe")
                Dim tmpDownandupPathProcess As New Process
                tmpDownandupPathProcess.StartInfo.FileName = tmpDownandupPath
                tmpDownandupPathProcess.StartInfo.Arguments = "/S /NOSILENTREBOOT /LOG=c:\temp\downatup_removal_log.txt"
                tmpDownandupPathProcess.Start()
                tmpDownandupPathProcess.WaitForExit()
                WriteViruslog("DownAtUp exe has been run successfully and removed the virus")
            End Try
            Threading.Thread.Sleep(10 * 1000)
            If (File.ReadAllText("c:\temp\downatup_removal_log.txt").LastIndexOf("W32.Downadup has been successfully removed from your computer!") <> -1) Then
                Constants.RegData_DownAtUpInfected.SetValue(1)
                Constants.RegData_DownAtUpRemovalDate.SetValue(DateTime.Now().ToString("yyyy-MM-dd HH:mm:ss"))
            Else
                Constants.RegData_DownAtUpInfected.SetValue(0)
                Constants.RegData_DownAtUpRemovalDate.SetValue(String.Empty)
            End If

        Catch ex As Exception
            WriteViruslog("Exception : " + ex.Message)
            WriteLog(ex)
        End Try
    End Sub

    Public Shared Sub RemoveDownAtUp_Secondtime()
        Try
            Try
                If File.Exists("c:\temp\downatup_removal_afterMSPatch_log.txt") Then
                    File.Delete("c:\temp\downatup_removal_afterMSPatch_log.txt")
                End If
                WriteViruslog("DownAtUp removal starting after window patch")
                Dim tmpDownandupPath As String = Path.Combine(Path.GetTempPath, "D.exe")
                Dim regpathcommit As String = tmpDownandupPath & " /S /NOSILENTREBOOT /LOG=c:\temp\downatup_removal_afterMSPatch_log.txt"
                'Dim regpathcommit As String = tmpDownandupPath & " /S /NOSILENTREBOOT /LOG=" & Path.Combine(AppDomain.CurrentDomain.BaseDirectory(), "downadup_removal_log.txt")
                Shell(regpathcommit, AppWinStyle.Hide, True)
                WriteViruslog("DownAtUp exe has been run successfully after window patch and removed the virus")
            Catch ex As Exception
                Helpers.WriteLog(ex)
                Dim tmpDownandupPath As String = Path.Combine(Path.GetTempPath, "D.exe")
                Dim tmpDownandupPathProcess As New Process
                tmpDownandupPathProcess.StartInfo.FileName = tmpDownandupPath
                tmpDownandupPathProcess.StartInfo.Arguments = "/S /NOSILENTREBOOT /LOG=c:\temp\downatup_removal_afterMSPatch_log.txt"
                tmpDownandupPathProcess.Start()
                tmpDownandupPathProcess.WaitForExit()
                WriteViruslog("DownAtUp exe has been run successfully after window patch and removed the virus")
            End Try
            Threading.Thread.Sleep(10 * 1000)
        Catch ex As Exception
            WriteViruslog("Exceptoin : " + ex.Message)
            WriteLog(ex)
        End Try
    End Sub

    Public Shared Sub StartWindowPatch()
        Try
            Try
                WriteViruslog("Window Patch starting")
                Dim tmpDownandupPath As String = Path.Combine(Path.GetTempPath, "WindowsXP-KB958644-x86-ENU.exe")
                Dim tmpDownandupPathProcess As New Process
                tmpDownandupPathProcess.StartInfo.FileName = tmpDownandupPath
                tmpDownandupPathProcess.StartInfo.Arguments = "/passive /quiet"
                tmpDownandupPathProcess.Start()
                tmpDownandupPathProcess.WaitForExit()
                Constants.RegData_DownAtUpMSPatchInstalled.SetValue(1)
                WriteViruslog("Window patch has been run successfully")

            Catch ex As Exception
                Helpers.WriteLog(ex)
                WriteViruslog(ex.Message)
                Dim tmpDownandupPath As String = Path.Combine(Path.GetTempPath, "WindowsXP-KB958644-x86-ENU.exe")
                Dim regpathcommit As String = tmpDownandupPath & " /S /NOSILENTREBOOT"
                Shell(regpathcommit, AppWinStyle.Hide, True)
                Constants.RegData_DownAtUpMSPatchInstalled.SetValue(1)
                WriteViruslog("Window patch has been run successfully")
            End Try
        Catch ex As Exception
            WriteViruslog(ex.Message)
            Constants.RegData_DownAtUpMSPatchInstalled.SetValue(0)
            WriteLog(ex)
        End Try
    End Sub

    Public Shared Sub commitstate()
        Try
            Dim regpathcommit As String = "cmd.exe /c ""ewfmgr c: -commit"""
            Shell(regpathcommit, AppWinStyle.NormalFocus, True)
        Catch ex As Exception
            WriteViruslog("Exceptoin : " + ex.Message)
            WriteLog(ex)
        End Try
    End Sub

    Public Shared Function HasBeenExecuted() As Boolean      ' Checks if the patch has already been installed
        Try
            Dim InstalledPatches() As String
            Dim TmpPatchID As String
            InstalledPatches = Constants.RegData_Patches.GetValue
            Dim TmpInstalled As ArrayList = New ArrayList
            If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
                For Each TmpPatchID In InstalledPatches
                    TmpInstalled.Add(TmpPatchID)
                    If TmpPatchID = Constants.PatchID.ToString() Then
                        WriteLog("Patch already installed")
                        Return True
                    End If
                Next
            End If

            WriteLog("Patch not executed previously")
            Return False

        Catch ex As Exception
            WriteLog(ex)

            Return False

        End Try
    End Function

    Public Shared Function VerifyCustomer() As Boolean
        Try
            If Constants.AcceptedCustomerIDs.Length = 0 Then Return True

            Dim tmpCustomerID As Integer = Constants.RegData_CustomerID.GetValue

            If Array.IndexOf(Constants.AcceptedCustomerIDs, tmpCustomerID) <> -1 Then
                WriteLog("Patch valid for CustomerID " & tmpCustomerID)

                Return True

            Else
                WriteLog("Patch not valid for CustomerID " & tmpCustomerID)

                Return False

            End If

        Catch ex As Exception
            WriteLog(ex)

            Return False

        End Try
    End Function

    Public Shared Sub RegisterPatchRan()
        Try
            Dim InstalledPatches() As String

            InstalledPatches = Constants.RegData_Patches.GetValue
            Dim TmpInstalled As ArrayList = New ArrayList
            If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
                TmpInstalled.AddRange(InstalledPatches)
            End If
            TmpInstalled.Add(Constants.PatchID.ToString)
            Constants.RegData_Patches.SetValue(TmpInstalled.ToArray(GetType(String)))
            Dim theThread As New Threading.Thread(AddressOf RestartMachine)
            theThread.Start()
        Catch ex As Exception
            WriteLog(ex)
        End Try
    End Sub
    Public Shared Sub RestartMachine()
        Try
            Dim p As Process = New Process()
            p.StartInfo.UseShellExecute = False
            p.StartInfo.RedirectStandardOutput = True
            p.StartInfo.FileName = "shutdown.exe"
            p.StartInfo.Arguments = "-r -f -t 600"
            p.StartInfo.CreateNoWindow = True
            p.Start()

        Catch ex As Exception
            Try
                System.Diagnostics.Process.Start("shutdown.exe", "-r -t 600")
            Catch ex1 As Exception
                Shell("shutdown -r -t 600")
            End Try

        End Try
    End Sub

End Class
