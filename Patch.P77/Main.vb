Imports System.IO
Imports Microsoft.Win32
Imports System.ServiceProcess
Imports System.Management


Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub
    Public Function HextoByte(ByVal hexstringtoByte As String) As Byte()
        Dim hex As String = hexstringtoByte '"8ad0fa189fd976bf801f410d3e1bd38fe23840068981ba3fa32c39797ad05a59"
        Dim raw As Byte() = New Byte((hex.Length / 2) - 1) {}
        Dim i As Integer
        For i = 0 To raw.Length - 1
            raw(i) = Convert.ToByte(hex.Substring((i * 2), 2), &H10)
        Next i
        Return raw
    End Function
    'Sub Main()
    '    Dim StringVariable As String = "vTouch-12"
    '    Dim ByteArray() As Byte
    '    ByteArray = System.Text.Encoding.ASCII.GetBytes(StringVariable)
    '    'For Each b As Byte In ByteArray
    '    '    Console.WriteLine(b)
    '    'Next

    '    Dim KeyPath As String = "HKEY_LOCAL_MACHINE\Software\TeamViewer\Version6\"
    '    'Dim BinaryData As Byte() = {8a,d0,fa,18,9f,d9,76,bf,80,1f,41,0d,3e,1b,d3,8f,e2,38,40,06,89,81,ba,3f,a3,2c,39,79,7a,d0,5a,59}

    '    Dim raw As Byte() = HextoByte("8ad0fa189fd976bf801f410d3e1bd38fe23840068981ba3fa32c39797ad05a59")
    '    'Dim byteArry() As Byte =
    '    My.Computer.Registry.SetValue(KeyPath, "SecurityPasswordAES1", raw, Microsoft.Win32.RegistryValueKind.Binary)
    '    Console.Read()
    'End Sub
    Public Sub StopService(ByVal PC As String)
        Dim obj As ManagementObject
        Dim inParams, outParams As ManagementBaseObject
        Dim Result As Integer

        ' we can only disable the service and wait
        ' until the next reboot to stop it

        obj = New ManagementObject("\\" & PC & "\root\cimv2:Win32_Service.Name='TeamViewer7'")

        ' change the Start Mode to Disabled
        If obj("StartMode").ToString <> "Disabled" Then
            ' Get an input parameters object for this method
            inParams = obj.GetMethodParameters("ChangeStartMode")
            inParams("StartMode") = "Disabled"

            ' do it!
            outParams = obj.InvokeMethod("ChangeStartMode", inParams, Nothing)
            Result = Convert.ToInt32(outParams("returnValue"))
            If Result <> 0 Then
                Throw New Exception("ChangeStartMode method error code " & Result)
            End If
        End If
    End Sub
    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then

                Dim bHeadlinePlayer As Boolean = Helpers.HeadlinePlayer()
                Dim bTeamViewInstalled As Boolean = Helpers.TeamViewInstalled()

                If Not bHeadlinePlayer And Not bTeamViewInstalled Then
                    Dim regKey As RegistryKey
                    Dim Teamviewpath As String = "SOFTWARE\TeamViewer\Version7"
                    regKey = Registry.LocalMachine.OpenSubKey(Teamviewpath)
                    If (Not regKey Is Nothing) Then
                        Teamviewpath = "HKEY_LOCAL_MACHINE\SOFTWARE\TeamViewer\Version7"
                        Dim raw As Byte() = HextoByte("8ad0fa189fd976bf801f410d3e1bd38fe23840068981ba3fa32c39797ad05a59")
                        My.Computer.Registry.SetValue(Teamviewpath, "SecurityPasswordAES", raw, Microsoft.Win32.RegistryValueKind.Binary)
                        Dim sc As New ServiceController("TeamViewer 7")
                        Try
                            If (sc.Status = ServiceControllerStatus.Running) Then
                                sc.Stop()
                                System.Threading.Thread.Sleep(5000)
                                sc.Refresh()
                                StopService(My.Computer.Name)
                            End If
                        Catch ex As Exception
                        End Try
                    End If

                    Dim tmpFileList As New ArrayList
                    tmpFileList.Add("TeamViewer_settings.reg")
                    tmpFileList.Add("TeamViewer_Host.msi")
                    tmpFileList.Add("install.bat")
                    For Each tmpFileName As String In tmpFileList
                        Dim tmpFilePath As String = Path.Combine(Path.GetTempPath, tmpFileName)
                        Dim tmpFileStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.Header & tmpFileName)
                        Helpers.WriteStream(tmpFileStream, tmpFilePath)
                    Next

                    Dim regpath As String = "regedit.exe /s """ & Path.Combine(Path.GetTempPath, "TeamViewer_settings.reg") & """"
                    Shell(regpath, AppWinStyle.NormalFocus)
                    Dim tmpTeamViewerInstallerPath As String = Path.Combine(Path.GetTempPath, "teamviewer_Host.msi")
                    Dim tmpTeamViewerProcess As New Process
                    tmpTeamViewerProcess.StartInfo.FileName = tmpTeamViewerInstallerPath
                    tmpTeamViewerProcess.StartInfo.Arguments = "/passive /quiet"
                    tmpTeamViewerProcess.Start()
                    tmpTeamViewerProcess.WaitForExit()
                    'End If

                    Try
                        Dim regpathcommit As String = "cmd.exe /c ""ewfmgr c: -commit"""
                        Shell(regpathcommit, AppWinStyle.NormalFocus, True)
                    Catch ex As Exception
                        Helpers.WriteLog(ex)
                    End Try

                    LblMessage.Text = "Installed"
                    Helpers.WriteLog("Installed")
                    Constants.RegData_RestartRequired.SetValue(1)
                Else
                    If bHeadlinePlayer Then
                        Helpers.WriteLog("This is Headline.tv player therefor Team-View software will not install here.")
                    End If
                    If bTeamViewInstalled Then
                        Helpers.WriteLog("Team-Viewer software is already installed here !")
                    End If

                End If
                Helpers.RegisterPatchRan()

            Else
                LblMessage.Text = "Patch Not Installed - skipped"

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class
