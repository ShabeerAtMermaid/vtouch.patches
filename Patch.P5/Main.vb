Imports Microsoft.Win32
Imports System.IO

Public Class Main

    Private RegData_Patches As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro", "InstalledPatches")
    Private RegData_Patches_NEW As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\Victor Soft Updater", "InstalledPatches")
    Private RegData_CustomerID As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro\Common Settings", "CustomerId")

    Private Const PatchID As Integer = 5
    Private Const HEADER As String = "VictorSoft.vTouch.Patch.P5."

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not VerifyAlreadyRun() Then
                Dim tmpFileStream As Stream
                Dim tmpFilePath As String

                Dim FileList As New List(Of DllData)
                'LEAD MPEG2 - Disabled
                'FileList.Add(New DllData("LDECMPG22.dll", True))
                'FileList.Add(New DllData("LDECMPG2KRN2.dll", True))
                'FileList.Add(New DllData("LMAMpgCnv.dll", True))

                'LEAD Supporting DLL's
                FileList.Add(New DllData("Lfbmp15u.dll", False))
                FileList.Add(New DllData("Lfcmp15u.dll", False))
                FileList.Add(New DllData("Lffax15u.dll", False))
                FileList.Add(New DllData("Lfgif15u.dll", False))
                FileList.Add(New DllData("Lfpcd15u.dll", False))
                FileList.Add(New DllData("Lfpct15u.dll", False))
                FileList.Add(New DllData("Lfpng15u.dll", False))
                FileList.Add(New DllData("Lftif15u.dll", False))
                FileList.Add(New DllData("Ltdis15u.dll", False))
                FileList.Add(New DllData("Ltfil15u.dll", False))
                FileList.Add(New DllData("Ltkrn15u.dll", False))

                ' LEAD Filters
                FileList.Add(New DllData("DSKernel2.dll", True))
                FileList.Add(New DllData("LMVRGBxf.dll", True))
                FileList.Add(New DllData("LMVRsz2.dll", True))
                FileList.Add(New DllData("LMVTOvLy2.dll", True))
                FileList.Add(New DllData("LTStlImgRd2.dll", True))

                'Deleting existing files
                For Each tmpDLLData As DllData In FileList
                    tmpFilePath = System.Environment.GetFolderPath(Environment.SpecialFolder.System) & "\" & tmpDLLData.FileName

                    Try
                        If File.Exists(tmpFilePath) Then File.Delete(tmpFilePath)

                    Catch ex As Exception

                    End Try

                Next

                'Copying files
                For Each tmpDLLData As DllData In FileList
                    tmpFilePath = System.Environment.GetFolderPath(Environment.SpecialFolder.System) & "\" & tmpDLLData.FileName
                    tmpFileStream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(HEADER & tmpDLLData.FileName)
                    Try
                        WriteStream(tmpFileStream, tmpFilePath)

                    Catch ex As Exception

                    End Try

                Next

                'Registering files
                Dim tmpProcess As New Process()

                For Each tmpDLLData As DllData In FileList
                    If tmpDLLData.Register Then
                        tmpProcess.StartInfo.FileName = System.Environment.GetFolderPath(Environment.SpecialFolder.System) & "\regsvr32.exe"
                        tmpProcess.StartInfo.Arguments = "/s " & tmpDLLData.FileName
                        tmpProcess.StartInfo.CreateNoWindow = True
                        tmpProcess.Start()

                    End If

                Next

            End If

            RegisterPatchRan()

        Catch ex As Exception
            LblStatus.Text = "Exception: " & ex.Message

        End Try

        Me.Close()

    End Sub

    Private Sub WriteStream(ByVal FileStream As Stream, ByVal Filename As String)
        Dim buffer(FileStream.Length - 1) As Byte
        FileStream.Read(buffer, 0, buffer.Length)

        Dim tmpStream As Stream = File.Create(Filename)

        Try
            tmpStream.Write(buffer, 0, buffer.Length)

        Finally
            tmpStream.Dispose()

        End Try

    End Sub

    Private Function VerifyAlreadyRun() As Boolean      ' Checks if the patch has already been installed
        Dim InstalledPatches() As String = GetRegValue(RegData_Patches_NEW.Hive, RegData_Patches_NEW.Path, RegData_Patches_NEW.Value)

        If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
            For Each tmpPatchID As String In InstalledPatches
                If tmpPatchID = PatchID Then
                    Return True

                End If
            Next
        End If

        InstalledPatches = GetRegValue(RegData_Patches.Hive, RegData_Patches.Path, RegData_Patches.Value)

        If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
            For Each tmpPatchID As String In InstalledPatches
                If tmpPatchID = PatchID Then
                    Return True

                End If
            Next
        End If

        Return False

    End Function

    Private Sub RegisterPatchRan()
        Dim InstalledPatches() As String = GetRegValue(RegData_Patches_NEW.Hive, RegData_Patches_NEW.Path, RegData_Patches_NEW.Value)
        Dim tmpInstalled As ArrayList = New ArrayList

        If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
            tmpInstalled.AddRange(InstalledPatches)

        End If

        tmpInstalled.Add(PatchID.ToString)

        SetRegValue(RegData_Patches_NEW.Hive, RegData_Patches_NEW.Path, RegData_Patches_NEW.Value, tmpInstalled.ToArray(GetType(String)))

    End Sub

#Region "Registry"
    Public Shared Function GetRegValue(ByVal whereToRead As Enums.Hive, ByVal path As String, ByVal value As String) As Object
        Dim regKey As RegistryKey
        Dim regKeyValue As Object

        'Determine the hive and open/create the designated path
        Select Case whereToRead
            Case Enums.Hive.classesroot
                regKey = Registry.ClassesRoot.CreateSubKey(path)
            Case Enums.Hive.currentconfig
                regKey = Registry.CurrentConfig.CreateSubKey(path)
            Case Enums.Hive.currentuser
                regKey = Registry.CurrentUser.CreateSubKey(path)
            Case Enums.Hive.dyndata
                regKey = Registry.DynData.CreateSubKey(path)
            Case Enums.Hive.localmachine
                regKey = Registry.LocalMachine.CreateSubKey(path)
            Case Enums.Hive.performancedata
                regKey = Registry.PerformanceData.CreateSubKey(path)
            Case Enums.Hive.users
                regKey = Registry.Users.CreateSubKey(path)
            Case Else
                regKey = Registry.LocalMachine.CreateSubKey(path)
        End Select

        regKeyValue = regKey.GetValue(value, Nothing)
        regKey.Close()
        Return regKeyValue
    End Function

    Public Shared Sub SetRegValue(ByVal whereToWrite As Enums.Hive, ByVal path As String, ByVal value As String, ByVal data As Object)
        Dim regKey As RegistryKey

        'Determine the hive and open/create the designated path
        Select Case whereToWrite
            Case Enums.Hive.classesroot
                regKey = Registry.ClassesRoot.CreateSubKey(path)
            Case Enums.Hive.currentconfig
                regKey = Registry.CurrentConfig.CreateSubKey(path)
            Case Enums.Hive.currentuser
                regKey = Registry.CurrentUser.CreateSubKey(path)
            Case Enums.Hive.dyndata
                regKey = Registry.DynData.CreateSubKey(path)
            Case Enums.Hive.localmachine
                regKey = Registry.LocalMachine.CreateSubKey(path)
            Case Enums.Hive.performancedata
                regKey = Registry.PerformanceData.CreateSubKey(path)
            Case Enums.Hive.users
                regKey = Registry.Users.CreateSubKey(path)
            Case Else
                regKey = Registry.LocalMachine.CreateSubKey(path)
        End Select

        'Determine which datatype to write to regedit
        If TypeOf data Is Integer Then
            regKey.SetValue(value, data, RegistryValueKind.DWord)

        ElseIf TypeOf data Is String() Then
            regKey.SetValue(value, data, RegistryValueKind.MultiString)

        ElseIf TypeOf data Is Byte() Then
            regKey.SetValue(value, data, RegistryValueKind.Binary)

        Else
            regKey.SetValue(value, data)

        End If

        regKey.Close()

    End Sub

#End Region

End Class

Friend Class RegData

#Region "Attributes"

    Private _Hive As Enums.Hive
    Private _Path As String
    Private _Value As String

#End Region

#Region "Properties"

    Public ReadOnly Property Hive() As Enums.Hive
        Get
            Return Me._Hive

        End Get
    End Property

    Public ReadOnly Property Path() As String
        Get
            Return Me._Path

        End Get
    End Property

    Public ReadOnly Property Value() As String
        Get
            Return Me._Value

        End Get
    End Property

#End Region

    Public Sub New(ByVal hive As Enums.Hive, ByVal path As String, ByVal value As String)
        Me._Hive = hive
        Me._Path = path
        Me._Value = value

    End Sub

End Class
