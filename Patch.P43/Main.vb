Imports System.IO

Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then
                Dim tmpProgramDirectory As String = Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "vTouch Pro\vTouch Pro 3G Modem")

                If Not Directory.Exists(tmpProgramDirectory) Then Directory.CreateDirectory(tmpProgramDirectory)

                For Each tmpProcess As Process In Process.GetProcessesByName("ModemMonitor")
                    tmpProcess.Kill()
                Next

                Threading.Thread.Sleep(5000)

                Shell("taskkill -f -im ModemMonitor.exe")

                Threading.Thread.Sleep(10000)

                Dim tmpFileList As New ArrayList
                tmpFileList.Add("Devices.xml")
                tmpFileList.Add("ModemMonitor.exe")
                tmpFileList.Add("ModemMonitor.exe.config")
                tmpFileList.Add("ModemMonitor.pdb")
                tmpFileList.Add("ModemMonitorDll.dll")
                tmpFileList.Add("ModemMonitorDll.pdb")
                tmpFileList.Add("ModemMonitorSwiSdkWrapper.dll")
                tmpFileList.Add("ModemMonitorSwiSdkWrapper.pdb")
                tmpFileList.Add("msvcr100.dll")
                tmpFileList.Add("SwiApiInterface.dll")
                tmpFileList.Add("SwiApiInterface.lib")
                tmpFileList.Add("SwiApiMux.exe")
                tmpFileList.Add("SwiCardDetect.dll")
                tmpFileList.Add("WinIo32.dll")
                tmpFileList.Add("WinIo32.sys")
                tmpFileList.Add("WinIo64.dll")
                tmpFileList.Add("WinIo64.sys")

                For Each tmpFileName As String In tmpFileList
                    Dim tmpFilePath As String = Path.Combine(tmpProgramDirectory, tmpFileName)
                    If File.Exists(tmpFilePath & ".vup") Then
                        File.Delete(tmpFilePath & ".vup")
                    End If
                    If File.Exists(tmpFilePath) Then
                        File.Move(tmpFilePath, tmpFilePath & ".vup")
                    End If
                Next

                For Each tmpFileName As String In tmpFileList
                    Dim tmpFilePath As String = Path.Combine(tmpProgramDirectory, tmpFileName)

                    Dim tmpFileStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.Header & tmpFileName)
                    Helpers.WriteStream(tmpFileStream, tmpFilePath)
                Next

                Dim tmpArgData As New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\vTouch Pro DisplayServices\Processes\vTouch Pro 3G Modem", "Arguments")
                tmpArgData.SetValue("/min_dialup_interval 30 84.246.245.135 8.8.8.8 /logfile \""C:\\Program Files\\vTouch Pro\\vTouch Pro 3G Modem\\mm.log\""  /number_of_quiet_pass_pings 89")

                Helpers.RegisterPatchRan()

                LblMessage.Text = "Installed"
                Helpers.WriteLog("Installed")

            Else
                LblMessage.Text = "Not installed"
                Helpers.WriteLog("Not installed")

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class