Imports System.IO
Imports System.ComponentModel

Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then

                If Environment.MachineName.StartsWith("TDC", StringComparison.OrdinalIgnoreCase) Then
                    For Each p As Process In System.Diagnostics.Process.GetProcesses()
                        Try
                            If p.ProcessName.StartsWith("Headline.") Then
                                p.Kill()
                                ' possibly with a timeout
                                p.WaitForExit(5000)
                                ' process was terminating or can't be terminated - deal with it
                            End If
                        Catch winException As Win32Exception
                            Throw winException
                            ' process has already exited - might be able to let this one go
                        Catch invalidException As InvalidOperationException
                            Throw invalidException
                        Catch exc As Exception
                            Throw exc
                        End Try

                    Next

                    Dim tmpFolderForMSI As String = "D:\Temp"
                    Dim tmpFilePathForMSI As String = "D:\Temp\Install.Headline.Player.msi"

                    If Directory.Exists(tmpFolderForMSI) Then
                        If File.Exists(tmpFilePathForMSI) Then
                            Dim tmpSetupProcess As Process = New System.Diagnostics.Process()
                            tmpSetupProcess.StartInfo.FileName = tmpFilePathForMSI
                            tmpSetupProcess.StartInfo.WindowStyle = ProcessWindowStyle.Normal
                            tmpSetupProcess.Start()
                            tmpSetupProcess.WaitForExit()
                        Else
                            Throw New Exception("The setup file '" & tmpFilePathForMSI & "' does not exist")
                        End If
                    Else
                        Throw New Exception("The folder '" & tmpFolderForMSI & "' does not exist")
                    End If

                    LblMessage.Text = "Installed"
                    Helpers.WriteLog("Installed")
                    Constants.RegData_RestartRequired.SetValue(1)
                    Helpers.RegisterPatchRan()
                Else
                    LblMessage.Text = "It is not a touch player"
                    Helpers.WriteLog("It is not a touch player so patch is not intalled")
                End If
            Else
                LblMessage.Text = "Not Installed"

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class