Imports System.IO

Public Class Main
    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then

                Dim tmpRegData_PagingFiles As RegData = New RegData(Enums.Hive.LocalMachine, "SYSTEM\CurrentControlSet\Services\W32Time\Parameters", "Type")
                If (tmpRegData_PagingFiles.GetValue().ToString() = "NTP") Then
                    Helpers.WriteLog("Automatically Synchronize with an Internet time Server option was already Enabled")
                End If

                Dim datalist As String = "NoSync"
                tmpRegData_PagingFiles.SetValue("NoSync")
                Helpers.WriteLog("Automatically Synchronize with an Internet time Server option option has been disabled")

                LblMessage.Text = "Installed"
                Helpers.WriteLog("Installed")
                Constants.RegData_RestartRequired.SetValue(1)
                Helpers.WriteLog("Display services is going to reboot machine")
                Helpers.RegisterPatchRan()

            Else
                LblMessage.Text = "Not Installed"

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class
