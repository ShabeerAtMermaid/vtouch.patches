Imports System.IO
Imports Microsoft.Win32
Imports System.ServiceProcess
Imports System.Management


Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() Then
                If Helpers.VerifyCustomer() Then
                    If Helpers.VerifyModelName Then
                        If Not Helpers.VerifyBIOSReleaseDate Then
                            If Helpers.UpdateBIOS Then
                                LblMessage.Text = "Installed"
                                Helpers.RebootMachine()
                            End If
                        Else
                            Helpers.WriteLog("Already Installed. BIOS Release date is already 11/06/2013")
                            Constants.RegData_RestartRequired.SetValue(1)
                            Helpers.RegisterPatchRan()
                        End If
                    Else
                        Helpers.WriteLog("Model name is not NDIS 127. Marking as Installed")
                        Constants.RegData_RestartRequired.SetValue(1)
                        Helpers.RegisterPatchRan()
                    End If
                End If
            Else
                LblMessage.Text = "Patch Not Installed - It is Already Installed."
            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class
