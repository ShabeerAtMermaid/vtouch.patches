﻿Imports System.IO
Imports Microsoft.Win32
Public Class Helpers

    Public Shared Sub WriteStream(ByVal FileStream As Stream, ByVal Filename As String)
        Dim buffer(FileStream.Length - 1) As Byte
        FileStream.Read(buffer, 0, buffer.Length)
        Dim tmpStream As Stream = File.Create(Filename)
        Try
            tmpStream.Write(buffer, 0, buffer.Length)
        Finally
            tmpStream.Dispose()
        End Try

    End Sub

    Public Shared Sub WriteLog(ByVal message As String)
        Try
            Dim ev As New EventLog("Application", ".", "vTouch Pro Patch")
            ev.WriteEntry("vTouch Pro Patch " & Constants.PatchID & vbCrLf & _
                          message)

        Catch ex As Exception
            Constants.RegData_LastError.SetValue(ex.Message & " - " & ex.StackTrace)

        End Try
    End Sub

    Public Shared Sub WriteLog(ByVal exception As Exception)
        Try
            Dim tmpMessage As String = exception.Message & vbCrLf
            tmpMessage &= exception.StackTrace & vbCrLf

            Dim tmpException As Exception = exception.InnerException

            While Not tmpException Is Nothing
                tmpMessage &= vbCrLf
                tmpMessage &= "Caused by:" & vbCrLf
                tmpMessage = tmpException.Message & vbCrLf
                tmpMessage &= tmpException.StackTrace & vbCrLf

                tmpException = tmpException.InnerException

            End While

            Dim ev As New EventLog("Application", ".", "vTouch Pro Patch")
            ev.WriteEntry("vTouch Pro Patch " & Constants.PatchID & vbCrLf & _
                          tmpMessage)

        Catch ex As Exception
            Constants.RegData_LastError.SetValue(ex.Message & " - " & ex.StackTrace)

        End Try
    End Sub
    Public Shared Function UpdateBIOS() As Boolean
        Try
            Dim tmpFileList As New ArrayList
            tmpFileList.Add("afuwin.exe")
            tmpFileList.Add("D127bios.rom")
            tmpFileList.Add("amifldrv.vxd")
            tmpFileList.Add("amifldrv32.sys")
            For Each tmpFileName As String In tmpFileList
                Dim tmpFilePath As String = Path.Combine(Path.GetTempPath, tmpFileName)
                Dim tmpFileStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.Header & tmpFileName)
                Helpers.WriteStream(tmpFileStream, tmpFilePath)
            Next
            Dim tmpBIOSUpateInstallerPath As String = Path.Combine(Path.GetTempPath, "afuwin.exe")
            Dim tmpBIOSROMPath As String = Path.Combine(Path.GetTempPath, "D127bios.rom")
            Dim tmpArguments As String = String.Format("""{0}"" /p /b /n /k /x", tmpBIOSROMPath)
            Dim tmpProcess As New Process
            tmpProcess.StartInfo.FileName = tmpBIOSUpateInstallerPath
            tmpProcess.StartInfo.Arguments = tmpArguments
            tmpProcess.Start()
            tmpProcess.WaitForExit()
            Helpers.WriteLog("BIOS has been Updated")
            Try
                Dim pathcommit As String = "cmd.exe /c ""ewfmgr c: -commit"""
                Shell(pathcommit, AppWinStyle.NormalFocus, True)
            Catch ex As Exception
                Helpers.WriteLog(ex)
            End Try
            Return True
        Catch ex1 As Exception
            Helpers.WriteLog(ex1)
        End Try
        Return False
    End Function
    Public Shared Function VerifyModelName() As Boolean
        Try
            If String.Compare(Constants.RegData_ModelName.GetValue.ToString, Constants.NDIS_127) = 0 Then
                Return True
            End If
        Catch ex As Exception

        End Try
        Return False
    End Function

    Public Shared Function VerifyBIOSReleaseDate() As Boolean
        Try
            If String.Compare(Constants.RegData_BIOSReleaseDate.GetValue.ToString, Constants.BIOS_RELEASED_DATE) = 0 Then
                Return True
            End If

        Catch ex As Exception

        End Try
        Return False
    End Function
    Public Shared Sub RebootMachine()
        Try
            Dim p As Process = New Process()
            p.StartInfo.UseShellExecute = False
            p.StartInfo.RedirectStandardOutput = True
            p.StartInfo.FileName = "shutdown.exe"
            p.StartInfo.Arguments = "-r -f -t 5"
            p.StartInfo.CreateNoWindow = True
            p.Start()

        Catch ex As Exception
            Try
                Helpers.WriteLog(ex)
                System.Diagnostics.Process.Start("shutdown.exe", "-r -t 5")
            Catch ex1 As Exception
                Helpers.WriteLog(ex1)
                Shell("shutdown -r -t 900")
            End Try

        End Try
    End Sub

    Public Shared Function HasBeenExecuted() As Boolean      ' Checks if the patch has already been installed
        Try
            Dim InstalledPatches() As String
            Dim TmpPatchID As String
            InstalledPatches = Constants.RegData_Patches.GetValue
            Dim TmpInstalled As ArrayList = New ArrayList
            If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
                For Each TmpPatchID In InstalledPatches
                    TmpInstalled.Add(TmpPatchID)
                    If TmpPatchID = Constants.PatchID Then
                        WriteLog("Patch already installed")
                        Return True
                    End If
                Next
            End If

            WriteLog("Patch not executed previously")
            Return False

        Catch ex As Exception
            WriteLog(ex)

            Return False

        End Try
    End Function

    Public Shared Function VerifyCustomer() As Boolean
        Try
            If Constants.AcceptedCustomerIDs.Length = 0 Then Return True

            Dim tmpCustomerID As Integer = Constants.RegData_CustomerID.GetValue

            If Array.IndexOf(Constants.AcceptedCustomerIDs, tmpCustomerID) <> -1 Then
                WriteLog("Patch valid for CustomerID " & tmpCustomerID)

                Return True

            Else
                WriteLog("Patch not valid for CustomerID " & tmpCustomerID)

                Return False

            End If

        Catch ex As Exception
            WriteLog(ex)

            Return False

        End Try
    End Function


    Public Shared Sub RegisterPatchRan()
        Try
            Dim InstalledPatches() As String

            InstalledPatches = Constants.RegData_Patches.GetValue
            Dim TmpInstalled As ArrayList = New ArrayList
            If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
                TmpInstalled.AddRange(InstalledPatches)
            End If
            TmpInstalled.Add(Constants.PatchID.ToString)
            Constants.RegData_Patches.SetValue(TmpInstalled.ToArray(GetType(String)))

        Catch ex As Exception
            WriteLog(ex)

        End Try
    End Sub

End Class
