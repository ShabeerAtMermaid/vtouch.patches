Imports System.IO

Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() Then
                If Helpers.VerifyCustomer() Then
                    Dim tmpRegData_DotNetInstallation As New RegData(Enums.Hive.LocalMachine, "SOFTWARE\Microsoft\NET Framework Setup\NDP\v3.5", "Version")

                    If Not tmpRegData_DotNetInstallation.KeyExists Then
                        Dim tmpInstallerPathPrefix As String = String.Empty
                        Dim tmpInstallerPath As String = String.Empty
                        Dim tmpAttemptedInstallerPaths As New ArrayList

                        If Constants.RegData_LocalResourcePath.KeyExists And Constants.RegData_CustomerID.KeyExists Then
                            tmpInstallerPathPrefix = Path.Combine(Constants.RegData_LocalResourcePath.GetValue, Constants.RegData_CustomerID.GetValue)

                        Else
                            tmpInstallerPathPrefix = Path.Combine(Constants.RegData_LocalResourcePath_Old.GetValue, Constants.RegData_CustomerID_Old.GetValue)

                        End If

                        tmpInstallerPath = Path.Combine(tmpInstallerPathPrefix, "Media\1030\35568.exe")
                        tmpAttemptedInstallerPaths.Add(tmpInstallerPath)

                        If Not File.Exists(tmpInstallerPath) Then
                            tmpInstallerPath = Path.Combine(tmpInstallerPathPrefix, "Media\35568.exe")
                            tmpAttemptedInstallerPaths.Add(tmpInstallerPath)
                        End If

                        If Not File.Exists(tmpInstallerPath) Then
                            tmpInstallerPath = Path.Combine(tmpInstallerPathPrefix, "Media\1030\418346.exe")
                            tmpAttemptedInstallerPaths.Add(tmpInstallerPath)
                        End If

                        If Not File.Exists(tmpInstallerPath) Then
                            tmpInstallerPath = Path.Combine(tmpInstallerPathPrefix, "Media\418346.exe")
                            tmpAttemptedInstallerPaths.Add(tmpInstallerPath)
                        End If

                        If Not File.Exists(tmpInstallerPath) Then
                            tmpInstallerPath = Path.Combine(tmpInstallerPathPrefix, "Media\1030\550616.exe")
                            tmpAttemptedInstallerPaths.Add(tmpInstallerPath)
                        End If

                        If Not File.Exists(tmpInstallerPath) Then
                            tmpInstallerPath = Path.Combine(tmpInstallerPathPrefix, "Media\550616.exe")
                            tmpAttemptedInstallerPaths.Add(tmpInstallerPath)
                        End If

                        If Not File.Exists(tmpInstallerPath) Then
                            tmpInstallerPath = Path.Combine(tmpInstallerPathPrefix, "DynamicContent\dotnetfx35sp1.exe")
                            tmpAttemptedInstallerPaths.Add(tmpInstallerPath)
                        End If

                        If Not File.Exists(tmpInstallerPath) Then
                            tmpInstallerPath = "C:\dotnetfx35sp1.exe"
                            tmpAttemptedInstallerPaths.Add(tmpInstallerPath)
                        End If

                        If File.Exists(tmpInstallerPath) Then
                            LblMessage.Text = "Installing Framework 3.5"
                            Helpers.WriteLog("Installing Framework 3.5")

                            'Run installer
                            Dim tmpInstallerProcess As New Process
                            tmpInstallerProcess.StartInfo.FileName = tmpInstallerPath
                            tmpInstallerProcess.StartInfo.Arguments = "/Q /NORESTART"
                            tmpInstallerProcess.Start()

                            tmpInstallerProcess.WaitForExit()

                            LblMessage.Text = "Installed"
                            Helpers.WriteLog("Installed")

                            Helpers.RegisterPatchRan()

                        Else
                            Dim tmpMessage As String = "Installer not found." & vbCrLf
                            tmpMessage &= "Attempted these paths:" & vbCrLf

                            For Each tmpPath As String In tmpAttemptedInstallerPaths
                                tmpMessage &= vbTab & tmpPath & vbCrLf
                            Next

                            LblMessage.Text = tmpMessage

                            Helpers.WriteLog(tmpMessage)

                        End If
                    Else
                        LblMessage.Text = "Framework 3.5 already installed"
                        Helpers.WriteLog("Framework 3.5 already installed")

                        Helpers.RegisterPatchRan()

                    End If

                Else
                    Helpers.RegisterPatchRan()

                    LblMessage.Text = "Not Installed"

                End If
            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class