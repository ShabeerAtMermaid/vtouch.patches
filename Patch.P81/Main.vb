Imports System.IO
Imports Microsoft.Win32
Imports System.ServiceProcess
Imports System.Management

Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Public Sub StopService(ByVal PC As String)
        Dim obj As ManagementObject
        Dim inParams, outParams As ManagementBaseObject
        Dim Result As Integer

        ' we can only disable the service and wait
        ' until the next reboot to stop it

        obj = New ManagementObject("\\" & PC & "\root\cimv2:Win32_Service.Name='TeamViewer7'")

        ' change the Start Mode to Disabled
        If obj("StartMode").ToString <> "Disabled" Then
            ' Get an input parameters object for this method
            inParams = obj.GetMethodParameters("ChangeStartMode")
            inParams("StartMode") = "Disabled"

            ' do it!
            outParams = obj.InvokeMethod("ChangeStartMode", inParams, Nothing)
            Result = Convert.ToInt32(outParams("returnValue"))
            If Result <> 0 Then
                Throw New Exception("ChangeStartMode method error code " & Result)
            End If
        End If
    End Sub

    Private Sub InstallPatch()
        Try
            Try
                Dim sc As New ServiceController("TeamViewer 7")
                If (sc.Status = ServiceControllerStatus.Running) Then
                    sc.Stop()
                    System.Threading.Thread.Sleep(5000)
                    sc.Refresh()
                    StopService(My.Computer.Name)
                End If
            Catch ex As Exception
            End Try

            Dim tmpFileList As New ArrayList
            tmpFileList.Add("TeamViewer_Host.msi")
            For Each tmpFileName As String In tmpFileList
                Dim tmpFilePath As String = Path.Combine(Path.GetTempPath, tmpFileName)
                Dim tmpFileStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.Header & tmpFileName)
                Helpers.WriteStream(tmpFileStream, tmpFilePath)
            Next

            Dim tmpTeamViewerUnInstallerPath As String = Path.Combine(Path.GetTempPath, "teamviewer_Host.msi")
            Dim tmpTeamViewerProcess As New Process
            tmpTeamViewerProcess.StartInfo.FileName = "msiexec"
            tmpTeamViewerProcess.StartInfo.Arguments = "/uninstall """ & tmpTeamViewerUnInstallerPath & """  /quiet"
            tmpTeamViewerProcess.Start()
            tmpTeamViewerProcess.WaitForExit()
            'End If

            Try
                Dim regpathcommit As String = "cmd.exe /c ""ewfmgr c: -commit"""
                Shell(regpathcommit, AppWinStyle.NormalFocus, True)
            Catch ex As Exception
                Helpers.WriteLog(ex)
            End Try

            LblMessage.Text = "Installed"
            Helpers.WriteLog("Installed")
            Constants.RegData_RestartRequired.SetValue(1)
            Helpers.RegisterPatchRan()



            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class