Imports System.IO

Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then
                Dim tmpRegData_ModelName As New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\Common Settings", "ModelName")

                If tmpRegData_ModelName.ValueExists Then
                    If tmpRegData_ModelName.GetValue.ToString.ToLower.StartsWith("samsung") Then
                        Dim tmpProgramDirectory As String = Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "Samsung\Samsung PanelControl")

                        If Not Directory.Exists(tmpProgramDirectory) Then Directory.CreateDirectory(tmpProgramDirectory)

                        Dim tmpFileList As New ArrayList
                        tmpFileList.Add("MDCComApi.dll")
                        tmpFileList.Add("MDCCommApi.dll")
                        tmpFileList.Add("MDCCons.exe")
                        tmpFileList.Add("MDCCons.obj")
                        tmpFileList.Add("MDCCons.pch")
                        tmpFileList.Add("MDCCons.res")
                        tmpFileList.Add("MDCSerial.dll")
                        tmpFileList.Add("MDCtest.bsc")
                        tmpFileList.Add("MDCtest.exe")
                        tmpFileList.Add("MDCtest.obj")
                        tmpFileList.Add("MDCtest.pch")
                        tmpFileList.Add("MDCtest.res")
                        tmpFileList.Add("MDCtestDlg.obj")
                        tmpFileList.Add("PanelControl.exe")
                        tmpFileList.Add("StdAfx.obj")
                        tmpFileList.Add("StdAfx.sbr")
                        tmpFileList.Add("vc60.idb")

                        For Each tmpFileName As String In tmpFileList
                            Dim tmpFilePath As String = Path.Combine(tmpProgramDirectory, tmpFileName)
                            Dim tmpActiveXFlashStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.Header & tmpFileName)
                            Helpers.WriteStream(tmpActiveXFlashStream, tmpFilePath)
                        Next

                        'Set command in registry
                        Dim tmpRegData_PMSEvents_PCSleeping As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\vTouch Pro Power Management\Events\PCSleeping", "Samsung PanelControl")
                        tmpRegData_PMSEvents_PCSleeping.SetValue("""" & Path.Combine(tmpProgramDirectory, "PanelControl.exe") & """ off")

                        LblMessage.Text = "Installed"
                        Helpers.WriteLog("Installed")

                    Else
                        LblMessage.Text = "Not applied to this hardware"
                        Helpers.WriteLog("Not applied to this hardware")

                    End If
                Else
                    LblMessage.Text = "Not applied to this hardware"
                    Helpers.WriteLog("Not applied to this hardware")

                End If

                Helpers.RegisterPatchRan()

            Else
                LblMessage.Text = "Not Installed"

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub SendCommand(ByVal command As String)
        Try
            Dim tmpProgramDirectory As String = Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "Samsung\Samsung PanelControl")

            Dim tmpProcess As New Process
            tmpProcess.StartInfo.FileName = Path.Combine(tmpProgramDirectory, "MDCCons.exe")
            tmpProcess.StartInfo.Arguments = command
            tmpProcess.StartInfo.WindowStyle = ProcessWindowStyle.Minimized
            tmpProcess.Start()

            tmpProcess.WaitForExit(5000)

        Catch ex As Exception
            Helpers.WriteLog(ex)

        End Try
    End Sub

End Class