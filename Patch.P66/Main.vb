Imports System.IO
Imports Microsoft.Win32
Imports System.ServiceProcess



Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub
    
    Private Function CheckService(ByVal Name As String) As Boolean
        Try

            Dim colServices As Object
            Dim objService As Object
            colServices = GetObject("winmgmts:").ExecQuery _
                ("Select Name from Win32_Service where Name = '" & Name & "'")
            For Each objService In colServices
                If Len(objService.Name) Then
                    CheckService = True
                End If
            Next
            colServices = Nothing
        Catch ex As Exception

        End Try
    End Function
    Private Sub InstallPatch()

        'Dim controller As New ServiceController

        Try
            'controller.MachineName = "."
            'controller.ServiceName = "vTouch Pro Display Services Monitor"
            'Dim status As String = controller.Status.ToString

            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then
                Dim install As Boolean = False

                If Not Directory.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "vTouch Pro\vTouch Pro Display Services Monitor")) Then
                    install = True
                End If

                If Not install Then
                    If Not Directory.Exists("D" & Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "vTouch Pro\vTouch Pro Display Services Monitor").Substring(1)) Then
                        install = True
                    End If
                End If

                If install Then

                    'Display Services Monitor installer
                    'Try
                    '    ' Stop the service
                    '    If (status.ToLower() = "running") Then
                    '        controller.Stop()
                    '        controller.Refresh()
                    '        UnInstalledDSMonitor()
                    '        System.Threading.Thread.Sleep(1000 * 3)
                    '    End If
                    '    ' Start the service
                    '    'UnInstalledDSMonitor()
                    '    'Shell("InstallUtil.exe /u ""C:\Program Files\vTouch Pro\vTouch Pro Display Services Monitor\vTouch Pro Display Services Monitor.exe""", AppWinStyle.Hide, True)
                    '    'System.Diagnostics.Process.Start("sc.exe", "delete vTouch Pro Display Services Monitor")
                    '    'Shell("sc.exe delete vTouch Pro Display Services Monitor", AppWinStyle.NormalFocus, True)
                    'Catch ex As Exception
                    '    'System.Diagnostics.Process.Start("sc.exe", "delete vTouch Pro Display Services Monitor")
                    'End Try
                    Dim bInstalled As Boolean = CheckService("vTouch Pro Display Services Monitor")
                    If (Not bInstalled) Then
                        Dim tmpMonitorInstallerPath As String = Path.Combine(Path.GetTempPath, "dsmonitor.msi")
                        Dim tmpMonitorStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.Header & "dsmonitor.msi")
                        Helpers.WriteStream(tmpMonitorStream, tmpMonitorInstallerPath)

                        Dim tmpMonitorProcess As New Process
                        tmpMonitorProcess.StartInfo.FileName = tmpMonitorInstallerPath
                        'tmpMonitorProcess.StartInfo.Arguments = "/s"
                        tmpMonitorProcess.Start()

                        tmpMonitorProcess.WaitForExit()

                        LblMessage.Text = "Installed"
                        Helpers.WriteLog("Installed")

                        Constants.RegData_RestartRequired.SetValue(1)
                    Else
                        LblMessage.Text = "vTouch Pro Display Services Monitory already installed"
                        Helpers.WriteLog("vTouch Pro Display Services Monitory already installed")
                    End If

                Else
                    LblMessage.Text = "vTouch Pro Display Services Monitory already installed"
                    Helpers.WriteLog("vTouch Pro Display Services Monitory already installed")
                End If
                Helpers.RegisterPatchRan()
            Else
                LblMessage.Text = "Not Installed"
            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True
        Finally
            'If Not controller.DisplayName() Is "" Then
            '    If (controller.Status.ToString().ToLower() = "stopped") Then
            '        controller.Start()
            '    End If
            'End If
        End Try
    End Sub

End Class