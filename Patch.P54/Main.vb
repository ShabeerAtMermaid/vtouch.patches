Imports System.IO

Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Function GetDiskSize(ByVal drive As String) As Long

        Try
            Dim disk As New System.Management.ManagementObject("win32_logicaldisk.deviceid=""" & drive & """")
            disk.Get()
            Return Long.Parse(disk("Size"))
        Catch ex As Exception
            GetDiskSize = 0
        End Try

    End Function

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then
                Dim tmpRegData_PagingFiles As RegData = New RegData(Enums.Hive.LocalMachine, "SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management", "PagingFiles")

                Dim Cdisksize As Long = GetDiskSize("c:")
                Dim Ddisksize As Long = GetDiskSize("d:")
                Dim pagingSizes As New ArrayList
                If Cdisksize <> 0 Then
                    pagingSizes.Add("c:\pagefile.sys 2048 2048")
                End If
                If Ddisksize <> 0 Then
                    pagingSizes.Add("d:\pagefile.sys 2048 2048")
                End If

                If pagingSizes.Count > 0 Then
                    Dim datalist As String() = CType(pagingSizes.ToArray(GetType(String)), String())
                    tmpRegData_PagingFiles.SetValue(datalist)
                    Dim f As Integer = 0
                End If

                LblMessage.Text = "Installed"
                Helpers.WriteLog("Installed")

                Helpers.RegisterPatchRan()

            Else
                LblMessage.Text = "Not Installed"

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class