Imports System.IO

Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then
                Dim tmpRegData_FlashVersion As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\Macromedia\FlashPlayer", "CurrentVersion")

                Dim tmpCurrentVersion As String = tmpRegData_FlashVersion.GetValue

                If Not String.IsNullOrEmpty(tmpCurrentVersion) Then
                    If Not tmpCurrentVersion.StartsWith("10,") Then
                        ''ActiveX installer
                        'Dim tmpActiveXFlashInstallerPath As String = Path.Combine(Path.GetTempPath, "flash10.exe")
                        'Dim tmpActiveXFlashStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Header & "flash10_activex.exe")
                        'WriteStream(tmpActiveXFlashStream, tmpActiveXFlashInstallerPath)

                        'Dim tmpActiveXFlashProcess As New Process
                        'tmpActiveXFlashProcess.StartInfo.FileName = tmpActiveXFlashInstallerPath
                        'tmpActiveXFlashProcess.StartInfo.Arguments = "/s"
                        'tmpActiveXFlashProcess.Start()

                        'tmpActiveXFlashProcess.WaitForExit()

                        ''Plugin installer
                        'Dim tmpPluginFlashInstallerPath As String = Path.Combine(Path.GetTempPath, "flash10.exe")
                        'Dim tmpPluginFlashStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Header & "flash10_plugin.exe")
                        'WriteStream(tmpPluginFlashStream, tmpPluginFlashInstallerPath)

                        'Dim tmpPluginFlashProcess As New Process
                        'tmpPluginFlashProcess.StartInfo.FileName = tmpPluginFlashInstallerPath
                        'tmpPluginFlashProcess.StartInfo.Arguments = "/s"
                        'tmpPluginFlashProcess.Start()

                        'tmpPluginFlashProcess.WaitForExit()

                    End If

                Else
                    'Me.WriteLog("Flash 10 already installed")

                End If

                LblMessage.Text = "Installed"
                Helpers.WriteLog("Installed")

                Helpers.RegisterPatchRan()

            Else
                LblMessage.Text = "Not Installed"

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class