Imports System.IO

Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then

                Dim tmpStr As String
                Dim tmpstrarr As String()
                Dim tmpInt As Integer
                Dim tmpArray As New ArrayList
                Dim tmpBool As Boolean
                Dim tmpdate As Date
                Dim temlong As Long

                tmpStr = DisplayServicesSettings.ServerAddress
                tmpStr = DisplayServicesSettings.UpdateServer
                tmpInt = DisplayServicesSettings.UpdateServerPort
                tmpStr = DisplayServicesSettings.ModelName
                tmpInt = DisplayServicesSettings.CustomerId
                tmpInt = DisplayServicesSettings.ComputerId
                tmpStr = DisplayServicesSettings.ModelName
                tmpStr = DisplayServicesSettings.LocalResourcePath
                tmpStr = DisplayServicesSettings.DownloadManagerBackupFolder
                tmpStr = DisplayServicesSettings.DownloadManagerResumeFolder
                tmpstrarr = DisplayServicesSettings.InstalledPatches
                tmpInt = DisplayServicesSettings.ActiveScreens
                tmpArray = DisplayServicesSettings.GetExtensions
                tmpStr = DisplayServicesSettings.GetProcesses
                tmpStr = DisplayServicesSettings.DFSServerProxyAddress
                tmpInt = DisplayServicesSettings.DFSStatusCheckInterval
                tmpInt = DisplayServicesSettings.DFSStatusCheckTolerance
                tmpInt = DisplayServicesSettings.DisplayStartupDelay
                tmpInt = DisplayServicesSettings.UpdateCounter
                tmpInt = DisplayServicesSettings.UpdateInterval
                tmpStr = DisplayServicesSettings.UpdateVersion
                tmpInt = DisplayServicesSettings.BackupExpireTime
                tmpBool = DisplayServicesSettings.StartDisplay
                'tmpBool = DisplayServicesSettings.CleanShutDownPerformed
                tmpdate = DisplayServicesSettings.LastUpdate
                tmpInt = DisplayServicesSettings.DisplayServicesPort
                tmpInt = DisplayServicesSettings.LogLevel
                tmpInt = DisplayServicesSettings.ConfigLogLevel
                tmpBool = DisplayServicesSettings.DFSEnabled

                ''''''''''DFS Settings''''''''''''''''''''
                temlong = DFSSettings.DFSchunksize
                tmpInt = DFSSettings.DFSlogs
                temlong = DFSSettings.DFSlogsize
                temlong = DFSSettings.DFSEnableRestore
                temlong = DFSSettings.DFSTimeout
                temlong = DFSSettings.DFSEnableCleanup


                '''''''''''''Display settings''''''''''''
                tmpBool = DisplaySettings.ReportStatistics
                tmpInt = DisplaySettings.DisplayType
                tmpStr = DisplaySettings.InstallPath
                tmpInt = DisplaySettings.UpdateCounter
                tmpInt = DisplaySettings.UpdateInterval
                tmpStr = DisplaySettings.UpdateVersion
                tmpInt = DisplaySettings.LogLevel
                tmpdate = DisplaySettings.LastUpdate
                tmpBool = DisplaySettings.WatchDogEnabled
                tmpStr = DisplaySettings.GetDisplays("0")
                tmpStr = DisplaySettings.GetDisplays("1")
                tmpStr = DisplaySettings.GetDisplays("2")
                tmpStr = DisplaySettings.GetDisplays("3")



                Helpers.RegisterPatchRan()
                LblMessage.Text = "Installed"
                Helpers.WriteLog("Installed")

            Else
                LblMessage.Text = "Not installed"
                Helpers.WriteLog("Not installed")

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class