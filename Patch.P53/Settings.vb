Imports System.Threading
Imports System.Xml
Imports System.IO
Imports System.Globalization
Imports System.Management
Imports Microsoft.Win32


Public MustInherit Class MachineSettings

#Region "Variables"

    Private Shared _Lock As New Object
    Private Shared _ConfigFile As String = ConfigPath

#End Region

#Region "Properties"

    Public Shared Property ConfigFile As String
        Get
            Return _ConfigFile
        End Get
        Set(ByVal value As String)
            _ConfigFile = value
        End Set
    End Property

#End Region

    Public Shared Function GetSetting(ByVal xpath As String, ByVal defaultValue As String) As String
        Try
            xpath = xpath.ToLower()
            xpath = "//settings/" + xpath
            Dim tmpConfigDocument As XmlDocument = OpenConfig(3)
            If (defaultValue = "xml") Then
                Dim tmpxml As String = String.Empty
                Dim Xn As XmlNodeList = tmpConfigDocument.SelectNodes(xpath)
                Dim xNode As XmlNode
                For Each xNode In Xn
                    tmpxml += xNode.InnerXml()
                Next
                Return tmpxml
            Else
                Dim tmpSettingNode As XmlNode = tmpConfigDocument.SelectSingleNode(xpath)
                If Not tmpSettingNode Is Nothing Then
                    Return tmpSettingNode.InnerText
                Else
                    Return defaultValue
                End If
            End If
        Catch exception As Exception
            Return ""
        Finally
            CloseConfig()
        End Try
    End Function

    Public Shared Sub SetSetting(ByVal xpath As String, ByVal value As String)
        Try
            xpath = xpath.ToLower()
            xpath = "//settings/" + xpath
            Dim tmpConfigDocument As XmlDocument = OpenConfig(3)

            Dim tmpSettingNode As XmlNode = tmpConfigDocument.SelectSingleNode(xpath)

            If tmpSettingNode Is Nothing Then
                tmpSettingNode = GetNode(tmpConfigDocument, xpath)
            End If

            tmpSettingNode.InnerText = value

            tmpConfigDocument.Save(ConfigFile)

        Finally
            CloseConfig()

        End Try
    End Sub

    Private Shared Function GetNode(ByVal document As XmlDocument, ByVal path As String) As XmlNode

        Dim tmpNode As XmlNode = document.SelectSingleNode(path)

        If tmpNode Is Nothing Then
            Dim tmpNodeName As String = path.Substring(path.LastIndexOf("/") + 1)
            Dim tmpParentPath As String = path.Substring(0, path.LastIndexOf("/"))

            Dim tmpParentNode As XmlNode = GetNode(document, tmpParentPath)

            tmpNode = document.CreateElement(tmpNodeName)
            tmpParentNode.AppendChild(tmpNode)

        End If

        Return tmpNode

    End Function

    Private Shared Function OpenConfig(ByVal attempts As Integer) As XmlDocument
        Monitor.Enter(_Lock)

        Try
            LockConfig()

            If Not File.Exists(ConfigFile) Then
                File.AppendAllText(ConfigFile, "<?xml version=""1.0"" encoding=""iso-8859-1"" ?>" & Environment.NewLine)
                File.AppendAllText(ConfigFile, "  <settings>" & Environment.NewLine)
                File.AppendAllText(ConfigFile, "  </settings>" & Environment.NewLine)
            End If

            Dim tmpConfigDocument As New XmlDocument
            tmpConfigDocument.Load(ConfigFile)

            Return tmpConfigDocument

        Catch ex As Exception
            If attempts > 1 Then
                Thread.Sleep(1000)

                Return OpenConfig(attempts - 1)

            Else
                Throw New FileLoadException("Config file is locked and not accessible", ConfigFile, ex)
            End If

        End Try
    End Function

    Private Shared Sub CloseConfig()
        Try
            UnlockConfig()

        Finally
            Monitor.Exit(_Lock)
        End Try
    End Sub

#Region "Lock"

#Region "Variables"

    Private Shared _CurrentLockStream As FileStream

#End Region

#Region "Properties"

    Private Shared ReadOnly Property LockFile As String
        Get
            Return ConfigFile & ".lock"

        End Get
    End Property

#End Region

    Private Shared Sub LockConfig()
        'Create Lock file if it doesn't exist
        If Not File.Exists(LockFile) Then InitializeLockFile()

        'Open the LockFile so others cant use it
        _CurrentLockStream = New FileStream(LockFile, FileMode.OpenOrCreate, IO.FileAccess.ReadWrite, FileShare.None)

    End Sub

    Private Shared Sub UnlockConfig()
        If Not _CurrentLockStream Is Nothing Then
            _CurrentLockStream.Close()
            _CurrentLockStream.Dispose()
            _CurrentLockStream = Nothing
        End If
    End Sub

    Private Shared Sub InitializeLockFile()
        File.Create(LockFile).Close()
        SetAttr(LockFile, FileAttribute.Hidden Or FileAttribute.System)

    End Sub

#End Region

    Public Shared ReadOnly Property ConfigPath() As String
        Get
            Dim tmpDirectory As String = "C:\"
            For Each drive_info As DriveInfo In DriveInfo.GetDrives()
                If (drive_info.Name = "D:\") Then
                    tmpDirectory = drive_info.Name
                    Exit For
                End If
            Next drive_info
            tmpDirectory = Path.Combine(tmpDirectory, "vTouch Pro Configs\Settings.xml")

            Dim tmpRegData_ConfigPath As RegData = New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\Common Settings", "Configpath")
            If tmpRegData_ConfigPath.ValueExists Then
                Dim tmpConfigPath As String = tmpRegData_ConfigPath.GetValue().ToString()
                If String.IsNullOrEmpty(tmpConfigPath) Then
                    tmpRegData_ConfigPath.SetValue(tmpDirectory)
                Else
                    tmpDirectory = tmpConfigPath
                End If
            Else
                tmpRegData_ConfigPath.SetValue(tmpDirectory)
            End If
            Dim index As Integer = tmpDirectory.LastIndexOf("\")
            Dim tmpdir As String = tmpDirectory.Substring(0, index)
            If Not Directory.Exists(tmpdir) Then
                Directory.CreateDirectory(tmpdir)
            End If
            Return tmpDirectory
        End Get
    End Property

    Public Shared Property ServerAddress() As String
        Get
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_COMMON_SETTINGS & "/ServerAddress", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_COMMON_SETTINGS, "ServerAddress") Then
                    '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "ServerAddress").ToString(), GetType(String))
                    '    tmpPath = tmpPath.Replace(":8001", "")
                    'Else
                    '    tmpPath = "localhost"
                    'End If
                    tmpPath = "localhost"
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_COMMON_SETTINGS, "ServerAddress", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "localhost"
                        End If
                    Catch ex As Exception
                        tmpPath = "localhost"
                    End Try
                    SetSetting(CON_PATH_COMMON_SETTINGS & "/ServerAddress", tmpPath)
                End If
                'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_COMMON_SETTINGS, "ServerAddress") Then
                '    RegistryEditor.SetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "ServerAddress", "localhost")
                '    tmpPath = String.Empty

                'Else
                '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "ServerAddress"), GetType(String))

                '    tmpPath = tmpPath.Replace(":8001", "")

                '    RegistryEditor.SetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "ServerAddress", tmpPath)

                'End If

                If tmpPath.IndexOf(":") <> -1 Then
                    tmpPath = tmpPath

                ElseIf Not String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = tmpPath & ":8001"

                End If

                Return tmpPath
            Catch ex As Exception
                Return "localhost:8001"
            End Try
        End Get
        Set(ByVal value As String)
            SetSetting(CON_PATH_COMMON_SETTINGS & "/ServerAddress", value)
        End Set
    End Property

    ' Public MustOverride ReadOnly Property MessageServerAddress() As String

    Public Shared ReadOnly Property UpdateServer() As String
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_COMMON_SETTINGS, "UpdateServer") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "UpdateServer", "update.vtouch.dk")

            'End If

            'Return RegistryEditor.GetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "UpdateServer")
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_COMMON_SETTINGS & "/UpdateServer", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "update.vtouch.dk"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_COMMON_SETTINGS, "UpdateServer") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "UpdateServer").ToString()
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_COMMON_SETTINGS, "UpdateServer", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "update.vtouch.dk"
                        End If
                    Catch ex As Exception
                        tmpPath = "update.vtouch.dk"
                    End Try

                    SetSetting(CON_PATH_COMMON_SETTINGS & "/UpdateServer", tmpPath)
                End If

                Return tmpPath
            Catch ex As Exception
                Return "update.vtouch.dk"
            End Try

        End Get
    End Property
    Public Shared ReadOnly Property UpdateServerPort() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_COMMON_SETTINGS, "CustomerId") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "CustomerId", 0)
            'End If
            'Return EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "CustomerId"), GetType(Integer))

            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_COMMON_SETTINGS & "/UpdateServerPort", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "9099"

                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_COMMON_SETTINGS, "CustomerId") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "CustomerId").ToString()
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_COMMON_SETTINGS, "UpdateServerPort", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "9099"
                        End If
                    Catch ex As Exception
                        tmpPath = "9099"
                    End Try

                    SetSetting(CON_PATH_COMMON_SETTINGS & "/UpdateServerPort", tmpPath)
                End If
                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 9099
            End Try
        End Get
    End Property
    Public Shared Property ModelName() As String
        Get
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_COMMON_SETTINGS & "/ModelName", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "MAIN"
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_COMMON_SETTINGS, "ModelName", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "MAIN"
                        End If
                    Catch ex As Exception
                        tmpPath = "MAIN"
                    End Try

                End If
                SetSetting(CON_PATH_COMMON_SETTINGS & "/ModelName", tmpPath)
                Return tmpPath
            Catch ex As Exception
                Return "MAIN"
            End Try

        End Get
        Set(ByVal value As String)
            SetSetting(CON_PATH_COMMON_SETTINGS & "/ModelName", value)
        End Set
    End Property
    
    Public Shared ReadOnly Property CustomerId() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_COMMON_SETTINGS, "CustomerId") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "CustomerId", 0)
            'End If
            'Return EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "CustomerId"), GetType(Integer))

            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_COMMON_SETTINGS & "/CustomerId", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "0"

                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_COMMON_SETTINGS, "CustomerId") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "CustomerId").ToString()
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_COMMON_SETTINGS, "CustomerId", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "0"
                        End If
                    Catch ex As Exception
                        tmpPath = "0"
                    End Try
                    SetSetting(CON_PATH_COMMON_SETTINGS & "/CustomerId", tmpPath)
                End If
                'Microsoft.Win32.Registry.SetValue(REG_PATH_COMMON_SETTINGS, "CustomerId", tmpPath, Microsoft.Win32.RegistryValueKind.DWord)
                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property

    Public Shared Property ComputerId() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_COMMON_SETTINGS, "ComputerID") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "ComputerID", 0)
            'End If
            'Return EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "ComputerID"), GetType(Integer))
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_COMMON_SETTINGS & "/ComputerID", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "0"

                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_COMMON_SETTINGS, "ComputerID") Then
                    '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "ComputerID").ToString(), GetType(String))
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_COMMON_SETTINGS, "ComputerID", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "0"
                        End If
                    Catch ex As Exception
                        tmpPath = "0"
                    End Try

                    SetSetting(CON_PATH_COMMON_SETTINGS & "/ComputerID", tmpPath)
                End If

                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 0
            End Try

        End Get
        Set(ByVal Value As Integer)
            'RegistryEditor.SetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "ComputerID", Value)
            SetSetting(CON_PATH_COMMON_SETTINGS & "/ComputerID", Value.ToString())

        End Set
    End Property

    Public Shared ReadOnly Property UniqueId() As String
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_COMMON_SETTINGS, "UniqueId") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "UniqueId", Me.GetMACAddress)
            'ElseIf String.IsNullOrEmpty(Me.EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "UniqueId"), GetType(String))) Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "UniqueId", Me.GetMACAddress)

            'End If

            'Return Me.EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "UniqueId"), GetType(String))

            Dim tmpPath As String
            Try
                tmpPath = GetSetting(CON_PATH_COMMON_SETTINGS & "/UniqueId", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = GetMACAddress()
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_COMMON_SETTINGS, "UniqueId") Then
                    '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "UniqueId").ToString(), GetType(String))
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_COMMON_SETTINGS, "UniqueId", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = GetMACAddress()
                        End If
                    Catch ex As Exception
                        tmpPath = GetMACAddress()
                    End Try

                    SetSetting(CON_PATH_COMMON_SETTINGS & "/UniqueId", tmpPath)
                End If

                Return tmpPath
            Catch ex As Exception
                tmpPath = GetMACAddress()
                Return tmpPath
            End Try
        End Get
    End Property

    Public Shared Property LocalResourcePath() As String
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_COMMON_SETTINGS, "LocalResourcePath") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "LocalResourcePath", System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) & "\vTouch Pro Resources\")

            'End If

            'Dim path As String = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "LocalResourcePath"), GetType(String))

            'If Not path = String.Empty Then
            '    If Not path.EndsWith("\") Then path = path & "\"

            'End If


            'Return path
            Dim tmpPath As String
            Try
                tmpPath = GetSetting(CON_PATH_COMMON_SETTINGS & "/LocalResourcePath", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) & "\vTouch Pro Resources\"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_COMMON_SETTINGS, "LocalResourcePath") Then
                    '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "LocalResourcePath").ToString(), GetType(String))
                    '    If Not tmpPath = String.Empty Then
                    '        If Not tmpPath.EndsWith("\") Then tmpPath = tmpPath & "\"
                    '    End If
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_COMMON_SETTINGS, "LocalResourcePath", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) & "\vTouch Pro Resources\"
                        End If
                    Catch ex As Exception
                        tmpPath = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) & "\vTouch Pro Resources\"
                    End Try

                    If Not tmpPath = String.Empty Then
                        If Not tmpPath.EndsWith("\") Then tmpPath = tmpPath & "\"
                    End If
                    SetSetting(CON_PATH_COMMON_SETTINGS & "/LocalResourcePath", tmpPath)
                End If
                'Microsoft.Win32.Registry.SetValue(REG_PATH_COMMON_SETTINGS, "LocalResourcePath", tmpPath, Microsoft.Win32.RegistryValueKind.String)
                Return tmpPath
            Catch ex As Exception
                tmpPath = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) & "\vTouch Pro Resources\"
                Return tmpPath
            End Try

        End Get
        Set(ByVal value As String)
            SetSetting(CON_PATH_COMMON_SETTINGS & "/LocalResourcePath", value)
            'Microsoft.Win32.Registry.SetValue(REG_PATH_COMMON_SETTINGS, "LocalResourcePath", value, Microsoft.Win32.RegistryValueKind.String)
        End Set
    End Property

    Public Shared ReadOnly Property DownloadManagerBackupFolder() As String
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_COMMON_SETTINGS, "DownloadManagerBackupFolder") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "DownloadManagerBackupFolder", System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) & "\vTouch Pro Backup\")

            'End If

            'Return RegistryEditor.GetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "DownloadManagerBackupFolder")
            Dim tmpPath As String
            Try

                tmpPath = GetSetting(CON_PATH_COMMON_SETTINGS & "/DownloadManagerBackupFolder", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    'tmpPath = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) & "\vTouch Pro Backup\"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_COMMON_SETTINGS, "DownloadManagerBackupFolder") Then
                    '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "DownloadManagerBackupFolder").ToString(), GetType(String))
                    '    If Not tmpPath = String.Empty Then
                    '        If Not tmpPath.EndsWith("\") Then tmpPath = tmpPath & "\"
                    '    End If
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_COMMON_SETTINGS, "DownloadManagerBackupFolder", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) & "\vTouch Pro Backup\"
                        End If
                    Catch ex As Exception
                        tmpPath = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) & "\vTouch Pro Backup\"
                    End Try

                    If Not tmpPath = String.Empty Then
                        If Not tmpPath.EndsWith("\") Then tmpPath = tmpPath & "\"
                    End If
                    SetSetting(CON_PATH_COMMON_SETTINGS & "/DownloadManagerBackupFolder", tmpPath)
                End If
                Return tmpPath
            Catch ex As Exception
                tmpPath = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) & "\vTouch Pro Backup\"
                Return tmpPath
            End Try

        End Get
    End Property

    Public Shared ReadOnly Property DownloadManagerResumeFolder() As String
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_COMMON_SETTINGS, "DownloadManagerResumeFolder") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "DownloadManagerResumeFolder", System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) & "\vTouch Pro Resume\")

            'End If
            'Return RegistryEditor.GetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "DownloadManagerResumeFolder")

            Dim tmpPath As String
            Try
                tmpPath = GetSetting(CON_PATH_COMMON_SETTINGS & "/DownloadManagerResumeFolder", "")
                'If String.IsNullOrEmpty(tmpPath) Then
                '    tmpPath = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) & "\vTouch Pro Resume\"
                '    If RegistryEditor.ValueExists(HIVE, REG_PATH_COMMON_SETTINGS, "DownloadManagerResumeFolder") Then
                '        tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "DownloadManagerResumeFolder").ToString(), GetType(String))
                '    End If
                '    SetSetting(CON_PATH_COMMON_SETTINGS & "/DownloadManagerResumeFolder", tmpPath)
                'End If
                If String.IsNullOrEmpty(tmpPath) Then
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_COMMON_SETTINGS, "DownloadManagerResumeFolder", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) & "\vTouch Pro Resume\"
                        End If
                    Catch ex As Exception
                        tmpPath = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) & "\vTouch Pro Resume\"
                    End Try

                End If
                If Not tmpPath = String.Empty Then
                    If Not tmpPath.EndsWith("\") Then tmpPath = tmpPath & "\"
                End If
                SetSetting(CON_PATH_COMMON_SETTINGS & "/DownloadManagerResumeFolder", tmpPath)
                Return tmpPath
            Catch ex As Exception
                tmpPath = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) & "\vTouch Pro Resume\"
                Return tmpPath
            End Try

        End Get
    End Property
    Public Shared ReadOnly Property InstalledPatches() As String()
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_COMMON_SETTINGS, "DownloadManagerResumeFolder") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "DownloadManagerResumeFolder", System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) & "\vTouch Pro Resume\")
            'End If
            'Return RegistryEditor.GetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "DownloadManagerResumeFolder")
            Dim tmpPath As String()
            Try
                tmpPath = GetSetting(CON_PATH_UPDATER_CLIENT & "/InstalledPatches", "").Split(" ")
                If String.IsNullOrEmpty(String.Join(" ", tmpPath)) Then
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_UPDATER_CLIENT, "InstalledPatches", tmpPath)
                    Catch ex As Exception
                        SetSetting(CON_PATH_UPDATER_CLIENT & "/InstalledPatches", String.Join(" ", tmpPath))
                        Return GetSetting(CON_PATH_UPDATER_CLIENT & "/InstalledPatches", "").Split(" ")
                    End Try
                End If
                SetSetting(CON_PATH_UPDATER_CLIENT & "/InstalledPatches", String.Join(" ", tmpPath))
                Return tmpPath
            Catch ex As Exception
                Return GetSetting(CON_PATH_UPDATER_CLIENT & "/InstalledPatches", "").Split(" ")
            End Try
        End Get
    End Property

    Public Shared ReadOnly Property ActiveScreens() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "ActiveScreens") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAY, "ActiveScreens", 3)
            'End If
            'Return EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "ActiveScreens"), GetType(Integer))
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAY & "/ActiveScreens", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "3"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "ActiveScreens") Then
                    '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "ActiveScreens").ToString(), GetType(String))
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAY, "ActiveScreens", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "3"
                        End If
                    Catch ex As Exception
                        tmpPath = "3"
                    End Try

                    SetSetting(CON_PATH_DISPLAY & "/ActiveScreens", tmpPath)
                End If

                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 3
            End Try

        End Get
    End Property

    'Public MustOverride ReadOnly Property LogLevel() As Integer

    'Public MustOverride Property LastMajorUpdate() As Integer

    'Public MustOverride Property LastMinorUpdate() As Integer

    'Public MustOverride Property LastBuildUpdate() As Integer

    'Public MustOverride Property LastUpdate() As Date

#Region "Constants"

    Protected Const CON_PATH_COMMON_SETTINGS As String = "vTouchPro/CommonSettings"
    Protected Const CON_PATH_DISPLAY As String = "vTouchPro/vTouchProDisplay"
    Protected Const CON_PATH_UPDATER_CLIENT As String = "mermaidUpdater/mermaidUpdateClient"

    Protected Const REG_PATH_COMMON_SETTINGS As String = "HKEY_LOCAL_MACHINE\Software\mermaid technology\vTouch Pro\Common Settings"
    Protected Const REG_PATH_DISPLAY As String = "HKEY_LOCAL_MACHINE\Software\mermaid technology\vTouch Pro\vTouch Pro Display"
    Protected Const REG_PATH_DISPLAYSERVICES As String = "HKEY_LOCAL_MACHINE\Software\mermaid technology\vTouch Pro\vTouch Pro DisplayServices"
    Protected Const REG_PATH_DFS As String = "HKEY_LOCAL_MACHINE\Software\mermaid technology\vTouch Pro\vTouch Pro DFS"
    Protected Const REG_PATH_UPDATER_CLIENT As String = "HKEY_LOCAL_MACHINE\Software\mermaid technology\mermaid Updater\mermaid Update Client"

    'Friend Const REG_PATH_DISPLAY As String = "Software\Victor Soft\vTouch Pro\vTouch Pro Display"
    'Private Const REG_PATH_DISPLAY_CHANNELS As String = "Software\Victor Soft\vTouch Pro\vTouch Pro Display\Channels"

    ''Private Const REG_PATH_DOWNLOADMANAGER As String = "Software\Victor Soft\vTouch Pro\vTouch Pro Distributed File System"
    'Private Const REG_PATH_DOWNLOADMANAGER_USERSETTINGS As String = "Software\Victor Soft\vTouch Pro\vTouch Pro Distributed File System\UserSettings"
    'Private Const REG_PATH_DOWNLOADMANAGER_COMMUNICATION As String = "Software\Victor Soft\vTouch Pro\vTouch Pro Distributed File System\Communication"
    'Private Const REG_PATH_DOWNLOADMANAGER_COMMUNICATION_FTP As String = "Software\Victor Soft\vTouch Pro\vTouch Pro Distributed File System\Communication\Ftp"
    'Private Const REG_PATH_DOWNLOADMANAGER_COMMUNICATION_REMOTING As String = "Software\Victor Soft\vTouch Pro\vTouch Pro Distributed File System\Communication\Remoting"

#End Region

    Public Sub New()
        MyBase.New()

        'LogWriter.ApplicationName = "vTouch Pro Display Settings"
        'LogWriter.WriteToEventLog = True
        'LogWriter.WriteToConsole = True
        'LogWriter.AttachGlobalExceptionHandler()

    End Sub

    Protected Shared Function EnsureNoNullValue(ByVal value As Object, ByVal expectedType As Type) As Object
        If value Is Nothing Then
            Select Case expectedType.ToString
                Case GetType(String).ToString
                    Return String.Empty

                Case GetType(Integer).ToString
                    Return 0

                Case GetType(Boolean).ToString
                    Return False

            End Select
        Else
            If Not value.GetType Is expectedType Then
                Select Case expectedType.ToString
                    Case GetType(String).ToString
                        Return value

                    Case GetType(Integer).ToString
                        Try
                            Return Convert.ToInt32(value)

                        Catch ex As Exception
                            Return 0
                        End Try

                    Case GetType(Boolean).ToString
                        Try
                            If TypeOf value Is Integer Then
                                If value > 0 Then
                                    Return True

                                Else
                                    Return False

                                End If
                            End If

                            Return Convert.ToBoolean(value)

                        Catch ex As Exception
                            Return False
                        End Try

                End Select
            End If
        End If

        Return value

    End Function

    Private Shared Function GetMACAddress() As String
        Dim res As String = String.Empty

        Try
            Dim mc As ManagementClass
            Dim moc As ManagementObjectCollection
            Dim mo As ManagementObject

            mc = New ManagementClass("Win32_NetworkAdapterConfiguration")
            moc = mc.GetInstances() ' Get all network adapter instances

            For Each mo In moc
                If mo.Item("IPEnabled") = True Then
                    res = mo.Item("MacAddress").ToString

                    Exit For

                End If

                mo.Dispose()

            Next

        Catch ex As Exception
            'DELETE: Console
            Console.WriteLine(ex.Message & " - " & ex.StackTrace)

        End Try

        Return res

    End Function

End Class

Public Class DisplayServicesSettings
    Inherits MachineSettings

    Public Sub New()
    End Sub
    Public Shared Sub SetValue(ByVal path As String, ByVal key As String, ByVal value As String)
        Dim tmpArray As New ArrayList
        Dim tmpExtensionsKeyName As String = path & "/" & key
        SetSetting(tmpExtensionsKeyName, value)
        'ConfigManager.WriteSetting(tmpExtensionsKeyName, value)
    End Sub

    Public Shared ReadOnly Property GetValue(ByVal path As String, ByVal key As String) As String
        Get
            'Dim tmpExtensionsKeyName As String = path & "/" & key
            'Dim xmlExtensions As String = ConfigManager.ReadSetting(tmpExtensionsKeyName, "")
            'Return xmlExtensions
            path = path & "/" & key
            Return GetSetting(path, "")
        End Get
    End Property

    Public Shared ReadOnly Property GetXmlStructure(ByVal path As String) As String
        Get
            'test
            Return GetSetting(path, "xml")
            'end test
            'Dim xmlCommands As String = Me.ConfigManager.GetXMLNodes(path, "")
            'Return xmlCommands

        End Get
    End Property
    
    Public Shared ReadOnly Property GetExtensions() As ArrayList
        Get
            Dim tmpArray As New ArrayList
            Dim tmpExtensionsKeyName As String = CON_PATH_DISPLAYSERVICES + "/Extensions"

            Dim xmlExtensions As String = GetSetting(tmpExtensionsKeyName, "xml")
            'Dim xmlExtensions As String = Me.ConfigManager.GetXMLNodes(tmpExtensionsKeyName, "")
            If Not String.IsNullOrEmpty(xmlExtensions) Then
                Try
                    Dim tmpXmlDoc As New System.Xml.XmlDocument()
                    tmpXmlDoc.LoadXml(xmlExtensions)
                    For Each node As System.Xml.XmlNode In tmpXmlDoc.FirstChild.ChildNodes
                        tmpArray.Add(node.Name & "|" & node.InnerText)
                    Next
                Catch ex As Exception

                End Try
            Else
                Dim regKey As RegistryKey
                regKey = Registry.LocalMachine.CreateSubKey("Software\mermaid technology\vTouch Pro\vTouch Pro DisplayServices\Extensions")
                Dim tmpPath As String
                For Each tmpExtensionValue As String In regKey.GetValueNames
                    tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAYSERVICES & "\Extensions", tmpExtensionValue.ToString(), "").ToString()
                    SetSetting(CON_PATH_DISPLAYSERVICES & "/Extensions/" & tmpExtensionValue.ToString(), tmpPath)
                    tmpArray.Add(tmpExtensionValue.ToString() & "|" & tmpPath)
                Next
            End If

            Return tmpArray
            'If Not RegistryEditor.KeyExists(RegistryEditor.Hive.LOCALMACHINE, tmpExtensionsKeyName) Then
            '    RegistryEditor.CreateKey(RegistryEditor.Hive.LOCALMACHINE, tmpExtensionsKeyName)
            'End If

            'For Each tmpExtensionValue As String In RegistryEditor.GetRegValueNames(RegistryEditor.Hive.LOCALMACHINE, tmpExtensionsKeyName)
            '    tmpExtensionList.Add(tmpExtensionValue & "|" & RegistryEditor.GetRegValue(RegistryEditor.Hive.LOCALMACHINE, tmpExtensionsKeyName, tmpExtensionValue.ToString))
            'Next

        End Get
    End Property

    Public Shared ReadOnly Property GetProcesses() As String
        Get
            Dim tmpExtensionsKeyName As String = CON_PATH_DISPLAYSERVICES + "/Processes"
            Dim tmpProcess As String
            tmpProcess = GetSetting(tmpExtensionsKeyName, "xml")

            If String.IsNullOrEmpty(tmpProcess) Then
                tmpProcess = "<Processes>"
                Dim regKey As RegistryKey
                regKey = Registry.LocalMachine.CreateSubKey("Software\mermaid technology\vTouch Pro\vTouch Pro DisplayServices\Processes")
                Dim tmpPath As String
                For Each tmpExtensionValue As String In regKey.GetValueNames
                    tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAYSERVICES & "\Processes", tmpExtensionValue.ToString(), "").ToString()
                    SetSetting(CON_PATH_DISPLAYSERVICES & "/Processes/" & tmpExtensionValue.ToString(), tmpPath)
                Next
                tmpProcess += GetSetting(tmpExtensionsKeyName, "xml")
            Else
                tmpProcess = "<Processes>" + tmpProcess
            End If
            tmpProcess += "</Processes>"
            Return tmpProcess
        End Get
    End Property

    Public Shared ReadOnly Property MessageServerAddress() As String
        Get
            'Dim tmpPath As String

            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "MessageServerAddress") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "MessageServerAddress", Me.ServerAddress)
            '    tmpPath = "MYSERVER"
            'Else
            '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "MessageServerAddress"), GetType(String))
            '    tmpPath = tmpPath.Replace(":8001", "")
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "MessageServerAddress", tmpPath)
            'End If

            'If tmpPath.IndexOf(":") <> -1 Then
            '    tmpPath = tmpPath
            'ElseIf Not String.IsNullOrEmpty(tmpPath) Then
            '    tmpPath = tmpPath & ":8001"
            'End If
            'Return tmpPath
            Dim tmpPath As String
            Try
                tmpPath = GetSetting(CON_PATH_DISPLAYSERVICES & "/MessageServerAddress", ServerAddress) 'Me.ConfigManager.ReadSetting(REG_PATH_DISPLAYSERVICES & "/MessageServerAddress", Me.ServerAddress)
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = ServerAddress()
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "MessageServerAddress") Then
                    '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "MessageServerAddress").ToString(), GetType(String))
                    '    tmpPath = tmpPath.Replace(":8001", "")
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAYSERVICES, "MessageServerAddress", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = ServerAddress()
                        End If
                    Catch ex As Exception
                        tmpPath = ServerAddress()
                    End Try
                    tmpPath = tmpPath.Replace(":8001", "")

                    SetSetting(CON_PATH_DISPLAYSERVICES & "/MessageServerAddress", tmpPath)
                    'SetSetting(REG_PATH_DISPLAYSERVICES & "/MessageServerAddress", Me.ServerAddress)
                End If

                If tmpPath.IndexOf(":") <> -1 Then
                    tmpPath = tmpPath

                ElseIf Not String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = tmpPath & ":8001"

                End If

                Return tmpPath
            Catch ex As Exception
                tmpPath = ServerAddress() & ":8001"
                Return tmpPath
            End Try

        End Get
    End Property

    Public Shared ReadOnly Property DFSServerProxyAddress() As String
        Get
            Dim tmpPath As String = ServerAddress

            If tmpPath.IndexOf(":") <> -1 Then
                Dim tmpParts() As String = tmpPath.Split(New Char() {":"}, StringSplitOptions.RemoveEmptyEntries)

                tmpPath = "gtcp://" & tmpParts(0) & ":" & CInt(tmpParts(1)) - 1 & "/DownloadClient.rem"

            ElseIf Not String.IsNullOrEmpty(tmpPath) Then
                tmpPath = "gtcp://" & tmpPath & ":8000/DownloadClient.rem"

            End If

            Return tmpPath

        End Get
    End Property

    Public Shared ReadOnly Property DFSStatusCheckInterval() As Integer
        Get
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAYSERVICES & "/DFSStatusCheckInterval", "") 'Me.ConfigManager.ReadSetting(REG_PATH_DISPLAYSERVICES & "/DFSStatusCheckInterval", "60")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "60"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "DFSStatusCheckInterval") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "DFSStatusCheckInterval").ToString()
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAYSERVICES, "DFSStatusCheckInterval", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "60"
                        End If
                    Catch ex As Exception
                        tmpPath = "60"
                    End Try

                    SetSetting(CON_PATH_DISPLAYSERVICES & "/DFSStatusCheckInterval", tmpPath)
                End If

                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 60
            End Try

        End Get
    End Property

    Public Shared ReadOnly Property DFSStatusCheckTolerance() As Integer
        Get
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAYSERVICES & "/DFSStatusCheckTolerance", "") 'Me.ConfigManager.ReadSetting(REG_PATH_DISPLAYSERVICES & "/DFSStatusCheckTolerance", "15")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "15"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "DFSStatusCheckTolerance") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "DFSStatusCheckTolerance").ToString()
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAYSERVICES, "DFSStatusCheckTolerance", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "15"
                        End If
                    Catch ex As Exception
                        tmpPath = "15"
                    End Try

                    SetSetting(CON_PATH_DISPLAYSERVICES & "/DFSStatusCheckTolerance", tmpPath) 'SetSetting(REG_PATH_DISPLAYSERVICES & "/DFSStatusCheckTolerance", "15")
                End If
                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 15
            End Try

        End Get
    End Property


    Public Shared Property DisplayStartupDelay() As Integer
        Get
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAYSERVICES & "/DisplayStartupDelay", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "30"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "DisplayStartupDelay") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "DisplayStartupDelay").ToString()
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAYSERVICES, "DisplayStartupDelay", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "30"
                        End If
                    Catch ex As Exception
                        tmpPath = "30"
                    End Try

                    SetSetting(CON_PATH_DISPLAYSERVICES & "/DisplayStartupDelay", tmpPath)
                End If

                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 30
            End Try

        End Get
        Set(ByVal value As Integer)
            SetSetting(CON_PATH_DISPLAYSERVICES & "/DisplayStartupDelay", value.ToString())
        End Set
    End Property
    Public Shared Property UpdateCounter() As Integer
        Get
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAYSERVICES & "/UpdateCounter", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "0"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "DisplayStartupDelay") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "DisplayStartupDelay").ToString()
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAYSERVICES, "UpdateCounter", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "0"
                        End If
                    Catch ex As Exception
                        tmpPath = "0"
                    End Try

                    SetSetting(CON_PATH_DISPLAYSERVICES & "/UpdateCounter", tmpPath)
                End If

                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            SetSetting(CON_PATH_DISPLAYSERVICES & "/UpdateCounter", value.ToString())
        End Set
    End Property
    Public Shared Property UpdateInterval() As Integer
        Get
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAYSERVICES & "/UpdateInterval", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "600000"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "DisplayStartupDelay") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "DisplayStartupDelay").ToString()
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAYSERVICES, "UpdateInterval", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "600000"
                        End If
                    Catch ex As Exception
                        tmpPath = "600000"
                    End Try

                    SetSetting(CON_PATH_DISPLAYSERVICES & "/UpdateInterval", tmpPath)
                End If

                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 600000
            End Try

        End Get
        Set(ByVal value As Integer)
            SetSetting(CON_PATH_DISPLAYSERVICES & "/UpdateInterval", value.ToString())
        End Set
    End Property
    Public Shared Property UpdateVersion() As String
        Get
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAYSERVICES & "/UpdateVersion", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "0"
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAYSERVICES, "UpdateVersion", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "0"
                        End If
                    Catch ex As Exception
                        tmpPath = "0"
                    End Try

                End If
                SetSetting(CON_PATH_DISPLAYSERVICES & "/UpdateVersion", tmpPath)
                Return tmpPath
            Catch ex As Exception
                Return 0
            End Try

        End Get
        Set(ByVal value As String)
            SetSetting(CON_PATH_DISPLAYSERVICES & "/UpdateVersion", value)
        End Set
    End Property

    Public Shared Property BackupExpireTime() As Integer
        Get
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAYSERVICES & "/BackupExpireTime", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "7"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "BackupExpireTime") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "BackupExpireTime").ToString()
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAYSERVICES, "BackupExpireTime", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "7"
                        End If
                    Catch ex As Exception
                        tmpPath = "7"
                    End Try
                    SetSetting(CON_PATH_DISPLAYSERVICES & "/BackupExpireTime", tmpPath)
                End If

                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 7
            End Try

        End Get
        Set(ByVal value As Integer)
            SetSetting(CON_PATH_DISPLAYSERVICES & "/BackupExpireTime", value.ToString())
        End Set
    End Property


    Public Shared Property StartDisplay() As Boolean
        Get
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAYSERVICES & "/StartDisplay", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "1"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "StartDisplay") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "StartDisplay").ToString()
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAYSERVICES, "StartDisplay", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "1"
                        End If
                    Catch ex As Exception
                        tmpPath = "1"
                    End Try

                    SetSetting(CON_PATH_DISPLAYSERVICES & "/StartDisplay", tmpPath)
                End If

                If tmpPath = "1" Then
                    Return True
                Else
                    Return False
                End If
                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return True
            End Try
        End Get
        Set(ByVal value As Boolean)
            If value Then
                SetSetting(CON_PATH_DISPLAYSERVICES & "/StartDisplay", "1")
            Else
                SetSetting(CON_PATH_DISPLAYSERVICES & "/StartDisplay", "0")
            End If

        End Set
    End Property

    'Public Shared Property Display0LastHeartbeat() As String
    '    Get
    '        'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "LastUpdate") Then
    '        '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastUpdate", Date.MinValue.ToString)

    '        'End If

    '        'Dim dateString As String = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastUpdate")
    '        'Try
    '        '    Return Date.Parse(dateString)

    '        'Catch ex As Exception
    '        '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastUpdate", Date.MinValue.ToString())
    '        '    Return Date.MinValue

    '        'End Try

    '        Dim tmpPath As String
    '        tmpPath = GetSetting(REG_PATH_DISPLAY & "/DISPLAY0/LastHeartBeat", "")
    '        If String.IsNullOrEmpty(tmpPath) Then
    '            SetSetting(REG_PATH_DISPLAY & "/DISPLAY0/LastHeartBeat", "")
    '        End If

    '        'Return Integer.Parse(tmpPath)
    '        Return tmpPath

    '    End Get
    '    Set(ByVal value As String)
    '        SetSetting(REG_PATH_DISPLAY & "/DISPLAY0/LastHeartBeat", value)
    '        'RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastUpdate", Date.MinValue.ToString())
    '    End Set
    'End Property

    'Public Shared Property Display1LastHeartbeat() As String
    '    Get
    '        Dim tmpPath As String
    '        tmpPath = GetSetting(REG_PATH_DISPLAY & "/DISPLAY1/LastHeartBeat", "")
    '        If String.IsNullOrEmpty(tmpPath) Then
    '            SetSetting(REG_PATH_DISPLAY & "/DISPLAY1/LastHeartBeat", "")
    '        End If

    '        Return Integer.Parse(tmpPath)

    '    End Get
    '    Set(ByVal value As String)
    '        SetSetting(REG_PATH_DISPLAY & "/DISPLAY1/LastHeartBeat", value)
    '    End Set
    'End Property

    Public Shared Property CleanShutDownPerformed() As Boolean
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "CleanShutDownPerformed") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "CleanShutDownPerformed", 0)

            'End If

            'Return RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "CleanShutDownPerformed")
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAYSERVICES & "/CleanShutDownPerformed", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "False"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "CleanShutDownPerformed") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "CleanShutDownPerformed").ToString()
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAYSERVICES, "CleanShutDownPerformed", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "False"
                        End If
                    Catch ex As Exception
                        tmpPath = "False"
                    End Try

                    SetSetting(CON_PATH_DISPLAYSERVICES & "/CleanShutDownPerformed", tmpPath)
                End If
                Return tmpPath
            Catch ex As Exception
                Return False
            End Try

        End Get
        Set(ByVal value As Boolean)
            'If value Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "CleanShutDownPerformed", 1)

            'Else
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "CleanShutDownPerformed", 0)

            'End If
            SetSetting(CON_PATH_DISPLAYSERVICES & "/CleanShutDownPerformed", value.ToString())
        End Set
    End Property

    Public Shared Property LastMajorUpdate() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "LastMajorUpdate") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastMajorUpdate", 0)

            'End If

            'Return RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastMajorUpdate")
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAYSERVICES & "/LastMajorUpdate", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "0"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "LastMajorUpdate") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastMajorUpdate").ToString()
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAYSERVICES, "LastMajorUpdate", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "0"
                        End If
                    Catch ex As Exception
                        tmpPath = "0"
                    End Try

                    SetSetting(CON_PATH_DISPLAYSERVICES & "/LastMajorUpdate", tmpPath)
                End If

                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 0
            End Try

        End Get
        Set(ByVal Value As Integer)
            'RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastMajorUpdate", Value)
            SetSetting(CON_PATH_DISPLAYSERVICES & "/LastMajorUpdate", Value.ToString())

        End Set
    End Property

    Public Shared Property LastMinorUpdate() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "LastMinorUpdate") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastMinorUpdate", 0)

            'End If

            'Return RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastMinorUpdate")
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAYSERVICES & "/LastMinorUpdate", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "0"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "LastMinorUpdate") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastMinorUpdate").ToString()
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAYSERVICES, "LastMinorUpdate", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "0"
                        End If
                    Catch ex As Exception
                        tmpPath = "0"
                    End Try

                    SetSetting(CON_PATH_DISPLAYSERVICES & "/LastMinorUpdate", tmpPath)
                End If

                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 0
            End Try

        End Get
        Set(ByVal Value As Integer)
            'RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastMinorUpdate", Value)
            SetSetting(CON_PATH_DISPLAYSERVICES & "/LastMinorUpdate", Value.ToString())

        End Set
    End Property

    Public Shared Property LastBuildUpdate() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "LastBuildUpdate") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastBuildUpdate", 0)

            'End If

            'Return RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastBuildUpdate")
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAYSERVICES & "/LastBuildUpdate", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "0"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "LastBuildUpdate") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastBuildUpdate").ToString()
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAYSERVICES, "LastBuildUpdate", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "0"
                        End If
                    Catch ex As Exception
                        tmpPath = "0"
                    End Try

                    SetSetting(CON_PATH_DISPLAYSERVICES & "/LastBuildUpdate", tmpPath)
                End If

                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 0
            End Try

        End Get
        Set(ByVal Value As Integer)
            'RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastBuildUpdate", Value)
            SetSetting(CON_PATH_DISPLAYSERVICES & "/LastBuildUpdate", Value.ToString())
        End Set
    End Property

    Public Shared Property LastUpdate() As Date
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "LastUpdate") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastUpdate", Date.MinValue.ToString)

            'End If

            'Dim dateString As String = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastUpdate")
            'Try
            '    Return Date.Parse(dateString)

            'Catch ex As Exception
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastUpdate", Date.MinValue.ToString())
            '    Return Date.MinValue

            'End Try
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAYSERVICES & "/LastUpdate", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = Date.MinValue.ToString("yyyy-MM-dd'T'HH:mm:ss")
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "LastUpdate") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastUpdate").ToString("yyyy-MM-dd'T'HH:mm:ss")
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAYSERVICES, "LastUpdate", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = Date.MinValue.ToString("yyyy-MM-dd'T'HH:mm:ss")
                        End If
                    Catch ex As Exception
                        tmpPath = Date.MinValue.ToString("yyyy-MM-dd'T'HH:mm:ss")
                    End Try

                    SetSetting(CON_PATH_DISPLAYSERVICES & "/LastUpdate", tmpPath.ToString())
                End If

                Try
                    Return DateTime.ParseExact(tmpPath, "yyyy-MM-dd'T'HH:mm:ss", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None)
                    'Return Convert.ToDateTime(tmpPath)

                Catch ex As Exception
                    SetSetting(CON_PATH_DISPLAYSERVICES & "/LastUpdate", Date.MinValue.ToString("yyyy-MM-dd'T'HH:mm:ss"))
                    Return Date.Parse(Date.MinValue.ToString("yyyy-MM-dd'T'HH:mm:ss"))

                End Try
            Catch ex As Exception
                SetSetting(CON_PATH_DISPLAYSERVICES & "/LastUpdate", Date.MinValue.ToString("yyyy-MM-dd'T'HH:mm:ss"))
                Return Date.Parse(Date.MinValue.ToString("yyyy-MM-dd'T'HH:mm:ss"))

            End Try

        End Get
        Set(ByVal Value As Date)
            'RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastUpdate", Value.ToString)
            SetSetting(CON_PATH_DISPLAYSERVICES & "/LastUpdate", Value.ToString("yyyy-MM-dd'T'HH:mm:ss"))

        End Set
    End Property

    Public Shared ReadOnly Property DisplayServicesPort() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "ServerPort") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "ServerPort", 8001)

            'End If

            'Return EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "ServerPort"), GetType(Integer))
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAYSERVICES & "/ServerPort", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "8001"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "ServerPort") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "ServerPort").ToString()
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAYSERVICES, "ServerPort", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "8001"
                        End If
                    Catch ex As Exception
                        tmpPath = "8001"
                    End Try

                    SetSetting(CON_PATH_DISPLAYSERVICES & "/ServerPort", "8001")
                End If

                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return "8001"
            End Try

        End Get
    End Property

    Public Shared Property TimeServer() As String
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_COMMON_SETTINGS, "TimeServer") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "TimeServer", "")

            'End If
            'Return EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "TimeServer"), GetType(String))

            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_COMMON_SETTINGS & "/TimeServer", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = ""
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "TimeServer") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "TimeServer").ToString()
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAYSERVICES, "TimeServer", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = ""
                        End If
                    Catch ex As Exception
                        tmpPath = ""
                    End Try

                    SetSetting(CON_PATH_COMMON_SETTINGS & "/TimeServer", tmpPath)
                End If

                Return tmpPath
            Catch ex As Exception
                Return ""
            End Try

        End Get
        Set(ByVal Value As String)
            'RegistryEditor.SetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "TimeServer", Value)
            SetSetting(CON_PATH_COMMON_SETTINGS & "/TimeServer", Value)

        End Set
    End Property

    Public Shared Property TimeSyncronizeInterval() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_COMMON_SETTINGS, "TimeSyncronizeInterval") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "TimeSyncronizeInterval", 1440)

            'End If

            'Return EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "TimeSyncronizeInterval"), GetType(Integer))
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_COMMON_SETTINGS & "/TimeSyncronizeInterval", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "1440"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "TimeSyncronizeInterval") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "TimeSyncronizeInterval").ToString()
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAYSERVICES, "TimeSyncronizeInterval", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "1440"
                        End If
                    Catch ex As Exception
                        tmpPath = "1440"
                    End Try

                    SetSetting(CON_PATH_COMMON_SETTINGS & "/TimeSyncronizeInterval", tmpPath)
                End If

                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 1440
            End Try
        End Get
        Set(ByVal Value As Integer)
            'RegistryEditor.SetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "TimeSyncronizeInterval", Value)
            SetSetting(CON_PATH_COMMON_SETTINGS & "/TimeSyncronizeInterval", Value.ToString())

        End Set
    End Property

    Public Shared Property LogLevel() As Integer
        Get
            'Return 0 ' need to be fixed for thread access
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAYSERVICES & "/LogLevel", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "0"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "LogLevel") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LogLevel").ToString()
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAYSERVICES, "LogLevel", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "0"
                        End If
                    Catch ex As Exception
                        tmpPath = "0"
                    End Try

                    SetSetting(CON_PATH_DISPLAYSERVICES & "/LogLevel", tmpPath)
                End If
                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal Value As Integer)
            'RegistryEditor.SetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "TimeServer", Value)
            SetSetting(CON_PATH_DISPLAYSERVICES & "/LogLevel", Value.ToString())
        End Set
    End Property
    Public Shared ReadOnly Property ConfigLogLevel() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "LogLevel") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LogLevel", 0)
            'End If

            'Return EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LogLevel"), GetType(Integer))
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAYSERVICES & "/LogLevel", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "0"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "LogLevel") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LogLevel").ToString()
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAYSERVICES, "LogLevel", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "0"
                        End If
                    Catch ex As Exception
                        tmpPath = "0"
                    End Try

                    SetSetting(CON_PATH_DISPLAYSERVICES & "/LogLevel", tmpPath)

                End If
                Return Integer.Parse(tmpPath)

            Catch ex As Exception
                Return 0
            End Try

        End Get
    End Property

    Public Shared Property RestartRequired() As Boolean
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "RestartRequired") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "RestartRequired", 0)

            'End If

            'Return EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "RestartRequired"), GetType(Boolean))
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAYSERVICES & "/RestartRequired", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "0"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "RestartRequired") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "RestartRequired").ToString()
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAYSERVICES, "RestartRequired", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "0"
                        End If
                    Catch ex As Exception
                        tmpPath = "0"
                    End Try

                    SetSetting(CON_PATH_DISPLAYSERVICES & "/RestartRequired", tmpPath)
                End If

                If String.Compare(tmpPath, "1") = 0 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Return False
            End Try

        End Get
        Set(ByVal Value As Boolean)
            If Value Then
                'RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "RestartRequired", 1)
                SetSetting(CON_PATH_DISPLAYSERVICES & "/RestartRequired", "1")
            Else
                'RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "RestartRequired", 0)
                SetSetting(CON_PATH_DISPLAYSERVICES & "/RestartRequired", "0")
            End If

        End Set
    End Property

    Public Shared Property DFSEnabled() As Boolean
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "DFSEnabled") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "DFSEnabled", 1)

            'End If

            'Return RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "DFSEnabled")
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAYSERVICES & "/DFSEnabled", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "1"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "DFSEnabled") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "DFSEnabled").ToString()
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAYSERVICES, "DFSEnabled", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "1"
                        End If
                    Catch ex As Exception
                        tmpPath = "1"
                    End Try

                    SetSetting(CON_PATH_DISPLAYSERVICES & "/DFSEnabled", tmpPath)
                End If

                If String.Compare(tmpPath, "1") = 0 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Return True
            End Try

        End Get
        Set(ByVal value As Boolean)
            If value Then
                'RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "DFSEnabled", 1)
                SetSetting(CON_PATH_DISPLAYSERVICES & "/DFSEnabled", "1")
            Else
                'RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "DFSEnabled", 0)
                SetSetting(CON_PATH_DISPLAYSERVICES & "/DFSEnabled", "0")
            End If
        End Set
    End Property

    Public Shared ReadOnly Property ResourceDiskSpaceWarning() As Long
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "ResourceDiskSpaceWarning") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "ResourceDiskSpaceWarning", 204800) '200MB

            'End If

            'Return Me.EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "ResourceDiskSpaceWarning"), GetType(Integer))
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAYSERVICES & "/ResourceDiskSpaceWarning", "") '200MB
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "204800"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "ResourceDiskSpaceWarning") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "ResourceDiskSpaceWarning").ToString()
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAYSERVICES, "ResourceDiskSpaceWarning", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "204800"
                        End If
                    Catch ex As Exception
                        tmpPath = "204800"
                    End Try

                    SetSetting(CON_PATH_DISPLAYSERVICES & "/ResourceDiskSpaceWarning", tmpPath)
                End If

                Return Long.Parse(tmpPath)
            Catch ex As Exception
                Return 204800
            End Try

        End Get
    End Property

    Public Shared ReadOnly Property SocketCompressionEnabled() As Boolean
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "SocketCompressionEnabled") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "SocketCompressionEnabled", 1)

            'End If

            'Return Me.EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "SocketCompressionEnabled"), GetType(Boolean))
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAYSERVICES & "/SocketCompressionEnabled", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "1"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "SocketCompressionEnabled") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "SocketCompressionEnabled").ToString()
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAYSERVICES, "SocketCompressionEnabled", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "1"
                        End If
                    Catch ex As Exception
                        tmpPath = "1"
                    End Try

                    SetSetting(CON_PATH_DISPLAYSERVICES & "/SocketCompressionEnabled", tmpPath)
                End If

                If String.Compare(tmpPath, "1") = 0 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Return True
            End Try

        End Get
    End Property



#Region "Constants"

    Private Const CON_PATH_DISPLAYSERVICES As String = "vTouchPro/vTouchProDisplayServices"
#End Region

End Class

Public Class DFSSettings
    Inherits MachineSettings

    Public Sub New()
    End Sub

#Region "Properties"

    Public Shared Sub SetValue(ByVal path As String, ByVal key As String, ByVal value As String)
        Dim tmpArray As New ArrayList
        Dim tmpExtensionsKeyName As String = path & "/" & key
        SetSetting(tmpExtensionsKeyName, value)
        'ConfigManager.WriteSetting(tmpExtensionsKeyName, value)
    End Sub

    Public Shared ReadOnly Property GetValue(ByVal path As String, ByVal key As String) As String
        Get
            'Dim tmpExtensionsKeyName As String = path & "/" & key
            'Dim xmlExtensions As String = ConfigManager.ReadSetting(tmpExtensionsKeyName, "")
            'Return xmlExtensions
            path = path & "/" & key
            Return GetSetting(path, "")
        End Get
    End Property

    Public Shared ReadOnly Property GetXmlStructure(ByVal path As String) As String
        Get
            'test
            Return GetSetting(path, "xml")
            'end test
            'Dim xmlCommands As String = Me.ConfigManager.GetXMLNodes(path, "")
            'Return xmlCommands

        End Get
    End Property

    Public Shared Property DFSchunksize() As Long
        Get
            'Return 0 ' need to be fixed for thread access
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DFS & "/DFSchunksize", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "32768"
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DFS, "DFSchunksize", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "32768"
                        End If
                    Catch ex As Exception
                        tmpPath = "32768"
                    End Try
                    SetSetting(CON_PATH_DFS & "/DFSchunksize", tmpPath)
                Else
                    Microsoft.Win32.Registry.SetValue(REG_PATH_DFS, "DFSchunksize", tmpPath, Microsoft.Win32.RegistryValueKind.DWord)
                End If
                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal Value As Long)
            Microsoft.Win32.Registry.SetValue(REG_PATH_DFS, "DFSchunksize", Value, Microsoft.Win32.RegistryValueKind.DWord)
            SetSetting(CON_PATH_DFS & "/DFSchunksize", Value.ToString())
        End Set
    End Property

    Public Shared Property DFSlogs() As Integer
        Get
            'Return 0 ' need to be fixed for thread access
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DFS & "/DFSlogs", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "0"
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DFS, "DFSlogs", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "0"
                        End If
                    Catch ex As Exception
                        tmpPath = "0"
                    End Try
                    SetSetting(CON_PATH_DFS & "/DFSlogs", tmpPath)
                Else
                    Microsoft.Win32.Registry.SetValue(REG_PATH_DFS, "DFSlogs", tmpPath, Microsoft.Win32.RegistryValueKind.DWord)
                End If
                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal Value As Integer)
            Microsoft.Win32.Registry.SetValue(REG_PATH_DFS, "DFSlogs", Value, Microsoft.Win32.RegistryValueKind.DWord)
            SetSetting(CON_PATH_DFS & "/DFSlogs", Value.ToString())
        End Set
    End Property


    Public Shared Property DFSlogsize() As Long
        Get
            'Return 0 ' need to be fixed for thread access
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DFS & "/DFSlogsize", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "1048576"
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DFS, "DFSlogsize", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "1048576"
                        End If
                    Catch ex As Exception
                        tmpPath = "1048576"
                    End Try
                    SetSetting(CON_PATH_DFS & "/DFSlogsize", tmpPath)
                Else
                    Microsoft.Win32.Registry.SetValue(REG_PATH_DFS, "DFSlogsize", tmpPath, Microsoft.Win32.RegistryValueKind.DWord)
                End If
                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal Value As Long)
            Microsoft.Win32.Registry.SetValue(REG_PATH_DFS, "DFSlogsize", Value, Microsoft.Win32.RegistryValueKind.DWord)
            SetSetting(CON_PATH_DFS & "/DFSlogsize", Value.ToString())
        End Set
    End Property
    Public Shared Property DFSEnableRestore() As Integer
        Get
            'Return 0 ' need to be fixed for thread access
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DFS & "/EnableRestore", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "1"
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DFS, "EnableRestore", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "1"
                        End If
                    Catch ex As Exception
                        tmpPath = "1"
                    End Try
                    SetSetting(CON_PATH_DFS & "/EnableRestore", tmpPath)
                Else
                    Microsoft.Win32.Registry.SetValue(REG_PATH_DFS, "EnableRestore", tmpPath, Microsoft.Win32.RegistryValueKind.DWord)
                End If
                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal Value As Integer)
            Microsoft.Win32.Registry.SetValue(REG_PATH_DFS, "EnableRestore", Value, Microsoft.Win32.RegistryValueKind.DWord)
            SetSetting(CON_PATH_DFS & "/EnableRestore", Value.ToString())
        End Set
    End Property
    Public Shared Property DFSTimeout() As Long
        Get
            'Return 0 ' need to be fixed for thread access
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DFS & "/dfstimeout", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "60000"
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DFS, "dfstimeout", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "60000"
                        End If
                    Catch ex As Exception
                        tmpPath = "60000"
                    End Try
                    SetSetting(CON_PATH_DFS & "/dfstimeout", tmpPath)
                Else
                    Microsoft.Win32.Registry.SetValue(REG_PATH_DFS, "dfstimeout", tmpPath, Microsoft.Win32.RegistryValueKind.DWord)
                End If
                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal Value As Long)
            Microsoft.Win32.Registry.SetValue(REG_PATH_DFS, "dfstimeout", Value, Microsoft.Win32.RegistryValueKind.DWord)
            SetSetting(CON_PATH_DFS & "/dfstimeout", Value.ToString())
        End Set
    End Property
    Public Shared Property DFSEnableCleanup() As Integer
        Get
            'Return 0 ' need to be fixed for thread access
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DFS & "/EnableCleanup", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "0"
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DFS, "EnableCleanup", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "0"
                        End If
                    Catch ex As Exception
                        tmpPath = "0"
                    End Try
                    SetSetting(CON_PATH_DFS & "/EnableCleanup", tmpPath)
                Else
                    Microsoft.Win32.Registry.SetValue(REG_PATH_DFS, "EnableCleanup", tmpPath, Microsoft.Win32.RegistryValueKind.DWord)
                End If
                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal Value As Integer)
            Microsoft.Win32.Registry.SetValue(REG_PATH_DFS, "EnableCleanup", Value, Microsoft.Win32.RegistryValueKind.DWord)
            SetSetting(CON_PATH_DFS & "/EnableCleanup", Value.ToString())
        End Set
    End Property

#End Region

#Region "Constants"

    Private Const CON_PATH_DFS As String = "vTouchPro/vTouchProDFS"
#End Region

End Class







Public Class IPPlayerSettings
    Inherits MachineSettings

#Region "Properties"

    Public Shared ReadOnly Property MessageServerAddress() As String
        Get
            'Dim tmpPath As String

            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "MessageServerAddress") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "MessageServerAddress", Me.ServerAddress)
            '    tmpPath = "MYSERVER"

            'Else
            '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "MessageServerAddress"), GetType(String))

            '    tmpPath = tmpPath.Replace(":8001", "")

            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "MessageServerAddress", tmpPath)

            'End If

            'If tmpPath.IndexOf(":") <> -1 Then
            '    tmpPath = tmpPath

            'ElseIf Not String.IsNullOrEmpty(tmpPath) Then
            '    tmpPath = tmpPath & ":8001"

            'End If

            'Return tmpPath

            Dim tmpPath As String
            tmpPath = GetSetting(CON_PATH_IPPLAYER & "/MessageServerAddress", "")
            If String.IsNullOrEmpty(tmpPath) Then
                SetSetting(CON_PATH_IPPLAYER & "/MessageServerAddress", ServerAddress)
                tmpPath = ServerAddress
            End If

            If tmpPath.IndexOf(":") <> -1 Then
                tmpPath = tmpPath

            ElseIf Not String.IsNullOrEmpty(tmpPath) Then
                tmpPath = tmpPath & ":8001"

            End If

            Return tmpPath

        End Get
    End Property

    Public Shared ReadOnly Property DFSServerProxyAddress() As String
        Get
            Dim tmpPath As String = ServerAddress

            If tmpPath.IndexOf(":") <> -1 Then
                Dim tmpParts() As String = tmpPath.Split(New Char() {":"}, StringSplitOptions.RemoveEmptyEntries)

                tmpPath = "gtcp://" & tmpParts(0) & ":" & CInt(tmpParts(1)) - 1 & "/DownloadClient.rem"

            ElseIf Not String.IsNullOrEmpty(tmpPath) Then
                tmpPath = "gtcp://" & tmpPath & ":8000/DownloadClient.rem"

            End If

            Return tmpPath

        End Get
    End Property

    Public Shared Property CleanShutDownPerformed() As Boolean
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "CleanShutDownPerformed") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "CleanShutDownPerformed", 0)

            'End If

            'Return RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "CleanShutDownPerformed")

            Dim tmpPath As String
            tmpPath = GetSetting(CON_PATH_IPPLAYER & "/CleanShutDownPerformed", "")
            If String.IsNullOrEmpty(tmpPath) Then
                SetSetting(CON_PATH_IPPLAYER & "/CleanShutDownPerformed", "False")
                tmpPath = "False"
            End If

            Return tmpPath

        End Get
        Set(ByVal value As Boolean)
            'If value Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "CleanShutDownPerformed", 1)

            'Else
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "CleanShutDownPerformed", 0)

            'End If
            SetSetting(CON_PATH_IPPLAYER & "/CleanShutDownPerformed", value.ToString())
        End Set
    End Property

    Public Shared Property LastMajorUpdate() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "LastMajorUpdate") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastMajorUpdate", 0)

            'End If

            'Return RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastMajorUpdate")

            Dim tmpPath As String
            tmpPath = GetSetting(CON_PATH_IPPLAYER & "/LastMajorUpdate", "")
            If String.IsNullOrEmpty(tmpPath) Then
                SetSetting(CON_PATH_IPPLAYER & "/LastMajorUpdate", "0")
                tmpPath = "0"
            End If

            Return Integer.Parse(tmpPath)

        End Get
        Set(ByVal Value As Integer)
            'RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastMajorUpdate", Value)
            SetSetting(CON_PATH_IPPLAYER & "/LastMajorUpdate", Value.ToString())

        End Set
    End Property

    Public Shared Property LastMinorUpdate() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "LastMinorUpdate") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastMinorUpdate", 0)

            'End If

            'Return RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastMinorUpdate")

            Dim tmpPath As String
            tmpPath = GetSetting(CON_PATH_IPPLAYER & "/LastMinorUpdate", "")
            If String.IsNullOrEmpty(tmpPath) Then
                SetSetting(CON_PATH_IPPLAYER & "/LastMinorUpdate", "0")
                tmpPath = "0"
            End If

            Return Integer.Parse(tmpPath)

        End Get
        Set(ByVal Value As Integer)
            'RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastMinorUpdate", Value)
            SetSetting(CON_PATH_IPPLAYER & "/LastMinorUpdate", Value.ToString())

        End Set
    End Property

    Public Shared Property LastBuildUpdate() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "LastBuildUpdate") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastBuildUpdate", 0)

            'End If

            'Return RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastBuildUpdate")

            Dim tmpPath As String
            tmpPath = GetSetting(CON_PATH_IPPLAYER & "/LastBuildUpdate", "")
            If String.IsNullOrEmpty(tmpPath) Then
                SetSetting(CON_PATH_IPPLAYER & "/LastBuildUpdate", "0")
                tmpPath = "0"
            End If

            Return Integer.Parse(tmpPath)

        End Get
        Set(ByVal Value As Integer)
            'RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastBuildUpdate", Value)
            SetSetting(CON_PATH_IPPLAYER & "/LastBuildUpdate", Value.ToString())
        End Set
    End Property

    Public Shared Property LastUpdate() As Date
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "LastUpdate") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastUpdate", Date.MinValue.ToString)

            'End If

            'Dim dateString As String = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastUpdate")
            'Try
            '    Return Date.Parse(dateString)

            'Catch ex As Exception
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastUpdate", Date.MinValue.ToString())
            '    Return Date.MinValue

            'End Try

            Dim tmpPath As String
            tmpPath = GetSetting(CON_PATH_IPPLAYER & "/LastUpdate", "")
            If String.IsNullOrEmpty(tmpPath) Then
                SetSetting(CON_PATH_IPPLAYER & "/LastUpdate", Date.MinValue.ToString("yyyy-MM-dd'T'HH:mm:ss"))
                tmpPath = Date.MinValue.ToString("yyyy-MM-dd'T'HH:mm:ss")
            End If

            Try
                Return Date.Parse(tmpPath)

            Catch ex As Exception
                SetSetting(CON_PATH_IPPLAYER & "/LastUpdate", Date.MinValue.ToString("yyyy-MM-dd'T'HH:mm:ss"))
                Return Date.Parse(Date.MinValue.ToString("yyyy-MM-dd'T'HH:mm:ss"))

            End Try
        End Get
        Set(ByVal Value As Date)
            'RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LastUpdate", Value.ToString)
            SetSetting(CON_PATH_IPPLAYER & "/LastUpdate", Value.ToString("yyyy-MM-dd'T'HH:mm:ss"))

        End Set
    End Property

    Public Shared ReadOnly Property DisplayServicesPort() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "ServerPort") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "ServerPort", 8001)

            'End If

            'Return EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "ServerPort"), GetType(Integer))
            Dim tmpPath As String
            tmpPath = GetSetting(CON_PATH_IPPLAYER & "/ServerPort", "")
            If String.IsNullOrEmpty(tmpPath) Then
                SetSetting(CON_PATH_IPPLAYER & "/ServerPort", "8001")
                tmpPath = "8001"
            End If

            Return Integer.Parse(tmpPath)
        End Get
    End Property

    Public Shared Property TimeServer() As String
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_COMMON_SETTINGS, "TimeServer") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "TimeServer", "")

            'End If

            'Return EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "TimeServer"), GetType(String))

            Dim tmpPath As String
            tmpPath = GetSetting(CON_PATH_COMMON_SETTINGS & "/TimeServer", "")
            If String.IsNullOrEmpty(tmpPath) Then
                SetSetting(CON_PATH_COMMON_SETTINGS & "/TimeServer", "")
            End If

            Return tmpPath

        End Get
        Set(ByVal Value As String)
            'RegistryEditor.SetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "TimeServer", Value)
            SetSetting(CON_PATH_COMMON_SETTINGS & "/TimeServer", Value)

        End Set
    End Property

    Public Shared Property TimeSyncronizeInterval() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_COMMON_SETTINGS, "TimeSyncronizeInterval") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "TimeSyncronizeInterval", 1440)

            'End If

            'Return EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "TimeSyncronizeInterval"), GetType(Integer))

            Dim tmpPath As String
            tmpPath = GetSetting(CON_PATH_COMMON_SETTINGS & "/TimeSyncronizeInterval", "")
            If String.IsNullOrEmpty(tmpPath) Then
                SetSetting(CON_PATH_COMMON_SETTINGS & "/TimeSyncronizeInterval", "1440")
                tmpPath = "1440"
            End If

            Return Integer.Parse(tmpPath)

        End Get
        Set(ByVal Value As Integer)
            'RegistryEditor.SetRegValue(HIVE, REG_PATH_COMMON_SETTINGS, "TimeSyncronizeInterval", Value)
            SetSetting(CON_PATH_COMMON_SETTINGS & "/TimeSyncronizeInterval", Value.ToString())

        End Set
    End Property

    Public Shared ReadOnly Property LogLevel() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "LogLevel") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LogLevel", 0)

            'End If

            'Return EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "LogLevel"), GetType(Integer))

            Dim tmpPath As String
            tmpPath = GetSetting(CON_PATH_IPPLAYER & "/LogLevel", "")
            If String.IsNullOrEmpty(tmpPath) Then
                SetSetting(CON_PATH_IPPLAYER & "/LogLevel", "0")
                tmpPath = "0"
            End If

            Return Integer.Parse(tmpPath)

        End Get
    End Property

    Public Shared Property RestartRequired() As Boolean
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "RestartRequired") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "RestartRequired", 0)

            'End If

            'Return EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "RestartRequired"), GetType(Boolean))

            Dim tmpPath As String
            tmpPath = GetSetting(CON_PATH_IPPLAYER & "/RestartRequired", "")
            If String.IsNullOrEmpty(tmpPath) Then
                SetSetting(CON_PATH_IPPLAYER & "/RestartRequired", "0")
                tmpPath = "0"
            End If

            If String.Compare(tmpPath, "1") = 0 Then
                Return True
            Else
                Return False
            End If
        End Get
        Set(ByVal Value As Boolean)
            If Value Then
                'RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "RestartRequired", 1)
                SetSetting(CON_PATH_IPPLAYER & "/RestartRequired", "1")
            Else
                'RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "RestartRequired", 0)
                SetSetting(CON_PATH_IPPLAYER & "/RestartRequired", "0")
            End If

        End Set
    End Property

    Public Shared Property DFSEnabled() As Boolean
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "DFSEnabled") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "DFSEnabled", 1)

            'End If

            'Return RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "DFSEnabled")

            Dim tmpPath As String
            tmpPath = GetSetting(CON_PATH_IPPLAYER & "/DFSEnabled", "")
            If String.IsNullOrEmpty(tmpPath) Then
                SetSetting(CON_PATH_IPPLAYER & "/DFSEnabled", "1")
                tmpPath = "1"
            End If

            If String.Compare(tmpPath, "1") = 0 Then
                Return True
            Else
                Return False
            End If

        End Get
        Set(ByVal value As Boolean)
            If value Then
                'RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "DFSEnabled", 1)
                SetSetting(CON_PATH_IPPLAYER & "/DFSEnabled", "1")
            Else
                'RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "DFSEnabled", 0)
                SetSetting(CON_PATH_IPPLAYER & "/DFSEnabled", "0")
            End If
        End Set
    End Property

    Public Shared ReadOnly Property ResourceDiskSpaceWarning() As Long
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "ResourceDiskSpaceWarning") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "ResourceDiskSpaceWarning", 204800) '200MB

            'End If

            'Return Me.EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "ResourceDiskSpaceWarning"), GetType(Integer))

            Dim tmpPath As String
            tmpPath = GetSetting(CON_PATH_IPPLAYER & "/ResourceDiskSpaceWarning", "") '200MB
            If String.IsNullOrEmpty(tmpPath) Then
                SetSetting(CON_PATH_IPPLAYER & "/ResourceDiskSpaceWarning", "204800")
                tmpPath = "204800"
            End If

            Return Long.Parse(tmpPath)
        End Get
    End Property

    Public Shared ReadOnly Property SocketCompressionEnabled() As Boolean
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "SocketCompressionEnabled") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "SocketCompressionEnabled", 1)

            'End If

            'Return Me.EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "SocketCompressionEnabled"), GetType(Boolean))

            Dim tmpPath As String
            tmpPath = GetSetting(CON_PATH_IPPLAYER & "/SocketCompressionEnabled", "")
            If String.IsNullOrEmpty(tmpPath) Then
                SetSetting(CON_PATH_IPPLAYER & "/SocketCompressionEnabled", "1")
                tmpPath = "1"
            End If

            If String.Compare(tmpPath, "1") = 0 Then
                Return True
            Else
                Return False
            End If

        End Get
    End Property

#End Region


#Region "Constants"

    Private Const CON_PATH_IPPLAYER As String = "vTouchPro/vTouchProIPPlayer"

#End Region

End Class

Public Class DisplaySettings
    Inherits MachineSettings

    Public Sub New(ByVal screenIndex As Integer)
        'MyBase.New()
        _ScreenIndex = screenIndex
        Dim screens() As Screen = System.Windows.Forms.Screen.AllScreens
        If Not screenIndex >= screens.Length Then
            DisplaySettings._ScreenHeight = screens(screenIndex).Bounds.Height()
            DisplaySettings._ScreenWidth = screens(screenIndex).Bounds.Width()
        End If
    End Sub
    Public Sub New()
        'MyBase.New()
    End Sub

#Region "Variables"

    Private Shared _ScreenIndex As Integer
    Private Shared _ScreenHeight As Integer
    Private Shared _ScreenWidth As Integer
    'Private liveSignalChannelsValue As New ArrayList
    Private _WatchDogEnabled As Nullable(Of Boolean)

#End Region

#Region "Properties"

    Public Shared ReadOnly Property MessageServerAddress() As String
        Get
            'Dim tmpPath As String

            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_SPECIFICDISPLAY, "MessageServerAddress") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_SPECIFICDISPLAY, "MessageServerAddress", "localhost")
            '    tmpPath = "localhost"

            'Else
            '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_SPECIFICDISPLAY, "MessageServerAddress"), GetType(String))

            '    tmpPath = tmpPath.Replace(":8001", "")

            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_SPECIFICDISPLAY, "MessageServerAddress", tmpPath)

            'End If

            'If tmpPath.IndexOf(":") <> -1 Then
            '    tmpPath = tmpPath

            'ElseIf Not String.IsNullOrEmpty(tmpPath) Then
            '    tmpPath = tmpPath & ":8001"

            'End If

            'Return tmpPath
            Dim tmpPath As String
            Try
                tmpPath = GetSetting(REG_PATH_SPECIFICDISPLAY & "/MessageServerAddress", ServerAddress)
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = ServerAddress()
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "MessageServerAddress") Then
                    '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "MessageServerAddress").ToString(), GetType(String))
                    '    tmpPath = tmpPath.Replace(":8001", "")
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAYSERVICES, "MessageServerAddress", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = ServerAddress()
                        End If
                    Catch ex As Exception
                        tmpPath = ServerAddress()
                    End Try

                    tmpPath = tmpPath.Replace(":8001", "")
                    SetSetting(REG_PATH_SPECIFICDISPLAY & "/MessageServerAddress", tmpPath)
                End If

                If tmpPath.IndexOf(":") <> -1 Then
                    tmpPath = tmpPath

                ElseIf Not String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = tmpPath & ":8001"

                End If

                Return tmpPath
            Catch ex As Exception
                tmpPath = ServerAddress() & ":8001"
                Return tmpPath
            End Try

        End Get
    End Property
    Public Shared ReadOnly Property GetDisplays(ByVal i As String) As String
        Get
            Dim tmpExtensionsKeyName As String = CON_PATH_DISPLAY + "/Display" + i
            Dim tmpProcess As String
            tmpProcess = GetSetting(tmpExtensionsKeyName, "xml")

            If String.IsNullOrEmpty(tmpProcess) Then
                Dim regKey As RegistryKey
                regKey = Registry.LocalMachine.CreateSubKey("Software\mermaid technology\vTouch Pro\vTouch Pro Display\DISPLAY" + i)
                Dim tmpPath As String
                For Each tmpExtensionValue As String In regKey.GetValueNames
                    If ((tmpExtensionValue.ToString() = "DisplayId") Or (tmpExtensionValue.ToString() = "AudioLevel") Or (tmpExtensionValue.ToString() = "AudioLevel")) Then
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAY & "\DISPLAY" + i, tmpExtensionValue.ToString(), "").ToString()
                        SetSetting(tmpExtensionsKeyName & "/" & tmpExtensionValue.ToString(), tmpPath)
                    End If
                Next
            End If
            Return tmpProcess
        End Get
    End Property

    Public Shared ReadOnly Property DisplayServicesServerAddress() As String
        Get
            Dim tmpPath As String = MessageServerAddress

            If tmpPath.IndexOf(":") <> -1 Then
                Dim tmpParts() As String = tmpPath.Split(New Char() {":"}, StringSplitOptions.RemoveEmptyEntries)

                tmpPath = "gtcp://" & tmpParts(0) & ":" & CInt(tmpParts(1)) - 1 & "/DisplayServices.rem"

            ElseIf Not String.IsNullOrEmpty(tmpPath) Then
                tmpPath = "gtcp://" & tmpPath & ":8000/DisplayServices.rem"

            End If

            Return tmpPath

        End Get
    End Property


    Public Shared ReadOnly Property StatisticsReportingInterval() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "StatisticsReportingInterval") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAY, "StatisticsReportingInterval", 15)

            'End If
            'Return EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "StatisticsReportingInterval"), GetType(Integer))

            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAY & "/StatisticsReportingInterval", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "15"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "StatisticsReportingInterval") Then
                    '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "StatisticsReportingInterval").ToString(), GetType(String))
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAY, "StatisticsReportingInterval", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "15"
                        End If
                    Catch ex As Exception
                        tmpPath = "15"
                    End Try

                    SetSetting(CON_PATH_DISPLAY & "/StatisticsReportingInterval", tmpPath)
                End If

                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 15
            End Try
        End Get
    End Property

    Public Shared Property StatisticsTimeSlot() As Integer
        Get
            'Dim tmpTimeSlot As Integer = 60

            ''Move value to new reg value
            'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "TimeSlot") Then
            '    tmpTimeSlot = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "TimeSlot"), GetType(Integer))

            '    RegistryEditor.DeleteRegValue(HIVE, REG_PATH_DISPLAY, "TimeSlot")

            'End If

            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "StatisticsTimeSlot") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAY, "StatisticsTimeSlot", tmpTimeSlot)

            'End If

            'Return EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "StatisticsTimeSlot"), GetType(Integer))
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAY & "/StatisticsTimeSlot", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "60"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "StatisticsTimeSlot") Then
                    '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "StatisticsTimeSlot").ToString(), GetType(String))
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAY, "StatisticsTimeSlot", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "60"
                        End If
                    Catch ex As Exception
                        tmpPath = "60"
                    End Try

                    SetSetting(CON_PATH_DISPLAY & "/StatisticsTimeSlot", tmpPath)
                End If

                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 60
            End Try

        End Get
        Set(ByVal value As Integer)
            'RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAY, "StatisticsTimeSlot", value)
            SetSetting(CON_PATH_DISPLAY & "/StatisticsTimeSlot", value.ToString())
        End Set
    End Property

    Public Shared Property ReportStatistics() As Boolean
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "ReportStatistics") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAY, "ReportStatistics", False)
            'End If
            'Return EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "ReportStatistics"), GetType(Boolean))
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAY & "/ReportStatistics", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "False"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "ReportStatistics") Then
                    '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "ReportStatistics").ToString(), GetType(String))
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAY, "ReportStatistics", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "False"
                        End If
                    Catch ex As Exception
                        tmpPath = "False"
                    End Try

                    SetSetting(CON_PATH_DISPLAY & "/ReportStatistics", tmpPath)
                End If

                Return Boolean.Parse(tmpPath)
            Catch ex As Exception
                Return False
            End Try

        End Get
        Set(ByVal value As Boolean)
            SetSetting(CON_PATH_DISPLAY & "/ReportStatistics", value.ToString())
        End Set
    End Property

    Public Shared ReadOnly Property AllowedToPlayFallBackInMin() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "AllowedToPlayFallBackInMin") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAY, "AllowedToPlayFallBackInMin", 10)

            'End If

            'Return EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "AllowedToPlayFallBackInMin"), GetType(Integer))
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAY & "/AllowedToPlayFallBackInMin", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "10"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "AllowedToPlayFallBackInMin") Then
                    '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "AllowedToPlayFallBackInMin").ToString(), GetType(String))
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAY, "AllowedToPlayFallBackInMin", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "10"
                        End If
                    Catch ex As Exception
                        tmpPath = "10"
                    End Try

                    SetSetting(CON_PATH_DISPLAY & "/AllowedToPlayFallBackInMin", tmpPath)

                End If

                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 10
            End Try

        End Get
    End Property
    Public Shared Property DisplayType() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "AllowedToPlayFallBackInMin") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAY, "AllowedToPlayFallBackInMin", 10)
            'End If
            'Return EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "AllowedToPlayFallBackInMin"), GetType(Integer))

            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAY & "/DisplayType", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAY, "DisplayType", tmpPath).ToString()
                        If Not String.IsNullOrEmpty(tmpPath) Then
                            SetSetting(CON_PATH_DISPLAY & "/DisplayType", tmpPath)
                            Return Integer.Parse(tmpPath)
                        End If
                    Catch ex As Exception
                    End Try
                Else
                    'Microsoft.Win32.Registry.SetValue(REG_PATH_DISPLAY, "DisplayType", tmpPath, Microsoft.Win32.RegistryValueKind.DWord)
                    Return Integer.Parse(tmpPath)
                End If
            Catch ex As Exception
            End Try
        End Get
        Set(ByVal value As Integer)
            SetSetting(CON_PATH_DISPLAY & "/DisplayType", value.ToString())
            'Microsoft.Win32.Registry.SetValue(REG_PATH_DISPLAY, "DisplayType", value.ToString(), Microsoft.Win32.RegistryValueKind.DWord)
        End Set
    End Property

    Public Shared Property InstallPath() As String
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "InstallPath") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAY, "InstallPath", System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\vTouch Pro\vTouch Pro Display\")
            'End If
            'Return EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "InstallPath"), GetType(String))
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAY & "/InstallPath", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\vTouch Pro\vTouch Pro Display\"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "InstallPath") Then
                    '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "InstallPath").ToString(), GetType(String))
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAY, "InstallPath", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\vTouch Pro\vTouch Pro Display\"
                        End If
                    Catch ex As Exception
                        tmpPath = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\vTouch Pro\vTouch Pro Display\"
                    End Try

                    SetSetting(CON_PATH_DISPLAY & "/InstallPath", tmpPath)
                End If
                'Microsoft.Win32.Registry.SetValue(REG_PATH_COMMON_SETTINGS, "LocalResourcePath", tmpPath, Microsoft.Win32.RegistryValueKind.String)
                Return tmpPath
            Catch ex As Exception
                Return System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\vTouch Pro\vTouch Pro Display\"
            End Try

        End Get
        Set(ByVal value As String)
            SetSetting(CON_PATH_DISPLAY & "/InstallPath", value)
            'Microsoft.Win32.Registry.SetValue(REG_PATH_COMMON_SETTINGS, "LocalResourcePath", value, Microsoft.Win32.RegistryValueKind.String)
        End Set
    End Property
    Public Shared Property UpdateCounter() As Integer
        Get
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAY & "/UpdateCounter", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "0"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "DisplayStartupDelay") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "DisplayStartupDelay").ToString()
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAY, "UpdateCounter", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "0"
                        End If
                    Catch ex As Exception
                        tmpPath = "0"
                    End Try

                    SetSetting(CON_PATH_DISPLAY & "/UpdateCounter", tmpPath)
                End If

                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            SetSetting(CON_PATH_DISPLAY & "/UpdateCounter", value.ToString())
        End Set
    End Property
    Public Shared Property UpdateInterval() As Integer
        Get
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAY & "/UpdateInterval", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "600000"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAYSERVICES, "DisplayStartupDelay") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAYSERVICES, "DisplayStartupDelay").ToString()
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAY, "UpdateInterval", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "600000"
                        End If
                    Catch ex As Exception
                        tmpPath = "600000"
                    End Try

                    SetSetting(CON_PATH_DISPLAY & "/UpdateInterval", tmpPath)
                End If

                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 600000
            End Try

        End Get
        Set(ByVal value As Integer)
            SetSetting(CON_PATH_DISPLAY & "/UpdateInterval", value.ToString())
        End Set
    End Property
    Public Shared Property UpdateVersion() As String
        Get
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAY & "/UpdateVersion", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "0"
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAY, "UpdateVersion", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "0"
                        End If
                    Catch ex As Exception
                        tmpPath = "0"
                    End Try

                End If
                SetSetting(CON_PATH_DISPLAY & "/UpdateVersion", tmpPath)
                Return tmpPath
            Catch ex As Exception
                Return 0
            End Try

        End Get
        Set(ByVal value As String)
            SetSetting(CON_PATH_DISPLAY & "/UpdateVersion", value)
        End Set
    End Property
    Public Shared Property ScreenIndex() As Integer
        Get
            Return _ScreenIndex
        End Get
        Set(ByVal value As Integer)
            _ScreenIndex = value

            ''''Set Screen Layout''''''''
            '_ScreenIndex = ScreenIndex
            Dim screens() As Screen = System.Windows.Forms.Screen.AllScreens
            If Not ScreenIndex >= screens.Length Then
                ScreenHeight = screens(ScreenIndex).Bounds.Height()
                ScreenHeight = screens(ScreenIndex).Bounds.Width()
            End If
            ''''End Set Screen Layout''''''''

        End Set
    End Property

    Public Shared Property ScreenHeight() As Integer
        Get
            Return _ScreenHeight

        End Get
        Set(ByVal value As Integer)
            _ScreenHeight = value
        End Set
    End Property

    Public Shared Property ScreenWidth() As Integer
        Get
            Return _ScreenWidth

        End Get
        Set(ByVal value As Integer)
            _ScreenWidth = value
        End Set
    End Property

    Public Shared Property DisplayId() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_SPECIFICDISPLAY, "DisplayId") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_SPECIFICDISPLAY, "DisplayId", 0)
            'End If
            'Return EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_SPECIFICDISPLAY, "DisplayId"), GetType(Integer))
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_SPECIFICDISPLAY & "/DisplayId", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "0"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_SPECIFICDISPLAY, "DisplayId") Then
                    '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_SPECIFICDISPLAY, "DisplayId").ToString(), GetType(String))
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_SPECIFICDISPLAY, "DisplayId", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "0"
                        End If
                    Catch ex As Exception
                        tmpPath = "0"
                    End Try

                    SetSetting(CON_PATH_SPECIFICDISPLAY & "/DisplayId", tmpPath)
                End If

                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 0
            End Try

        End Get
        Set(ByVal Value As Integer)
            'RegistryEditor.SetRegValue(HIVE, REG_PATH_SPECIFICDISPLAY, "DisplayId", Value)
            SetSetting(CON_PATH_SPECIFICDISPLAY & "/DisplayId", Value.ToString())

        End Set
    End Property

    Public Shared Property LogLevel() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "LogLevel") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAY, "LogLevel", 0)

            'End If

            'Return EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "LogLevel"), GetType(Integer))
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAY & "/LogLevel", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "0"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "LogLevel") Then
                    '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "LogLevel").ToString(), GetType(String))
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAY, "LogLevel", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "0"
                        End If
                    Catch ex As Exception
                        tmpPath = "0"
                    End Try

                    SetSetting(CON_PATH_DISPLAY & "/LogLevel", tmpPath)
                End If

                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 0
            End Try

        End Get
        Set(ByVal value As Integer)
            SetSetting(CON_PATH_DISPLAY & "/LogLevel", value.ToString())
        End Set
    End Property

    Public Shared Property StandardGroupPlaylistId() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_SPECIFICDISPLAY, "StandardGroupPlaylistId") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_SPECIFICDISPLAY, "StandardGroupPlaylistId", 0)

            'End If

            'Return EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_SPECIFICDISPLAY, "StandardGroupPlaylistId"), GetType(Integer))
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_SPECIFICDISPLAY & "/StandardGroupPlaylistId", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "0"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_SPECIFICDISPLAY, "StandardGroupPlaylistId") Then
                    '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_SPECIFICDISPLAY, "StandardGroupPlaylistId").ToString(), GetType(String))
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_SPECIFICDISPLAY, "StandardGroupPlaylistId", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "0"
                        End If
                    Catch ex As Exception
                        tmpPath = "0"
                    End Try

                    SetSetting(CON_PATH_SPECIFICDISPLAY & "/StandardGroupPlaylistId", tmpPath)
                End If

                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 0
            End Try

        End Get
        Set(ByVal Value As Integer)
            'RegistryEditor.SetRegValue(HIVE, REG_PATH_SPECIFICDISPLAY, "StandardGroupPlaylistId", Value)
            SetSetting(CON_PATH_SPECIFICDISPLAY & "/StandardGroupPlaylistId", Value.ToString())
        End Set
    End Property

    Public Shared Property CustomPropertyCollectionId() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_SPECIFICDISPLAY, "CustomPropertyCollectionId") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_SPECIFICDISPLAY, "CustomPropertyCollectionId", 0)

            'End If

            'Return EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_SPECIFICDISPLAY, "CustomPropertyCollectionId"), GetType(Integer))
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_SPECIFICDISPLAY & "/CustomPropertyCollectionId", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "0"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_SPECIFICDISPLAY, "CustomPropertyCollectionId") Then
                    '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_SPECIFICDISPLAY, "CustomPropertyCollectionId").ToString(), GetType(String))
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_SPECIFICDISPLAY, "CustomPropertyCollectionId", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "0"
                        End If
                    Catch ex As Exception
                        tmpPath = "0"
                    End Try

                    SetSetting(CON_PATH_SPECIFICDISPLAY & "/CustomPropertyCollectionId", tmpPath)
                End If

                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 0
            End Try

        End Get
        Set(ByVal Value As Integer)
            'RegistryEditor.SetRegValue(HIVE, REG_PATH_SPECIFICDISPLAY, "CustomPropertyCollectionId", Value)
            SetSetting(CON_PATH_SPECIFICDISPLAY & "/CustomPropertyCollectionId", Value.ToString())

        End Set
    End Property

    Public Shared Property PlaylistFilterId() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_SPECIFICDISPLAY, "PlaylistFilterId") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_SPECIFICDISPLAY, "PlaylistFilterId", 0)

            'End If

            'Return EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_SPECIFICDISPLAY, "PlaylistFilterId"), GetType(Integer))
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_SPECIFICDISPLAY & "/PlaylistFilterId", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "0"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_SPECIFICDISPLAY, "PlaylistFilterId") Then
                    '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_SPECIFICDISPLAY, "PlaylistFilterId").ToString(), GetType(String))
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_SPECIFICDISPLAY, "PlaylistFilterId", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "0"
                        End If
                    Catch ex As Exception
                        tmpPath = "0"
                    End Try

                    SetSetting(CON_PATH_SPECIFICDISPLAY & "/PlaylistFilterId", "0")
                End If

                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 0
            End Try

        End Get
        Set(ByVal Value As Integer)
            'RegistryEditor.SetRegValue(HIVE, REG_PATH_SPECIFICDISPLAY, "PlaylistFilterId", Value)
            SetSetting(CON_PATH_SPECIFICDISPLAY & "/PlaylistFilterId", Value.ToString())

        End Set
    End Property

    Public Shared ReadOnly Property ShowBackground() As Boolean
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_SPECIFICDISPLAY, "ShowBackground") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_SPECIFICDISPLAY, "ShowBackground", 1)

            'End If

            'Return EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_SPECIFICDISPLAY, "ShowBackground"), GetType(Boolean))
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_SPECIFICDISPLAY & "/ShowBackground", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "0"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_SPECIFICDISPLAY, "ShowBackground") Then
                    '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_SPECIFICDISPLAY, "ShowBackground").ToString(), GetType(String))
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_SPECIFICDISPLAY, "ShowBackground", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "0"
                        End If
                    Catch ex As Exception
                        tmpPath = "0"
                    End Try

                    SetSetting(CON_PATH_SPECIFICDISPLAY & "/ShowBackground", "0")
                End If

                If tmpPath = "1" Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Return False
            End Try

        End Get
    End Property

    Public Shared Property Culture() As CultureInfo
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "Culture") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAY, "Culture", 1030)

            'End If
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(REG_PATH_DISPLAY & "/Culture", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "1030"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "Culture") Then
                    '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "Culture").ToString(), GetType(String))
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAY, "Culture", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "1030"
                        End If
                    Catch ex As Exception
                        tmpPath = "1030"
                    End Try

                    SetSetting(CON_PATH_DISPLAY & "/Culture", tmpPath)
                End If

                'Return New CultureInfo(CInt(EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "Culture"), GetType(Integer))))
                Return New CultureInfo(CInt(EnsureNoNullValue(tmpPath, GetType(Integer))))
            Catch ex As Exception
                Return New CultureInfo(CInt(EnsureNoNullValue("1030", GetType(Integer))))
            End Try

        End Get
        Set(ByVal Value As CultureInfo)
            'RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAY, "Culture", Value.LCID)
            SetSetting(CON_PATH_DISPLAY & "/Culture", Value.LCID.ToString())
        End Set
    End Property

    Public Shared Property LastMajorUpdate() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "LastMajorUpdate") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAY, "LastMajorUpdate", 0)

            'End If

            'Return RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "LastMajorUpdate")
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAY & "/LastMajorUpdate", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "0"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "LastMajorUpdate") Then
                    '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "LastMajorUpdate").ToString(), GetType(String))
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAY, "LastMajorUpdate", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "0"
                        End If
                    Catch ex As Exception
                        tmpPath = "0"
                    End Try

                    SetSetting(CON_PATH_DISPLAY & "/LastMajorUpdate", "0")
                End If

                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 0
            End Try

        End Get
        Set(ByVal Value As Integer)
            'RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAY, "LastMajorUpdate", Value)
            SetSetting(CON_PATH_DISPLAY & "/LastMajorUpdate", Value.ToString())

        End Set
    End Property

    Public Shared Property LastMinorUpdate() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "LastMinorUpdate") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAY, "LastMinorUpdate", 0)

            'End If

            'Return RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "LastMinorUpdate")
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAY & "/LastMinorUpdate", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "0"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "LastMinorUpdate") Then
                    '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "LastMinorUpdate").ToString(), GetType(String))
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAY, "LastMinorUpdate", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "0"
                        End If
                    Catch ex As Exception
                        tmpPath = "0"
                    End Try

                    SetSetting(CON_PATH_DISPLAY & "/LastMinorUpdate", "0")
                End If

                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 0
            End Try

        End Get
        Set(ByVal Value As Integer)
            'RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAY, "LastMinorUpdate", Value)
            SetSetting(CON_PATH_DISPLAY & "/LastMinorUpdate", Value.ToString())

        End Set
    End Property

    Public Shared Property LastBuildUpdate() As Integer
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "LastBuildUpdate") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAY, "LastBuildUpdate", 0)

            'End If

            'Return RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "LastBuildUpdate")
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(REG_PATH_DISPLAY & "/LastBuildUpdate", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "0"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "LastBuildUpdate") Then
                    '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "LastBuildUpdate").ToString(), GetType(String))
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAY, "LastBuildUpdate", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "0"
                        End If
                    Catch ex As Exception
                        tmpPath = "0"
                    End Try

                    SetSetting(CON_PATH_DISPLAY & "/LastBuildUpdate", "0")
                End If

                Return Integer.Parse(tmpPath)
            Catch ex As Exception
                Return 0
            End Try

        End Get
        Set(ByVal Value As Integer)
            'RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAY, "LastBuildUpdate", Value)
            SetSetting(CON_PATH_DISPLAY & "/LastBuildUpdate", Value.ToString())
        End Set
    End Property

    Public Shared Property LastUpdate() As Date
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "LastUpdate") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAY, "LastUpdate", Date.MinValue.ToString)

            'End If

            'Dim dateString As String = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "LastUpdate")
            'Try

            '    Return Date.Parse(dateString)
            'Catch ex As Exception
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAY, "LastUpdate", Date.MinValue.ToString())
            '    Return Date.MinValue

            'End Try
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAY & "/LastUpdate", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = Date.MinValue.ToString("yyyy-MM-dd'T'HH:mm:ss")
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "LastBuildUpdate") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "LastBuildUpdate").ToString("yyyy-MM-dd'T'HH:mm:ss")
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAY, "LastUpdate", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = Date.MinValue.ToString("yyyy-MM-dd'T'HH:mm:ss")
                        End If
                    Catch ex As Exception
                        tmpPath = Date.MinValue.ToString("yyyy-MM-dd'T'HH:mm:ss")
                    End Try

                    SetSetting(CON_PATH_DISPLAY & "/LastUpdate", tmpPath)
                End If

                Try
                    Return DateTime.ParseExact(tmpPath, "yyyy-MM-dd'T'HH:mm:ss", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None)
                Catch ex As Exception
                    SetSetting(CON_PATH_DISPLAY & "/LastUpdate", Date.MinValue.ToString("yyyy-MM-dd'T'HH:mm:ss"))
                    Return Date.Parse(Date.MinValue.ToString("yyyy-MM-dd'T'HH:mm:ss"))
                    'Return Date.MinValue
                End Try
            Catch ex As Exception
                Return Date.Parse(Date.MinValue.ToString("yyyy-MM-dd'T'HH:mm:ss"))
            End Try

        End Get
        Set(ByVal Value As Date)
            'RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAY, "LastUpdate", Value.ToString)
            SetSetting(CON_PATH_DISPLAY & "/LastUpdate", Value.ToString("yyyy-MM-dd'T'HH:mm:ss"))

        End Set
    End Property

    Public Shared Property SocketCompressionEnabled() As Boolean
        Get
            'If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "SocketCompressionEnabled") Then
            '    RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAY, "SocketCompressionEnabled", 1)

            'End If

            'Return Me.EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "SocketCompressionEnabled"), GetType(Boolean))

            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAY & "/SocketCompressionEnabled", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "1"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "SocketCompressionEnabled") Then
                    '    tmpPath = RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "SocketCompressionEnabled").ToString()
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAY, "SocketCompressionEnabled", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "1"
                        End If
                    Catch ex As Exception
                        tmpPath = "1"
                    End Try

                    SetSetting(CON_PATH_DISPLAY & "/SocketCompressionEnabled", "1")
                End If

                If tmpPath = "1" Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Return True
            End Try

        End Get
        Set(ByVal value As Boolean)
            SetSetting(CON_PATH_DISPLAY & "/SocketCompressionEnabled", value.ToString())
        End Set
    End Property

    Public Shared Property WatchDogEnabled() As Boolean
        Get
            'If Not _WatchDogEnabled.HasValue Then
            '    If Not RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "WatchDogEnabled") Then
            '        RegistryEditor.SetRegValue(HIVE, REG_PATH_DISPLAY, "WatchDogEnabled", 1)

            '    End If

            '    Me._WatchDogEnabled = Me.EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "WatchDogEnabled"), GetType(Boolean))

            'End If

            'Return Me._WatchDogEnabled.Value
            Try
                Dim tmpPath As String
                tmpPath = GetSetting(CON_PATH_DISPLAY & "/WatchDogEnabled", "")
                If String.IsNullOrEmpty(tmpPath) Then
                    tmpPath = "1"
                    'If RegistryEditor.ValueExists(HIVE, REG_PATH_DISPLAY, "WatchDogEnabled") Then
                    '    tmpPath = EnsureNoNullValue(RegistryEditor.GetRegValue(HIVE, REG_PATH_DISPLAY, "WatchDogEnabled").ToString(), GetType(String))
                    'End If
                    Try
                        tmpPath = Microsoft.Win32.Registry.GetValue(REG_PATH_DISPLAY, "WatchDogEnabled", tmpPath).ToString()
                        If String.IsNullOrEmpty(tmpPath) Then
                            tmpPath = "1"
                        End If
                    Catch ex As Exception
                        tmpPath = "1"
                    End Try

                    SetSetting(REG_PATH_DISPLAY & "/WatchDogEnabled", tmpPath)
                End If

                If tmpPath = "1" Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Return True
            End Try

        End Get
        Set(ByVal value As Boolean)
            SetSetting(REG_PATH_DISPLAY & "/WatchDogEnabled", value.ToString())
        End Set
    End Property

#End Region

#Region "Constants"
    'Friend REG_PATH_SPECIFICDISPLAY As String = "Software\Victor Soft\vTouch Pro\vTouch Pro Display\" & "DISPLAY" & ScreenIndex
    'Private Const REG_PATH_DISPLAY_CHANNELS_CURCHANNELSTATUS As String = "Software\Victor Soft\vTouch Pro\vTouch Pro Display\Channels\CurChannelStatus"
    'Private Const REG_PATH_DISPLAY_CHANNELS_TUNEDCHANNELS As String = "Software\Victor Soft\vTouch Pro\vTouch Pro Display\Channels\TunedChannels"
    'Private Const REG_PATH_DISPLAY_GTCP As String = "Software\Victor Soft\vTouch Pro\vTouch Pro Display\GTCP"

    Private Shared ReadOnly Property CON_PATH_SPECIFICDISPLAY() As String
        Get
            Return "vTouchPro/vTouchProDisplay/" & "DISPLAY" & ScreenIndex
        End Get
    End Property

    Private Shared ReadOnly Property REG_PATH_SPECIFICDISPLAY() As String
        Get
            Return REG_PATH_DISPLAY & "\DISPLAY" & ScreenIndex
        End Get
    End Property


#End Region

End Class