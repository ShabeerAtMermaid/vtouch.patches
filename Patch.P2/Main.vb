Imports Microsoft.Win32
Imports System.IO

' REMEMBER TO UPDATE THE PATCH ID (in case you are copy/pasting this code) UNDER CONSTANTS! /SFP
Public Class Main

#Region "Constants"

    Private Const PatchID As Integer = 2
    Private RegData_Patches As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro", "InstalledPatches")
    Private RegData_Patches_NEW As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\Victor Soft Updater", "InstalledPatches")
    Private RegData_CustomerID As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro\Common Settings", "CustomerId")

    'Private Const REG_PATH_WINDOWS_POWERSETTINGS As String = "Control Panel\PowerCfg"
    Private RegData_PowerSettings As RegData = New RegData(Enums.Hive.currentuser, "Control Panel\PowerCfg", "CurrentPowerPolicy")
    Private RegData_DesktopBGImage As RegData = New RegData(Enums.Hive.currentuser, "Control Panel\Desktop", "Wallpaper")
    Private RegData_DesktopConvertedBGImage As RegData = New RegData(Enums.Hive.currentuser, "Control Panel\Desktop", "ConvertedWallpaper")
    Private RegData_DesktopBGImageStyle As RegData = New RegData(Enums.Hive.currentuser, "Control Panel\Desktop", "WallpaperStyle")
    Private RegData_DesktopBGColor As RegData = New RegData(Enums.Hive.currentuser, "Control Panel\Colors", "Background")
    Private RegData_WinLogonAutoAdminLogon As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon", "AutoAdminLogon")
    Private RegData_WinLogonDefaultUsername As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon", "DefaultUsername")
    Private RegData_WinLogonDefaultPassword As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon", "DefaultPassword")
    Private RegData_DesktopIcons As RegData = New RegData(Enums.Hive.currentuser, "Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced", "HideIcons")
    Private RegData_ErrorReporting As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Microsoft\PCHealth\ErrorReporting", "DoReport")
    Private RegData_ErrorReportingUI As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Microsoft\PCHealth\ErrorReporting", "ShowUI")
    Private RegData_ScreenSaver As RegData = New RegData(Enums.Hive.currentuser, "Control Panel\Desktop", "SCRNSAVE.EXE")
    Private RegData_LogonType As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon", "LogonType")
    Private RegData_LogonLogoffMessages As RegData = New RegData(Enums.Hive.localmachine, "Software\Microsoft\Windows\CurrentVersion\Policies\System", "DisableStatusMessages")
    Private RegData_Firewall As RegData = New RegData(Enums.Hive.localmachine, "SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\StandardProfile", "EnableFirewall")
    Private RegData_BalloonTips As RegData = New RegData(Enums.Hive.currentuser, "Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced", "EnableBalloonTips")
    Private RegData_StandbyPassword As RegData = New RegData(Enums.Hive.currentuser, "Software\Policies\Microsoft\Windows\System\Power", "PromptPasswordOnResume")
    Private RegData_StandbyPassword2 As RegData = New RegData(Enums.Hive.localmachine, "Software\Policies\Microsoft\Windows\System\Power", "PromptPasswordOnResume")
    Private RegData_CursorName As RegData = New RegData(Enums.Hive.currentuser, "Control Panel\Cursors", "@")
    Private RegData_CursorArrow As RegData = New RegData(Enums.Hive.currentuser, "Control Panel\Cursors", "Arrow")
    Private RegData_CursorWait As RegData = New RegData(Enums.Hive.currentuser, "Control Panel\Cursors", "Wait")
    Private RegData_CursorAppStarting As RegData = New RegData(Enums.Hive.currentuser, "Control Panel\Cursors", "AppStarting")
    Private RegData_Shell As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon", "Shell")
    Private RegData_Event As RegData = New RegData(Enums.Hive.localmachine, "SYSTEM\CurrentControlSet\Services\Eventlog\Application", "Retention")
    Private RegData_ActiveDesktop As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer", "NoActiveDesktop")
    Private RegData_MouseLocation As RegData = New RegData(Enums.Hive.currentuser, "Control Panel\Desktop", "UserPreferencesMask")
    Private RegData_RestartRequired As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro\vTouch Pro DisplayServices", "RestartRequired")
    Private Hive As String = Enums.Hive.localmachine
    Private Path As String = "SYSTEM\CurrentControlSet\Services\"

#End Region

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            LblStatus.Text = "Tweaking system.."

            If Not VerifyAlreadyRun() Then
                Me.HideDesktopIcons()
                Me.DisableBalloonTips()
                Me.DisableErrorReporting()
                Me.DisableScreenSaver()
                'Me.DisableWelcomeScreen()
                'Me.DisableLogonLogoffMessages()
                Me.DisableShell()
                Me.DisableMouseArrow()
                Me.DisableActiveDesktop()
                Me.EventClean()
                Me.DisableStandbyPassword()
                Me.SetPowerSettings()
                Me.SetMouseLocation()
                '  Me.SetAutoLogon()

                'DisableService("wuauserv")
                EnableService("AudioSrv")
                'DisableService("CryptSvc")
                'DisableService("seclogon")
                'DisableService("WZCSVC")
                'DisableService("helpsvc")
                'DisableService("PolicyAgent")
                DisableService("Messenger")
                'DisableService("Schedule")
                'DisableService("Spooler")
                'DisableService("Themes")
                'DisableService("srservice")
                'DisableService("W32Time")

                ' We have a bunch of customer specific code to execute also
                'ExecuteCustomerSpecific()

                'Lets mark it for restart
                SetRegValue(RegData_RestartRequired.Hive, RegData_RestartRequired.Path, RegData_RestartRequired.Value, 1)

            End If

            RegisterPatchRan()

            LblStatus.Text = "All done."
        Catch ex As Exception
            LblStatus.Text = "Exception: " & ex.Message

        End Try
        Me.Close()
    End Sub

    Private Function VerifyAlreadyRun() As Boolean      ' Checks if the patch has already been installed
        Dim InstalledPatches() As String = GetRegValue(RegData_Patches_NEW.Hive, RegData_Patches_NEW.Path, RegData_Patches_NEW.Value)

        If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
            For Each tmpPatchID As String In InstalledPatches
                If tmpPatchID = PatchID Then
                    Return True

                End If
            Next
        End If

        InstalledPatches = GetRegValue(RegData_Patches.Hive, RegData_Patches.Path, RegData_Patches.Value)

        If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
            For Each tmpPatchID As String In InstalledPatches
                If tmpPatchID = PatchID Then
                    Return True

                End If
            Next
        End If

        Return False

    End Function

    Private Sub RegisterPatchRan()
        Dim InstalledPatches() As String = GetRegValue(RegData_Patches_NEW.Hive, RegData_Patches_NEW.Path, RegData_Patches_NEW.Value)
        Dim tmpInstalled As ArrayList = New ArrayList

        If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
            tmpInstalled.AddRange(InstalledPatches)

        End If

        tmpInstalled.Add(PatchID.ToString)

        SetRegValue(RegData_Patches_NEW.Hive, RegData_Patches_NEW.Path, RegData_Patches_NEW.Value, tmpInstalled.ToArray(GetType(String)))

    End Sub

#Region "Customer Specific code"

    Private Sub ExecuteCustomerSpecific()
        Dim CustomerID As Integer = GetRegValue(Enums.Hive.localmachine, RegData_CustomerID.Path, RegData_CustomerID.Value)
        If Not CustomerID = Nothing Then
            If CustomerID = 2019 Then Matas()
        End If
    End Sub

    Private Sub Matas()
        If File.Exists(System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) & "\vTouch Pro Resources\2019\Media\1030\1101.jpg") Then
            File.Move(System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) & "\vTouch Pro Resources\2019\Media\1030\1101.jpg", System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) & "\vTouch Pro Resources\2019\Media\1030\1104.jpg")
        End If
        If File.Exists(System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) & "\vTouch Pro Resources\2019\Media\1030\1102.jpg") Then
            File.Move(System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) & "\vTouch Pro Resources\2019\Media\1030\1102.jpg", System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) & "\vTouch Pro Resources\2019\Media\1030\1105.jpg")
        End If
        If File.Exists(System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) & "\vTouch Pro Resources\2019\Media\1030\1103.jpg") Then
            File.Move(System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) & "\vTouch Pro Resources\2019\Media\1030\1103.jpg", System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) & "\vTouch Pro Resources\2019\Media\1030\1106.jpg")
        End If

        ' Replaces the normal fallback images
        Dim TmpImages(2) As String
        TmpImages(0) = "A-Podiet.jpg"
        TmpImages(1) = "SemiSelektiv.jpg"
        TmpImages(2) = "Selektiv.jpg"

        Dim counter As Integer = 1
        For Each filestr As String In TmpImages
            Dim filepath As String = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles) & "\vTouch Pro Resources\2019\Media\1030\110" & counter & ".jpg"
            If File.Exists(filepath) Then File.Delete(filepath)
            Dim tmpImage As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("VictorSoft.vTouch.Patch.P2." & filestr)
            Dim buffer(tmpImage.Length - 1) As Byte
            tmpImage.Read(buffer, 0, buffer.Length)
            Dim tmpStream As Stream = File.Create(filepath)
            Try
                tmpStream.Write(buffer, 0, buffer.Length)
            Finally
                tmpStream.Dispose()
            End Try
            counter = counter + 1
        Next
    End Sub

#End Region

#Region "Services"
    Private Sub DisableService(ByVal name As String)
        SetRegValue(Hive, Path & name, "Start", 4) ' Disabled
        Dim TmpControl As Control() = Me.Controls.Find("Lbl" & name, False)
        If TmpControl.Length = 1 Then
            TmpControl(0).ForeColor = Color.DarkGreen
            TmpControl = Me.Controls.Find("Chk" & name, False)
            If TmpControl.Length = 1 Then TmpControl(0).Visible = False
        End If

    End Sub
    Private Sub EnableService(ByVal name As String)
        SetRegValue(Hive, Path & name, "Start", 2) ' Automatic
    End Sub

#End Region

    Private Sub SetAutoLogon()
        SetRegValue(RegData_WinLogonAutoAdminLogon.Hive, RegData_WinLogonAutoAdminLogon.Path, RegData_WinLogonAutoAdminLogon.Value, "1")
        SetRegValue(RegData_WinLogonDefaultUsername.Hive, RegData_WinLogonDefaultUsername.Path, RegData_WinLogonDefaultUsername.Value, "starkistv")
        SetRegValue(RegData_WinLogonDefaultPassword.Hive, RegData_WinLogonDefaultPassword.Path, RegData_WinLogonDefaultPassword.Value, "starkis01")

    End Sub

    Private Sub DisableMouseArrow()
        Dim filepath As String = System.Environment.GetEnvironmentVariable("SystemRoot") & "\Cursors\vTouchCursor.cur"
        If File.Exists(filepath) Then File.Delete(filepath)
        Dim tmpImage As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("VictorSoft.vTouch.Patch.P2.vTouchCursor.cur")
        Dim buffer(tmpImage.Length - 1) As Byte
        tmpImage.Read(buffer, 0, buffer.Length)
        Dim tmpStream As Stream = File.Create(filepath)
        Try
            tmpStream.Write(buffer, 0, buffer.Length)
        Finally
            tmpStream.Dispose()
        End Try
        Dim TmpLocation = System.Environment.GetEnvironmentVariable("SystemRoot") & "\Cursors\vTouchCursor.cur"

        SetRegValue(Enums.Hive.currentuser, RegData_CursorArrow.Path, RegData_CursorArrow.Value, TmpLocation)
        SetRegValue(Enums.Hive.currentuser, RegData_CursorWait.Path, RegData_CursorWait.Value, TmpLocation)
        SetRegValue(Enums.Hive.currentuser, RegData_CursorAppStarting.Path, RegData_CursorAppStarting.Value, TmpLocation)

    End Sub

    Private Sub DisableShell()
        SetRegValue(Enums.Hive.localmachine, RegData_Shell.Path, RegData_Shell.Value, "")
    End Sub

    Private Sub EventClean()
        SetRegValue(Enums.Hive.localmachine, RegData_Event.Path, RegData_Event.Value, 0)
    End Sub

    Private Sub DisableActiveDesktop()
        SetRegValue(Enums.Hive.localmachine, RegData_ActiveDesktop.Path, RegData_ActiveDesktop.Value, "1")
    End Sub

    Private Sub HideDesktopIcons()
        SetRegValue(Me.RegData_DesktopIcons.Hive, Me.RegData_DesktopIcons.Path, Me.RegData_DesktopIcons.Value, 1)

    End Sub

    Private Sub DisableErrorReporting()
        SetRegValue(Me.RegData_ErrorReporting.Hive, Me.RegData_ErrorReporting.Path, Me.RegData_ErrorReporting.Value, 0)
        SetRegValue(Me.RegData_ErrorReportingUI.Hive, Me.RegData_ErrorReportingUI.Path, Me.RegData_ErrorReportingUI.Value, 0)

    End Sub

    Private Sub DisableScreenSaver()
        SetRegValue(Me.RegData_ScreenSaver.Hive, Me.RegData_ScreenSaver.Path, Me.RegData_ScreenSaver.Value, "")

    End Sub

    Private Sub SetPowerSettings()
        SetRegValue(Me.RegData_PowerSettings.Hive, Me.RegData_PowerSettings.Path, Me.RegData_PowerSettings.Value, "2")

    End Sub

    Private Sub DisableWelcomeScreen()
        SetRegValue(Me.RegData_LogonType.Hive, Me.RegData_LogonType.Path, Me.RegData_LogonType.Value, 0)

    End Sub

    Private Sub DisableLogonLogoffMessages()
        SetRegValue(Me.RegData_LogonLogoffMessages.Hive, Me.RegData_LogonLogoffMessages.Path, Me.RegData_LogonLogoffMessages.Value, 1)

    End Sub

    Private Sub DisableFirewall()
        SetRegValue(Me.RegData_Firewall.Hive, Me.RegData_Firewall.Path, Me.RegData_Firewall.Value, 0)

    End Sub

    Private Sub DisableBalloonTips()
        SetRegValue(Me.RegData_BalloonTips.Hive, Me.RegData_BalloonTips.Path, Me.RegData_BalloonTips.Value, 0)

    End Sub

    Private Sub DisableStandbyPassword()
        SetRegValue(Me.RegData_StandbyPassword.Hive, Me.RegData_StandbyPassword.Path, Me.RegData_StandbyPassword.Value, 0)
        SetRegValue(Me.RegData_StandbyPassword2.Hive, Me.RegData_StandbyPassword2.Path, Me.RegData_StandbyPassword2.Value, 0)

    End Sub

    Private Sub SetMouseLocation()
        SetRegValue(Me.RegData_MouseLocation.Hive, Me.RegData_MouseLocation.Path, Me.RegData_MouseLocation.Value, New Byte() {158, 126, 5, 128})

    End Sub

#Region "Registry"
    Public Shared Function GetRegValue(ByVal whereToRead As Enums.Hive, ByVal path As String, ByVal value As String) As Object
        Dim regKey As RegistryKey
        Dim regKeyValue As Object

        'Determine the hive and open/create the designated path
        Select Case whereToRead
            Case Enums.Hive.classesroot
                regKey = Registry.ClassesRoot.CreateSubKey(path)
            Case Enums.Hive.currentconfig
                regKey = Registry.CurrentConfig.CreateSubKey(path)
            Case Enums.Hive.currentuser
                regKey = Registry.CurrentUser.CreateSubKey(path)
            Case Enums.Hive.dyndata
                regKey = Registry.DynData.CreateSubKey(path)
            Case Enums.Hive.localmachine
                regKey = Registry.LocalMachine.CreateSubKey(path)
            Case Enums.Hive.performancedata
                regKey = Registry.PerformanceData.CreateSubKey(path)
            Case Enums.Hive.users
                regKey = Registry.Users.CreateSubKey(path)
            Case Else
                regKey = Registry.LocalMachine.CreateSubKey(path)
        End Select

        regKeyValue = regKey.GetValue(value, Nothing)
        regKey.Close()
        Return regKeyValue
    End Function

    Public Shared Sub SetRegValue(ByVal whereToWrite As Enums.Hive, ByVal path As String, ByVal value As String, ByVal data As Object)
        Dim regKey As RegistryKey

        'Determine the hive and open/create the designated path
        Select Case whereToWrite
            Case Enums.Hive.classesroot
                regKey = Registry.ClassesRoot.CreateSubKey(path)
            Case Enums.Hive.currentconfig
                regKey = Registry.CurrentConfig.CreateSubKey(path)
            Case Enums.Hive.currentuser
                regKey = Registry.CurrentUser.CreateSubKey(path)
            Case Enums.Hive.dyndata
                regKey = Registry.DynData.CreateSubKey(path)
            Case Enums.Hive.localmachine
                regKey = Registry.LocalMachine.CreateSubKey(path)
            Case Enums.Hive.performancedata
                regKey = Registry.PerformanceData.CreateSubKey(path)
            Case Enums.Hive.users
                regKey = Registry.Users.CreateSubKey(path)
            Case Else
                regKey = Registry.LocalMachine.CreateSubKey(path)
        End Select

        'Determine which datatype to write to regedit
        If TypeOf data Is Integer Then
            regKey.SetValue(value, data, RegistryValueKind.DWord)

        ElseIf TypeOf data Is String() Then
            regKey.SetValue(value, data, RegistryValueKind.MultiString)

        ElseIf TypeOf data Is Byte() Then
            regKey.SetValue(value, data, RegistryValueKind.Binary)

        Else
            regKey.SetValue(value, data)

        End If

        regKey.Close()

    End Sub
#End Region
End Class

Friend Class RegData

#Region "Attributes"

    Private _Hive As Enums.Hive
    Private _Path As String
    Private _Value As String

#End Region

#Region "Properties"

    Public ReadOnly Property Hive() As Enums.Hive
        Get
            Return Me._Hive

        End Get
    End Property

    Public ReadOnly Property Path() As String
        Get
            Return Me._Path

        End Get
    End Property

    Public ReadOnly Property Value() As String
        Get
            Return Me._Value

        End Get
    End Property

#End Region

    Public Sub New(ByVal hive As Enums.Hive, ByVal path As String, ByVal value As String)
        Me._Hive = hive
        Me._Path = path
        Me._Value = value

    End Sub

End Class

