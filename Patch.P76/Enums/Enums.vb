Public Class Enums

    Public Enum Hive As Integer
        ClassesRoot = 1
        CurrentConfig = 2
        CurrentUser = 4
        DynData = 8
        LocalMachine = 16
        PerformanceData = 32
        Users = 64

    End Enum

End Class
