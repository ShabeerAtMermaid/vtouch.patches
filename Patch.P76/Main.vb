Imports System.IO

Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then
                'Dim tmpProgramDirectory As String = Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "vTouch Pro\vTouch Pro 3G Modem")

                Dim tmpProgramDirectory As String = Constants.RegData_CleanupInstallPath.GetValue().ToString()

                tmpProgramDirectory = tmpProgramDirectory.Replace("Display", "Cleanup")
                If Not Directory.Exists(tmpProgramDirectory) Then Directory.CreateDirectory(tmpProgramDirectory)

                Dim tmpFileList As New ArrayList

                tmpFileList.Add("ConfigFile.dll")
                tmpFileList.Add("Interop.CONFIGFILELib.dll")
                tmpFileList.Add("mermaid.BaseObjects.dll")
                tmpFileList.Add("mermaid.LogSystem.Shared.dll")
                tmpFileList.Add("mermaid.LogSystem.Shared.pdb")
                tmpFileList.Add("mermaid.RegistryEditor.dll")
                tmpFileList.Add("mermaid.RegistryEditor.pdb")
                tmpFileList.Add("vTouch.Display.Settings.dll")
                tmpFileList.Add("vTouch.Display.Settings.pdb")
                tmpFileList.Add("vTouch.Shared.dll")
                tmpFileList.Add("vTouchCleaupExtension.dll")
                tmpFileList.Add("vTouchCleaupExtension.pdb")

                For Each tmpFileName As String In tmpFileList
                    Dim tmpFilePath As String = Path.Combine(tmpProgramDirectory, tmpFileName)
                    If File.Exists(tmpFilePath & ".vup") Then
                        File.Delete(tmpFilePath & ".vup")
                    End If
                    If File.Exists(tmpFilePath) Then
                        File.Move(tmpFilePath, tmpFilePath & ".vup")
                        'My.Computer.FileSystem.RenameFile(tmpFilePath, tmpSettingFilePath & ".vup")
                    End If
                Next

                For Each tmpFileName As String In tmpFileList
                    Dim tmpFilePath As String = Path.Combine(tmpProgramDirectory, tmpFileName)

                    Dim tmpFileStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.Header & tmpFileName)
                    Helpers.WriteStream(tmpFileStream, tmpFilePath)
                Next


                Dim tmpSettingFilePath As String = Path.Combine(System.Environment.CurrentDirectory, "vTouch.Display.Settings.dll")
                If File.Exists(tmpSettingFilePath & ".vup") Then
                    File.Delete(tmpSettingFilePath & ".vup")
                End If
                If File.Exists(tmpSettingFilePath) Then
                    File.Move(tmpSettingFilePath, tmpSettingFilePath & ".vup")
                    'My.Computer.FileSystem.RenameFile(tmpSettingFilePath, tmpSettingFilePath & ".vup")
                End If
                tmpSettingFilePath = Path.Combine(System.Environment.CurrentDirectory, "vTouch.Display.Settings.pdb")
                If File.Exists(tmpSettingFilePath & ".vup") Then
                    File.Delete(tmpSettingFilePath & ".vup")
                End If
                If File.Exists(tmpSettingFilePath) Then
                    File.Move(tmpSettingFilePath, tmpSettingFilePath & ".vup")
                    'My.Computer.FileSystem.RenameFile(tmpSettingFilePath, tmpSettingFilePath & ".vup")
                End If

                tmpSettingFilePath = Path.Combine(System.Environment.CurrentDirectory, "vTouch.Display.Settings.dll")
                Dim tmpSettingFileStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.Header & "vTouch.Display.Settings.dll")
                Helpers.WriteStream(tmpSettingFileStream, tmpSettingFilePath)

                tmpSettingFilePath = Path.Combine(System.Environment.CurrentDirectory, "vTouch.Display.Settings.pdb")
                tmpSettingFileStream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.Header & "vTouch.Display.Settings.pdb")
                Helpers.WriteStream(tmpSettingFileStream, tmpSettingFilePath)


                Dim tmpArgData As New RegData(Enums.Hive.LocalMachine, "SOFTWARE\mermaid technology\vTouch Pro\vTouch Pro DisplayServices\Extensions", "vTouch Pro Cleanup")
                'tmpArgData.SetValue("vTouch.CleanupExt.CleanupExtension,C:\Program Files\vTouch Pro\vTouch Pro Cleanup\vTouchCleaupExtension.dll")
                tmpProgramDirectory = Path.Combine(tmpProgramDirectory, "vTouchCleaupExtension.dll")
                tmpArgData.SetValue("vTouch.CleanupExt.CleanupExtension," & tmpProgramDirectory)
                Constants.RegData_RestartRequired.SetValue(1)

                Helpers.RegisterPatchRan()

                LblMessage.Text = "Installed"
                Helpers.WriteLog("Installed")

            Else
                LblMessage.Text = "Not installed"
                Helpers.WriteLog("Not installed")

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class