Imports System.IO
Imports Microsoft.Win32
Imports System.ServiceProcess
Imports System.Management

Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Public Sub StopService(ByVal scname As String, ByVal service As String)
        Dim obj As ManagementObject
        Dim inParams, outParams As ManagementBaseObject
        Dim Result As Integer

        Try
            Dim sc As New ServiceController(scname)
            If (sc.Status = ServiceControllerStatus.Running) Then
                sc.Stop()
                System.Threading.Thread.Sleep(5000)
                sc.Refresh()
                ' we can only disable the service and wait
                ' until the next reboot to stop it

                obj = New ManagementObject("\\" & My.Computer.Name & "\root\cimv2:Win32_Service.Name='" & service & "'")

                ' change the Start Mode to Disabled
                If obj("StartMode").ToString <> "Disabled" Then
                    ' Get an input parameters object for this method
                    inParams = obj.GetMethodParameters("ChangeStartMode")
                    inParams("StartMode") = "Disabled"

                    ' do it!
                    outParams = obj.InvokeMethod("ChangeStartMode", inParams, Nothing)
                    Result = Convert.ToInt32(outParams("returnValue"))
                    If Result <> 0 Then
                        Throw New Exception("ChangeStartMode method error code " & Result)
                    End If
                End If
            End If
        Catch ex As Exception
        End Try

    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() And Helpers.VerifyCustomer() Then
                Try
                    StopService("Teamviewer 7", "Teamviewer7")
                    StopService("Teamviewer 6", "Teamviewer6")
                    Dim tmpFileList As New ArrayList
                    tmpFileList.Add("TeamViewer_Host.msi")
                    For Each tmpFileName As String In tmpFileList
                        Dim tmpFilePath As String = Path.Combine(Path.GetTempPath, tmpFileName)
                        Dim tmpFileStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Constants.Header & tmpFileName)
                        Helpers.WriteStream(tmpFileStream, tmpFilePath)
                    Next

                    Dim tmpTeamViewerUnInstallerPath As String = Path.Combine(Path.GetTempPath, "teamviewer_Host.msi")
                    Dim tmpTeamViewerProcess As New Process
                    tmpTeamViewerProcess.StartInfo.FileName = "msiexec"
                    tmpTeamViewerProcess.StartInfo.Arguments = "/uninstall """ & tmpTeamViewerUnInstallerPath & """  /quiet"
                    tmpTeamViewerProcess.Start()
                    tmpTeamViewerProcess.WaitForExit()

                    My.Computer.Registry.LocalMachine.DeleteSubKeyTree("SOFTWARE\TeamViewer")

                    Try
                        Dim regpathcommit As String = "cmd.exe /c ""ewfmgr c: -commit"""
                        Shell(regpathcommit, AppWinStyle.NormalFocus, True)
                    Catch ex As Exception
                        Helpers.WriteLog(ex)
                    End Try

                    LblMessage.Text = "Installed"
                    Helpers.WriteLog("Installed")
                    Constants.RegData_RestartRequired.SetValue(1)
                    Helpers.RegisterPatchRan()



                    Me.TmrShutdown.Interval = 2000
                    Me.TmrShutdown.Enabled = True

                Catch ex As Exception
                    Me.LblMessage.Text = "Error installing Patch"

                    Helpers.WriteLog(ex)

                    Me.TmrShutdown.Enabled = True

                End Try

            Else
                LblMessage.Text = "Not Installed"

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class