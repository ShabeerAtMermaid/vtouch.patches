Imports Microsoft.Win32
Imports System.IO

Public Class Main

    Private Const PatchID As Integer = 14
    'Private RegData_Patches As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro", "InstalledPatches")
    Private RegData_Patches As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\Victor Soft Updater", "InstalledPatches")
    Private RegData_CustomerID As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro\Common Settings", "CustomerId")
    Private RegData_RestartRequired As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro\vTouch Pro DisplayServices", "RestartRequired")
    Private RegData_LastError As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro", "LastError")
    Private Const Header = "VictorSoft.vTouch.Patch.P14."

    Private Sub WriteStream(ByVal FileStream As Stream, ByVal Filename As String)
        Dim buffer(FileStream.Length - 1) As Byte
        FileStream.Read(buffer, 0, buffer.Length)
        Dim tmpStream As Stream = File.Create(Filename)
        Try
            tmpStream.Write(buffer, 0, buffer.Length)
        Finally
            tmpStream.Dispose()
        End Try

    End Sub

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not VerifyAlreadyRun() Then
                LblStatus.Text = "Loading files.."

                Dim tmpProcess As Process

                'Live
                Dim tmpLiveDll As New DllData("VSLive.dll", True)
                Dim tmpLivePath As String = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\vTouch Pro\vTouch Pro DisplayServices\" & tmpLiveDll.FileName

                'Try
                '    If File.Exists(tmpLivePath) Then File.Delete(tmpLivePath)

                'Catch ex As Exception
                '    LblStatus.Text = "Cannot delete " & tmpLivePath
                '    Try
                '        SetRegValue(RegData_LastError.Hive, RegData_LastError.Path, RegData_LastError.Value, "Cannot delete " & tmpLivePath)

                '    Catch logex As Exception

                '    End Try
                'End Try

                'Dim TmpLiveFileStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Header & tmpLiveDll.FileName)

                'Try
                '    WriteStream(TmpLiveFileStream, tmpLivePath)

                'Catch ex As Exception
                '    Try
                '        SetRegValue(RegData_LastError.Hive, RegData_LastError.Path, RegData_LastError.Value, ex.Message)

                '    Catch logex As Exception

                '    End Try
                'End Try

                tmpProcess = New Process
                tmpProcess.StartInfo.FileName = System.Environment.GetFolderPath(Environment.SpecialFolder.System) & "\regsvr32.exe"
                tmpProcess.StartInfo.Arguments = "/s """ & tmpLivePath & """"
                tmpProcess.StartInfo.CreateNoWindow = True
                tmpProcess.Start()

                'Watch Dog
                Dim tmpWatchDogDll As New DllData("VSDogWatch.dll", True)
                Dim tmpWatchDogPath As String = System.Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\vTouch Pro\vTouch Pro Display\" & tmpWatchDogDll.FileName

                'Try
                '    If File.Exists(tmpWatchDogPath) Then File.Delete(tmpWatchDogPath)

                'Catch ex As Exception
                '    LblStatus.Text = "Cannot delete " & tmpWatchDogPath
                '    Try
                '        SetRegValue(RegData_LastError.Hive, RegData_LastError.Path, RegData_LastError.Value, "Cannot delete " & tmpWatchDogPath)

                '    Catch logex As Exception

                '    End Try
                'End Try

                'Dim TmpWatchDogFileStream As Stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(Header & tmpWatchDogDll.FileName)

                'Try
                '    WriteStream(TmpWatchDogFileStream, tmpWatchDogPath)

                'Catch exp As Exception
                '    Try
                '        SetRegValue(RegData_LastError.Hive, RegData_LastError.Path, RegData_LastError.Value, exp.Message)

                '    Catch logex As Exception

                '    End Try
                'End Try

                tmpProcess = New Process
                tmpProcess.StartInfo.FileName = System.Environment.GetFolderPath(Environment.SpecialFolder.System) & "\regsvr32.exe"
                tmpProcess.StartInfo.Arguments = "/s """ & tmpWatchDogPath & """"
                tmpProcess.StartInfo.CreateNoWindow = True
                tmpProcess.Start()

                RegisterPatchRan()

            End If

            LblStatus.Text = "All done."

        Catch ex As Exception
            LblStatus.Text = "Exception: " & ex.Message

        End Try

        Me.Close()

    End Sub

    Private Function VerifyAlreadyRun() As Boolean      ' Checks if the patch has already been installed
        Dim InstalledPatches() As String
        Dim TmpPatchID As String
        InstalledPatches = GetRegValue(Enums.Hive.localmachine, RegData_Patches.Path, RegData_Patches.Value)
        Dim TmpInstalled As ArrayList = New ArrayList
        If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
            For Each TmpPatchID In InstalledPatches
                TmpInstalled.Add(TmpPatchID)
                If TmpPatchID = PatchID Then
                    Return True
                End If
            Next
        End If
        ' TmpInstalled.Add(PatchID.ToString)
        ' SetRegValue(Enums.Hive.localmachine, RegData_Patches.Path, RegData_Patches.Value, TmpInstalled.ToArray(GetType(String)))

        Return False

    End Function

    Private Sub RegisterPatchRan()
        Dim InstalledPatches() As String

        InstalledPatches = GetRegValue(Enums.Hive.localmachine, RegData_Patches.Path, RegData_Patches.Value)
        Dim TmpInstalled As ArrayList = New ArrayList
        If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
            TmpInstalled.AddRange(InstalledPatches)
        End If
        TmpInstalled.Add(PatchID.ToString)
        SetRegValue(Enums.Hive.localmachine, RegData_Patches.Path, RegData_Patches.Value, TmpInstalled.ToArray(GetType(String)))

    End Sub

#Region "Registry"
    Public Shared Function GetRegValue(ByVal whereToRead As Enums.Hive, ByVal path As String, ByVal value As String) As Object
        Dim regKey As RegistryKey
        Dim regKeyValue As Object

        'Determine the hive and open/create the designated path
        Select Case whereToRead
            Case Enums.Hive.classesroot
                regKey = Registry.ClassesRoot.CreateSubKey(path)
            Case Enums.Hive.currentconfig
                regKey = Registry.CurrentConfig.CreateSubKey(path)
            Case Enums.Hive.currentuser
                regKey = Registry.CurrentUser.CreateSubKey(path)
            Case Enums.Hive.dyndata
                regKey = Registry.DynData.CreateSubKey(path)
            Case Enums.Hive.localmachine
                regKey = Registry.LocalMachine.CreateSubKey(path)
            Case Enums.Hive.performancedata
                regKey = Registry.PerformanceData.CreateSubKey(path)
            Case Enums.Hive.users
                regKey = Registry.Users.CreateSubKey(path)
            Case Else
                regKey = Registry.LocalMachine.CreateSubKey(path)
        End Select

        regKeyValue = regKey.GetValue(value, Nothing)
        regKey.Close()
        Return regKeyValue
    End Function

    Public Shared Sub SetRegValue(ByVal whereToWrite As Enums.Hive, ByVal path As String, ByVal value As String, ByVal data As Object)
        Dim regKey As RegistryKey

        'Determine the hive and open/create the designated path
        Select Case whereToWrite
            Case Enums.Hive.classesroot
                regKey = Registry.ClassesRoot.CreateSubKey(path)
            Case Enums.Hive.currentconfig
                regKey = Registry.CurrentConfig.CreateSubKey(path)
            Case Enums.Hive.currentuser
                regKey = Registry.CurrentUser.CreateSubKey(path)
            Case Enums.Hive.dyndata
                regKey = Registry.DynData.CreateSubKey(path)
            Case Enums.Hive.localmachine
                regKey = Registry.LocalMachine.CreateSubKey(path)
            Case Enums.Hive.performancedata
                regKey = Registry.PerformanceData.CreateSubKey(path)
            Case Enums.Hive.users
                regKey = Registry.Users.CreateSubKey(path)
            Case Else
                regKey = Registry.LocalMachine.CreateSubKey(path)
        End Select

        'Determine which datatype to write to regedit
        If TypeOf data Is Integer Then
            regKey.SetValue(value, data, RegistryValueKind.DWord)

        ElseIf TypeOf data Is String() Then
            regKey.SetValue(value, data, RegistryValueKind.MultiString)

        ElseIf TypeOf data Is Byte() Then
            regKey.SetValue(value, data, RegistryValueKind.Binary)

        Else
            regKey.SetValue(value, data)

        End If

        regKey.Close()

    End Sub

#End Region

End Class

Friend Class DllData

    Public FileName As String
    Public Register As Boolean

    Public Sub New(ByVal filename As String, ByVal register As Boolean)
        MyBase.New()

        Me.FileName = filename
        Me.Register = register

    End Sub

End Class

Friend Class RegData

#Region "Attributes"

    Private _Hive As Enums.Hive
    Private _Path As String
    Private _Value As String

#End Region

#Region "Properties"

    Public ReadOnly Property Hive() As Enums.Hive
        Get
            Return Me._Hive

        End Get
    End Property

    Public ReadOnly Property Path() As String
        Get
            Return Me._Path

        End Get
    End Property

    Public ReadOnly Property Value() As String
        Get
            Return Me._Value

        End Get
    End Property

#End Region

    Public Sub New(ByVal hive As Enums.Hive, ByVal path As String, ByVal value As String)
        Me._Hive = hive
        Me._Path = path
        Me._Value = value

    End Sub

End Class
