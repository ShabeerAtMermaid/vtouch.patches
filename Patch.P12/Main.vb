Imports Microsoft.Win32
Imports System.IO

Public Module Main

#Region "Constants"

    Private Const PatchID As Integer = 12

#End Region

    Private RegData_Patches As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro", "InstalledPatches")
    Private RegData_CustomerID As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro\Common Settings", "CustomerId")
    Private RegData_LastError As RegData = New RegData(Enums.Hive.localmachine, "SOFTWARE\Victor Soft\vTouch Pro", "LastError")

    Public Sub Main(ByVal args() As String)
        Try
            If Not VerifyAlreadyRun() And VerifyCustomer() And ValidateExecution() Then
                Dim tmpIPAddress As String = GetNewIPAddress()
                Dim tmpGateway As String = GetNewGateway()

                Shell("netsh interface ip set address LAN-forbindelse static " & tmpIPAddress & " 255.255.255.0 " & tmpGateway & " 1", AppWinStyle.NormalFocus, True)
                Shell("ipconfig -flushdns", AppWinStyle.NormalFocus, True)

                RegisterPatchRan()

            End If

        Catch ex As Exception
            Try
                SetRegValue(RegData_LastError.Hive, RegData_LastError.Path, RegData_LastError.Value, ex.Message)

            Catch logex As Exception

            End Try
        End Try
    End Sub

    Private Sub WriteStream(ByVal fileStream As Stream, ByVal filepath As String)
        If File.Exists(filepath) Then
            File.Delete(filepath)

        End If

        Dim buffer(fileStream.Length - 1) As Byte
        fileStream.Read(buffer, 0, buffer.Length)

        Dim tmpLocation = filepath
        Dim tmpStream As Stream = File.Create(tmpLocation)

        Try
            tmpStream.Write(buffer, 0, buffer.Length)

        Finally
            tmpStream.Dispose()

        End Try
    End Sub

    Private Function VerifyAlreadyRun() As Boolean      ' Checks if the patch has already been installed
        Dim TmpInstalled As ArrayList = New ArrayList
        Dim InstalledPatches() As String = GetRegValue(Enums.Hive.localmachine, RegData_Patches.Path, RegData_Patches.Value)

        If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
            For Each TmpPatchID As String In InstalledPatches
                TmpInstalled.Add(TmpPatchID)

                If TmpPatchID = PatchID Then
                    Return True

                End If
            Next
        End If

        Return False

    End Function

    Private Function VerifyCustomer() As Boolean
        Dim tmpCustomerId As Integer = GetRegValue(RegData_CustomerID.Hive, RegData_CustomerID.Path, RegData_CustomerID.Value)

        If tmpCustomerId = 2019 Then
            Return True

        End If

        Return False

    End Function

    Private Function ValidateExecution() As Boolean
        'Dim tmpComputerList As New ArrayList
        'tmpComputerList.Add("b17493p1")
        'tmpComputerList.Add("b17493p2")
        'tmpComputerList.Add("b17507p1")
        'tmpComputerList.Add("b17507p2")
        'tmpComputerList.Add("b17515p1")
        'tmpComputerList.Add("b17515p2")
        'tmpComputerList.Add("b17523p1")
        'tmpComputerList.Add("b17523p2")
        'tmpComputerList.Add("b10243p1")
        'tmpComputerList.Add("b10243p2")
        'tmpComputerList.Add("b10529p1")
        'tmpComputerList.Add("b10529p2")
        'tmpComputerList.Add("b12522p1")
        'tmpComputerList.Add("b12522p2")
        'tmpComputerList.Add("b12526p1")
        'tmpComputerList.Add("b12526p2")

        'Return tmpComputerList.Contains(My.Computer.Name.ToLower)

        Return True

    End Function

    Private Function GetNewIPAddress() As String
        Dim h As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName)
        Dim tmpIPAddress As System.Net.IPAddress = h.AddressList.GetValue(0)

        Dim tmpAddressBytes() As Byte = tmpIPAddress.GetAddressBytes

        Dim tmpIPAddressA As String = tmpAddressBytes(0)
        Dim tmpIPAddressB As String = tmpAddressBytes(1)
        Dim tmpIPAddressC As String = tmpAddressBytes(2)
        Dim tmpIPAddressD As String = tmpAddressBytes(3)

        tmpIPAddressB = tmpIPAddressB(0) & "0" & tmpIPAddressB(1)
        tmpIPAddressD = "10" & tmpIPAddressD

        Return tmpIPAddressA & "." & tmpIPAddressB & "." & tmpIPAddressC & "." & tmpIPAddressD

    End Function

    Private Function GetNewGateway() As String
        Dim h As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName)
        Dim tmpIPAddress As System.Net.IPAddress = h.AddressList.GetValue(0)

        Dim tmpAddressBytes() As Byte = tmpIPAddress.GetAddressBytes

        Dim tmpIPAddressA As String = tmpAddressBytes(0)
        Dim tmpIPAddressB As String = tmpAddressBytes(1)
        Dim tmpIPAddressC As String = tmpAddressBytes(2)
        Dim tmpIPAddressD As String = tmpAddressBytes(3)

        tmpIPAddressB = tmpIPAddressB(0) & "0" & tmpIPAddressB(1)
        tmpIPAddressD = 250

        Return tmpIPAddressA & "." & tmpIPAddressB & "." & tmpIPAddressC & "." & tmpIPAddressD

    End Function

    Private Sub RegisterPatchRan()
        Dim TmpInstalled As ArrayList = New ArrayList
        Dim InstalledPatches() As String = GetRegValue(Enums.Hive.localmachine, RegData_Patches.Path, RegData_Patches.Value)

        If Not InstalledPatches Is Nothing Then         ' Loop through the installed patches if any
            TmpInstalled.AddRange(InstalledPatches)

        End If

        TmpInstalled.Add(PatchID.ToString)
        SetRegValue(Enums.Hive.localmachine, RegData_Patches.Path, RegData_Patches.Value, TmpInstalled.ToArray(GetType(String)))

    End Sub

#Region "Registry"

    Public Function GetRegValue(ByVal whereToRead As Enums.Hive, ByVal path As String, ByVal value As String) As Object
        Dim regKey As RegistryKey
        Dim regKeyValue As Object

        'Determine the hive and open/create the designated path
        Select Case whereToRead
            Case Enums.Hive.classesroot
                regKey = Registry.ClassesRoot.CreateSubKey(path)
            Case Enums.Hive.currentconfig
                regKey = Registry.CurrentConfig.CreateSubKey(path)
            Case Enums.Hive.currentuser
                regKey = Registry.CurrentUser.CreateSubKey(path)
            Case Enums.Hive.dyndata
                regKey = Registry.DynData.CreateSubKey(path)
            Case Enums.Hive.localmachine
                regKey = Registry.LocalMachine.CreateSubKey(path)
            Case Enums.Hive.performancedata
                regKey = Registry.PerformanceData.CreateSubKey(path)
            Case Enums.Hive.users
                regKey = Registry.Users.CreateSubKey(path)
            Case Else
                regKey = Registry.LocalMachine.CreateSubKey(path)
        End Select

        regKeyValue = regKey.GetValue(value, Nothing)
        regKey.Close()

        Return regKeyValue

    End Function

    Public Sub SetRegValue(ByVal whereToWrite As Enums.Hive, ByVal path As String, ByVal value As String, ByVal data As Object)
        Dim regKey As RegistryKey

        'Determine the hive and open/create the designated path
        Select Case whereToWrite
            Case Enums.Hive.classesroot
                regKey = Registry.ClassesRoot.CreateSubKey(path)
            Case Enums.Hive.currentconfig
                regKey = Registry.CurrentConfig.CreateSubKey(path)
            Case Enums.Hive.currentuser
                regKey = Registry.CurrentUser.CreateSubKey(path)
            Case Enums.Hive.dyndata
                regKey = Registry.DynData.CreateSubKey(path)
            Case Enums.Hive.localmachine
                regKey = Registry.LocalMachine.CreateSubKey(path)
            Case Enums.Hive.performancedata
                regKey = Registry.PerformanceData.CreateSubKey(path)
            Case Enums.Hive.users
                regKey = Registry.Users.CreateSubKey(path)
            Case Else
                regKey = Registry.LocalMachine.CreateSubKey(path)
        End Select

        'Determine which datatype to write to regedit
        If TypeOf data Is Integer Then
            regKey.SetValue(value, data, RegistryValueKind.DWord)

        ElseIf TypeOf data Is String() Then
            regKey.SetValue(value, data, RegistryValueKind.MultiString)

        ElseIf TypeOf data Is Byte() Then
            regKey.SetValue(value, data, RegistryValueKind.Binary)

        Else
            regKey.SetValue(value, data)

        End If

        regKey.Close()

    End Sub

#End Region

End Module

Friend Class RegData

#Region "Attributes"

    Private _Hive As Enums.Hive
    Private _Path As String
    Private _Value As String

#End Region

#Region "Properties"

    Public ReadOnly Property Hive() As Enums.Hive
        Get
            Return Me._Hive

        End Get
    End Property

    Public ReadOnly Property Path() As String
        Get
            Return Me._Path

        End Get
    End Property

    Public ReadOnly Property Value() As String
        Get
            Return Me._Value

        End Get
    End Property

#End Region

    Public Sub New(ByVal hive As Enums.Hive, ByVal path As String, ByVal value As String)
        Me._Hive = hive
        Me._Path = path
        Me._Value = value

    End Sub


End Class