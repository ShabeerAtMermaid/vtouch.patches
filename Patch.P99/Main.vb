Imports System.IO

Public Class Main

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text &= " " & Constants.PatchID
            Me.LblMessage.Text = "Installing vTouch Pro Patch " & Constants.PatchID
            Helpers.WriteLog("Installing vTouch Pro Patch " & Constants.PatchID)

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

    Private Sub TmrInstall_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrInstall.Tick
        Me.TmrInstall.Enabled = False

        Me.InstallPatch()

    End Sub

    Private Sub TmrShutdown_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TmrShutdown.Tick
        Try
            Me.TmrShutdown.Enabled = False
            Process.GetCurrentProcess.Kill()

        Catch ex As Exception
            'SILENT

        End Try
    End Sub

    Private Sub InstallPatch()
        Try
            If Not Helpers.HasBeenExecuted() Then

                If Helpers.VerifyCustomer() And Helpers.RejseplanenXMLExist() Then

                    Dim screenWidth As Integer = Screen.PrimaryScreen.Bounds.Width
                    Dim screenHeight As Integer = Screen.PrimaryScreen.Bounds.Height

                    If (screenWidth = 1080 And screenHeight = 1920) Then
                        Constants.RegData_WathdogPoint.SetValue("962,81")
                    ElseIf (screenWidth = 900 And screenHeight = 1600) Then
                        Constants.RegData_WathdogPoint.SetValue("757,98")
                    ElseIf (screenWidth = 768 And screenHeight = 1360) Then
                        Constants.RegData_WathdogPoint.SetValue("625,98")
                    ElseIf (screenWidth = 720 And screenHeight = 1280) Then
                        Constants.RegData_WathdogPoint.SetValue("602,82")
                    End If
                    Constants.RegData_RestartRequired.SetValue(1)
                End If

                Helpers.RegisterPatchRan()

                LblMessage.Text = "Installed"
                Helpers.WriteLog("Installed")

            Else
                LblMessage.Text = "Not installed"
                Helpers.WriteLog("Not installed")

            End If

            Me.TmrShutdown.Interval = 2000
            Me.TmrShutdown.Enabled = True

        Catch ex As Exception
            Me.LblMessage.Text = "Error installing Patch"

            Helpers.WriteLog(ex)

            Me.TmrShutdown.Enabled = True

        End Try
    End Sub

End Class